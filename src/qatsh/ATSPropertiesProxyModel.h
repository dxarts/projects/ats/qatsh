// ATSPropertiesProxyModel class definition
//
//  ATS proxy model for the file properties
//  (Row = ignored, Column = property id)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef ATSPPROPERTIESPROXYMODEL_H
#define ATSPPROPERTIESPROXYMODEL_H

#include <QModelIndex>
#include <QAbstractProxyModel>


class ATSPropertiesProxyModel : public QAbstractProxyModel
{
    Q_OBJECT

public:

    ATSPropertiesProxyModel(QObject *parent = 0);
    virtual ~ATSPropertiesProxyModel();

    virtual QModelIndex mapFromSource(const QModelIndex& sourceIndex) const;
    virtual QModelIndex mapToSource(const QModelIndex& proxyIndex) const;

    //virtual QItemSelection mapSelectionFromSource (const QItemSelection& sourceSelection) const;
    //virtual QItemSelection mapSelectionToSource (const QItemSelection& proxySelection) const;

    virtual QModelIndex index(int row, int column, const QModelIndex &miParent) const;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QModelIndex parent(const QModelIndex &index) const;
};

#endif // ATSPPROPERTIESPROXYMODEL_H
