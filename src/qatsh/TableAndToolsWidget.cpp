// TableAndToolsWidget class implementation
//
//  The left widget that holds :
//  - the table views for partials and residual frames properties
//  - the tab widget holding the frame selector and threshold selector tools
//  - the frame properties editor tool
//  - the synthetizer tool
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <iostream>

#include <QDir>
#include <QHeaderView>
#include <QPushButton>
#include <QFileDialog>

#include "TableAndToolsWidget.h"
#include "ui_TableAndToolsWidget.h"

#include "ATSModelManager.h"
#include "ATSModel.h"
#include "ATSModelItems.h"
#include "ATSPropertiesProxyModel.h"
#include "ATSPartialsFrameProxyModel.h"
#include "ATSResidualsFrameProxyModel.h"
#include "FrameAndDataProcessor.h"


// Constructors / destructor =====================================================
TableAndToolsWidget::TableAndToolsWidget(QWidget *parent)
: QWidget(parent), _pui(new Ui::TableAndToolsWidget), 
  _pModelMgr(0), _eCurrentDataType(TypesAndConstants::ePartial), 
  _pProcessor(0)
{
    _pui->setupUi(this);

    // Analysis (2nd widget in the stack)
    connect(_pui->qpbAnaReset, SIGNAL(clicked()), _pui->qwAnaParams, SLOT(onRestoreDefaults()));
    connect(_pui->qpbAnaSampleFile, SIGNAL(clicked()), this, SLOT(onChangeAnalysisSampleFile()));
    connect(_pui->qpbAnaApply, SIGNAL(clicked()), this, SLOT(onApplyAnalysisParams()));

    // Synthesis (3nd widget in the stack)
    connect(_pui->qpbSyntReset, SIGNAL(clicked()), _pui->qwSyntParams, SLOT(onRestoreDefaults()));
    connect(_pui->qpbSyntSampleFile, SIGNAL(clicked()), this, SLOT(onChangeSynthesisSampleFile()));
    connect(_pui->qpbSyntApply, SIGNAL(clicked()), this, SLOT(onApplySynthesisParams()));
    connect(_pui->qpbSyntPlayStart, SIGNAL(clicked()), 
            this, SLOT(onStartPlayingSynthesisResult()));
    connect(_pui->qpbSyntPlayStop, SIGNAL(clicked()), 
	    this, SLOT(onStopPlayingSynthesisResult()));

    // Default target file for synthesis
    // TODO: Put that in the settings.
    setSynthesisSampleFile(QDir::homePath()+"/default.wav");
}

TableAndToolsWidget::~TableAndToolsWidget()
{
    delete _pui;
	delete _pProcessor;
}

// Model managment ================================================================
void TableAndToolsWidget::setModelManager(ATSModelManager* pModelMgr)
{
    _pModelMgr = pModelMgr;

    // When the model will be reset, warn custom views of it
    // (standard table views are automatically warned, no need to do it).
    connect(_pModelMgr->mainModel(), SIGNAL(modelReset()), this, SLOT(onModelReset()));

    // Set partials and residual table views model.
    _pui->qfPartTable->setModelManager(_pModelMgr);
    _pui->qfResidTable->setModelManager(_pModelMgr);

    // Set frame and data selectors and editor model.
    _pui->qwFrameSelector->setModelManager(_pModelMgr);
    _pui->qwFrameSelector->switchDataType(_eCurrentDataType);
    _pui->qwDataSelector->setModelManager(_pModelMgr);
    _pui->qwDataSelector->switchDataType(_eCurrentDataType);
    _pui->qwEditorWidget->setModelManager(_pModelMgr);
    _pui->qwEditorWidget->switchDataType(_eCurrentDataType);

    // Initialize frame and data selectors state.
    _pui->qwFrameSelector->setState(_frameSelectStates[_eCurrentDataType]);
    _pui->qwDataSelector->setState(_dataSelectStates[_eCurrentDataType]);
    
    // Connect frame selector and partials/residuals table views.
    connect(_pui->qfPartTable, SIGNAL(currentFrameChanged(int)),
            _pui->qwFrameSelector, SLOT(onCurrentIndexChanged(int)));
    connect(_pui->qfResidTable, SIGNAL(currentFrameChanged(int)),
            _pui->qwFrameSelector, SLOT(onCurrentIndexChanged(int)));

    // Create the frame and data processor, and give it to the relevant widgets.
    _pProcessor = new FrameAndDataProcessor(_pModelMgr->mainModel());
    _pui->qfPartTable->setFrameAndDataProcessor(_pProcessor);

    // Set synthesis max duration.
    const QModelIndex miSoundDuration = 
	_pModelMgr->propertiesModel()->index(0, (int)ATSFilePropertiesItem::eSoundPropDuration,
					     QModelIndex());
    const double dDuration =
	_pModelMgr->propertiesModel()->data(miSoundDuration).toDouble();
    _pui->qwSyntParams->setMaxDuration(dDuration);
}

void TableAndToolsWidget::onModelReset()
{
    switchSelectEditDataList(_eCurrentDataType);
}

// Stacked wiget managment ===========================================================
void TableAndToolsWidget::switchTool(ETool eTool)
{
    // Show the selected tool widget.
    _pui->qswPages->setCurrentIndex(eTool);
}

void TableAndToolsWidget::switchSelectEditDataList(TypesAndConstants::EDataType eDataType)
{
    // Show the selected table view.
    _pui->qswTables->setCurrentIndex(eDataType);

    // Save frame/data selector state for current list.
    if (_eCurrentDataType != TypesAndConstants::eDataTypeNumber)
    {
	_frameSelectStates[_eCurrentDataType] = _pui->qwFrameSelector->getState();
	_dataSelectStates[_eCurrentDataType] = _pui->qwDataSelector->getState();
    }

    // Switch the frame/data selectors and editor to the right model.
    _pui->qwFrameSelector->switchDataType(eDataType);
    _pui->qwDataSelector->switchDataType(eDataType);
    _pui->qwEditorWidget->switchDataType(eDataType);

    // Restore frame/data selector state for the new list
    _pui->qwFrameSelector->setState(_frameSelectStates[eDataType]);
    _pui->qwDataSelector->setState(_dataSelectStates[eDataType]);
    switch (eDataType)
    {
        case TypesAndConstants::ePartial:
            _pui->qwFrameSelector->onCurrentIndexChanged(_pui->qfPartTable->currentFrameIndex());
            break;
        case TypesAndConstants::eResidualBand:
            _pui->qwFrameSelector->onCurrentIndexChanged(_pui->qfResidTable->currentFrameIndex());
            break;
        default:
            break;
    }

    // Save new current data type
    _eCurrentDataType = eDataType;
}

// Analysis managment ================================================================
void TableAndToolsWidget::enableAnalysis(bool bOn)
{
    _pui->qwAnalysis->setEnabled(bOn);
}

void TableAndToolsWidget::setAnalysisSampleFile(const QString& qsFileName)
{
    _pui->qleAnaSampleFile->setText(qsFileName);
}

void TableAndToolsWidget::setAnalysisParams(const IATSAnalysis* pAnaParams)
{
    _pui->qwAnaParams->reset(pAnaParams);
}

void TableAndToolsWidget::onChangeAnalysisSampleFile()
{
    const QString qsFileName =
        QFileDialog::getOpenFileName(this, tr("Choose a source sound file for analysis"),
                                     "", // TODO: get from settings
                                     tr("Sound files "
                                        "(*.wav *.aif* *.au *.snd *.raw *.voc *.paf *.caf *.sf)"));
    if (!qsFileName.isEmpty())
        _pui->qleAnaSampleFile->setText(qsFileName);
}

void TableAndToolsWidget::onApplyAnalysisParams()
{
    _pui->qwAnaParams->setupAnalysisParams();

    emit reAnalyse(_pui->qleAnaSampleFile->text(), _pui->qwAnaParams->analysisParams());
}

// Synthesis managment ================================================================
void TableAndToolsWidget::setSynthesisSampleFile(const QString& qsFileName)
{
    _pui->qleSyntSampleFile->setText(qsFileName);
}

void TableAndToolsWidget::onChangeSynthesisSampleFile()
{
    const QString qsFileName =
        QFileDialog::getSaveFileName(this, tr("Choose a target sound file for synthesis"),
                                     "", // TODO: get from settings
                                     tr("Sound files "
                                        "(*.wav *.aif* *.au *.snd *.raw *.voc *.paf *.caf *.sf)"));
    if (!qsFileName.isEmpty())
        _pui->qleSyntSampleFile->setText(qsFileName);
}

void TableAndToolsWidget::onApplySynthesisParams()
{
    emit reSynthesise();
}

void TableAndToolsWidget::onStartPlayingSynthesisResult()
{
    emit startPlayingSynthesisResult();
}

void TableAndToolsWidget::getSynthesisParams(QString& qsFileName, SampledSound::ESampleFormat& eFormat,
											 ATSSynthesis*& pSynthesis)
{
    _pui->qwSyntParams->setupSynthesis();

    qsFileName = _pui->qleSyntSampleFile->text();
    eFormat = (SampledSound::ESampleFormat)_pui->qcbSyntSampleFormat->currentIndex();
	pSynthesis = _pui->qwSyntParams->synthesis();
}

void TableAndToolsWidget::onStopPlayingSynthesisResult()
{
}

// Frame / data selection managment ===============================================
void TableAndToolsWidget::onFirstSelectedFrameChanged(int nNewIndex)
{
    // TODO : Signal graphical view for visual selection
}

void TableAndToolsWidget::onLastSelectedFrameChanged(int nNewIndex)
{
    // TODO : Signal graphical view for visual selection
}

// Events managment ================================================================
void TableAndToolsWidget::changeEvent(QEvent *e)
{
    switch (e->type()) {
    case QEvent::LanguageChange:
        _pui->retranslateUi(this);
        break;
    default:
        break;
    }
}
