// PartialsFrameTableView class definition
//
//  The table view for 1 frame of the partials.
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef PartialsFrameTableView_h
#define PartialsFrameTableView_h

#include <QFrame>

namespace Ui {
    class PartialsFrameTableView;
}

class ATSModelManager;
class QItemSelection;
class FrameAndDataProcessor;


class PartialsFrameTableView : public QFrame 
{
    Q_OBJECT
    Q_DISABLE_COPY(PartialsFrameTableView)

 public:

    explicit PartialsFrameTableView(QWidget *parent = 0);
    virtual ~PartialsFrameTableView();

    void setModelManager(ATSModelManager* pModelMgr);
    void setFrameAndDataProcessor(FrameAndDataProcessor* pProcessor);

    int currentFrameIndex() const;

 signals:

    void currentFrameChanged(int nNewIndex);

 private slots:

    // Slots to synchronize control values
    void onCurrentIndexChanged(int nNewIndex);
    void onCurrentStartTimeChanged(double dNewTime);
    void onCurrentEndTimeChanged(double dNewTime);

    // Slots for add/remove partial buttons
    void onAddPartial();
    void onRemovePartial();

    // Table view selection change management.
    void onFrameSelectionChanged(const QItemSelection& qisSelected,
                                 const QItemSelection& qisDeselected);

    // Frame model selection change management.
    void onSelectionChanged(const QItemSelection& qisSelected,
                            const QItemSelection& qisDeselected);

 private:

    Ui::PartialsFrameTableView *_pui;

    // Model manager.
    ATSModelManager *_pModelMgr;

    // Frame and data processor.
    FrameAndDataProcessor* _pProcessor;
  
    // Convenience copies of model data.
    int _nMaxFrameIndex;
    double _dFrameDuration;
    
    // Sliders / Spin-boxes syncronization lock.
    bool _bSynchronizeControls;
};

#endif // PartialsFrameTableView_h
