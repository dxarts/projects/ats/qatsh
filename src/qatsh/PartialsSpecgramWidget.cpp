// PartialsSpecgramWidget class implementation
//
//  The widget that holds the partials spectrogram graphics view
//  and the associated controls (contrast/brightness sliders, color scale, 
//  time and frequency rulers ...)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include "PartialsSpecgramWidget.h"
#include "ui_PartialsSpecgramWidget.h"

#include "ATSModelManager.h"


PartialsSpecgramWidget::PartialsSpecgramWidget(QWidget *parent)
: QWidget(parent), _pui(new Ui::PartialsSpecgramWidget)
{
    _pui->setupUi(this);
}

PartialsSpecgramWidget::~PartialsSpecgramWidget()
{
    delete _pui;
}

void PartialsSpecgramWidget::setModelManager(ATSModelManager* pModelMgr)
{
    connect(_pui->qsdAmplContrast, SIGNAL(valueChanged(int)), 
	    this, SLOT(onContrastSliderChanged(int)));

    connect(_pui->qsdAmplBrightness, SIGNAL(valueChanged(int)), 
	    this, SLOT(onBrightnessSliderChanged(int)));

    connect(_pui->qgvSpecgram, SIGNAL(colorMapChanged(const ColorMap*)),
	    _pui->qgwAmplColorScale, SLOT(onColorMapChanged(const ColorMap*)));

    connect(_pui->qgvSpecgram, SIGNAL(timeRangeChanged(const RulerSpec*)),
            _pui->qwTimeScale, SLOT(onSpecChanged(const RulerSpec*)));

    connect(_pui->qgvSpecgram, SIGNAL(frequencyRangeChanged(const RulerSpec*)),
            _pui->qwFreqScale, SLOT(onSpecChanged(const RulerSpec*)));

    connect(_pui->qwTimeScale, SIGNAL(zoomShift(double, double)),
	    _pui->qgvSpecgram, SLOT(zoomShiftTime(double, double)));

    connect(_pui->qwFreqScale, SIGNAL(zoomShift(double, double)),
	    _pui->qgvSpecgram, SLOT(zoomShiftFrequency(double, double)));

    _pui->qgvSpecgram->setModelManager(pModelMgr);
}

void PartialsSpecgramWidget::zoom(double dRelTimeAnchor, double dRelTimeFactor,
                                  double dRelFreqAnchor, double dRelFreqFactor)
{
    if (dRelTimeFactor != 1.0)
        _pui->qwTimeScale->zoom(dRelTimeAnchor, dRelTimeFactor);
    if (dRelFreqFactor != 1.0)
        _pui->qwFreqScale->zoom(dRelFreqAnchor, dRelFreqFactor);
}

void PartialsSpecgramWidget::zoomAll()
{
    _pui->qwTimeScale->zoomAll();
    _pui->qwFreqScale->zoomAll();
}

void PartialsSpecgramWidget::changeEvent(QEvent *pqEvent)
{
    switch (pqEvent->type()) {
    case QEvent::LanguageChange:
        _pui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void PartialsSpecgramWidget::onContrastSliderChanged(int nValue)
{
    const double dContrast = 
	nValue / (double)(_pui->qsdAmplContrast->maximum() - _pui->qsdAmplContrast->minimum());
    _pui->qgvSpecgram->setAmplitudeContrast(dContrast);
}

void PartialsSpecgramWidget::onBrightnessSliderChanged(int nValue)
{
    const double dBrightness = 
	nValue / (double)(_pui->qsdAmplBrightness->maximum() - _pui->qsdAmplBrightness->minimum());
    _pui->qgvSpecgram->setAmplitudeBrightness(dBrightness);
}
