// RulerSpec class definition
//
//  A ruler descriptor
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef RULERSPEC_H
#define RULERSPEC_H

#include <Qt>
#include <QList>


class RulerSpec
{
 public:

    // Note: Orientations, directions conventions are the standard mathematics ones.

    // Orientation (Horizontal / Vertical).
    typedef enum { eHorizontal = 0, eVertical } EOrientation;

    // Side of the marks (Negative = Left / Bottom, Positive = Right / Up).
    typedef enum { eNegativeSide = 0, ePositiveSide } ESide;

    // Direction (Negative = Left / Bottom, Positive = Right / Up).
    typedef enum { eNegative = 0, ePositive } EDirection;

    RulerSpec();
    RulerSpec(EOrientation eOrientation, EDirection eDirection, ESide eMarksSide,
	      double dMinValue, double dMaxValue, 
	      double dBaseOffset, double dBaseStep, double dLevelDivisor,
	      const QList<double>& qlSecondDivisors, const QList<double>& qlSecondSizes);

    EOrientation orientation() const;
    void setOrientation(EOrientation eOrientation);

    ESide marksSide() const;
    void setMarksSide(ESide eSide);

    EDirection direction() const;
    void setDirection(EDirection eDirection);

    double minValue() const;
    void setMinValue(double dValue);

    double maxValue() const;
    void setMaxValue(double dValue);

    double baseOffset() const;
    void setBaseOffset(double dOffset);

    double baseStep() const;
    void setBaseStep(double dOffset);

    double levelDivisor() const;
    void setLevelDivisor(double dDivisor);

    const QList<double>& secondaryDivisors() const;
    void setSecondaryDivisors(const QList<double>& qlDivisors);

    const QList<double>& secondaryMarksSizes() const;
    void setSecondaryMarksSizes(const QList<double>& qlDivisors);

    void reset(EOrientation eOrientation, EDirection eDirection, ESide eMarksSide,
	       double dMinValue, double dMaxValue, 
	       double dBaseOffset, double dBaseStep, double dLevelDivisor,
	       const QList<double>& qlSecondDivisors, const QList<double>& qlSecondSizes);

 protected:

    void resetBaseScale(double dMinValue, double dMaxValue, 
			double dBaseOffset, double dBaseStep);

 private:
    
    EOrientation _eOrientation;
    EDirection _eDirection;
    ESide _eMarksSide;

    double _dMinValue;
    double _dMaxValue;

    double _dBaseOffset; // First mark at dMinValue+dBaseOffset (level 0)
    double _dBaseStep; // Second mark at dMinValue+dBaseOffset+dBaseStep, third ... (level 0)

    double _dLevelDivisor; // Division factor to apply to step from level N to level N+1

     // Division factor to apply to step for each secondary scale, at a given level.
    QList<double> _qlSecondDivisors;

    // Size of secondary scale marks, whatever level, relative to the main scale marks ]0, 1]
    QList<double> _qlSecondSizes;
};

#endif // RULERSPEC_H
