// ATSSound class definition
// 
//  C++ interface to ATS_SOUND C structure and functions (atsa.h)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef ATSSOUND_H
#define ATSSOUND_H

#include <ostream>

#include "atsa.h"

#include "TypesAndConstants.h"


class ATSSound
{
 public:

    // Sound types (should have been defined in atsa.h)
    // WARNING : Values must stay consistent with atsa::ATS_HEADER comments (!).
    typedef enum
    {
	eSoundTypePartialsAmplFreq = ATSA_TYPE - 3,
	eSoundTypePartialsAmplFreqPhase = ATSA_TYPE - 2,
	eSoundTypePartialsAmplFreqNoise = ATSA_TYPE - 1,
	eSoundTypePartialsAmplFreqPhaseNoise = ATSA_TYPE
    } ESoundTypeId;

 public:

    ATSSound(ATS_SOUND* pSoundData = 0);
    ~ATSSound();

    // Properties get/setters.
    double samplingRate() const;
    //void setSamplingRate(double dSamplingRate);

    int frameSize() const;
    //void setFrameSize(int nbSamples);

    int windowSize() const;
    //void setWindowSize(int nbSamples);

    int nbFrames() const;
    //void setNbFrames(int nbFrames);

    double duration() const;
    //void setDuration(double dDuration);

    ESoundTypeId type() const;
    //void setType(ESoundTypeId eType);
    bool hasPhase() const;
    bool hasNoise() const;

    int nbPartialProperties() const;

    int fileSize() const;

    bool isOptimized() const;

    // Partials properties.
    int nbPartials() const;
    double partialsMaxAmplitude() const;
    double partialsMaxFrequency() const;

    double partialTime(int nFrameInd) const;
    double partialFrequency(int nPartInd, int nFrameInd) const;
    double partialAmplitude(int nPartInd, int nFrameInd) const;
    double partialPhase(int nPartInd, int nFrameInd) const;
    double partialSMR(int nPartInd, int nFrameInd) const;

    // Residual properties.
    int nbResidualBands() const;
    double residualBandMinFrequency(int nBandInd) const;
    double residualBandMaxFrequency(int nBandInd) const;
    double residualBandsMaxFrequency() const;
    double residualBandsMaxEnergy() const;

    double residualBandTime(int nFrameInd) const;
    double residualBandEnergy(int nBandInd, int nFrameInd) const;

    // Add/Remove partials.
    void addPartial(int nSrcPartIndex = -1);
    void removePartials(const int aPartIndexes[], int nParts);

    // Modify partials (return the parsed formula, where no "Error" should be found,
    // otherwise, no modifications is done).
    std::string modifyPartials(const int aPartIndexes[], int nParts,
			       const std::string& strFormula, 
			       TypesAndConstants::EPartialMagnitude eMagnitude);

   // .ats file input/output.
    bool load(const char* pszATSFileName);
    bool store(const char* pszATSFileName) const;

    // Direct access to the internal ATS_SOUND structure.
    ATS_SOUND* data();

    // Tools.
    void dump(const char* pszHeader, std::ostream& oStream) const;

    // TODO: move to ATSA ...
    void updateResidualMaxEnergy();

 private:

    static bool soundHasPhase(ESoundTypeId eType);
    static bool soundHasNoise(ESoundTypeId eType);

 private:

    // Internal ATSA sound structure.
    ATS_SOUND* _pSoundData;

    // The sound type (with/without noise, phase, ...).
    ESoundTypeId _eSoundType;
    
    // Maximum of energy in residual band frames
    double _dResBandFrameMaxEnergy;

    // Total data file size (header included, bytes).
    int _nFileSize;

    // Flag to indicate if _pSoundData internals must be freed
    // before reusing _pSoundData for a new sound.
    bool _bNeedFree;

};

#endif // ATSSOUND_H
