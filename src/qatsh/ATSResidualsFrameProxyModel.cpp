// ATSResidualsFrameProxyModel class definition
//
//  ATS proxy model for the residual bands data associated to 1 given frame 
//  (Row = Band index, Column = property id)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <iostream>

#include "ATSModelItems.h"
#include "ATSResidualsProxyModel.h"
#include "ATSResidualsFrameProxyModel.h"


// Constructors / Destructor ===============================================
ATSResidualsFrameProxyModel::ATSResidualsFrameProxyModel(QObject *parent)
: ATSFrameProxyModel(parent)
{
}

ATSResidualsFrameProxyModel::~ATSResidualsFrameProxyModel()
{
}

// Get number of residual bands ============================================
int ATSResidualsFrameProxyModel::nbResiduals() const
{
    return static_cast<ATSResidualsProxyModel*>(sourceModel())->nbResiduals();
}

// Proxy <-> Main model index conversions ==================================
QModelIndex ATSResidualsFrameProxyModel::mapFromSource(const QModelIndex& miSource) const
{
    if (miSource.isValid())
    {
	const ATSModelItem *pmiSourceItem = 
	    static_cast<const ATSModelItem*>(miSource.internalPointer());
	if (pmiSourceItem->type() == ATSResidualBandItem::eResidualBand)
	{
	    const ATSResidualBandItem *pmiResBandItem = 
		static_cast<const ATSResidualBandItem*>(pmiSourceItem);
	    return createIndex(pmiResBandItem->bandIndex(), miSource.column());
	}
    }

    return QModelIndex();
}

QModelIndex ATSResidualsFrameProxyModel::mapToSource(const QModelIndex& miProxy) const
{ 
    if (miProxy.isValid())
    {
	const ATSResidualsProxyModel* pSourceModel = 
	    static_cast<const ATSResidualsProxyModel*>(sourceModel());
	QModelIndex miSource = 
	    pSourceModel->index(_nCurrFrameIndex, miProxy.column(), 
				pSourceModel->index(0, miProxy.row(), QModelIndex()));
	return pSourceModel->index(_nCurrFrameIndex, miProxy.column(), 
				   pSourceModel->index(0, miProxy.row(), QModelIndex()));
    }

    return QModelIndex();
}

// QAbstractItemModel implementation =======================================
QModelIndex ATSResidualsFrameProxyModel::index(int row, int column, const QModelIndex &miParent) const
{
    if (!miParent.isValid()) // Only the root item is supported.
    {
	return createIndex(row, column); // And (row, column) is sufficient as an identifier.
    }

    return QModelIndex();
}

QVariant ATSResidualsFrameProxyModel::data(const QModelIndex &miIndex, int role) const
{
    // Note: Here, we specialize standard QAbstractModelItem::data(...) implementation
    // only to be able to format each column in a different and customized way.
    QVariant qvValue;

    // Get data to display and format it smartly but differently for each column
    if (role == Qt::DisplayRole)
    {
	const double dValue = sourceModel()->data(mapToSource(miIndex), role).toDouble();

	switch ((ATSResidualBandItem::EPropertyId)miIndex.column())
        {
            case ATSResidualBandItem::ePropMinFrequency:
		qvValue = QString::number(dValue, 'f', 2);
		break;
    
	    case ATSResidualBandItem::ePropMaxFrequency:
		qvValue = QString::number(dValue, 'f', 2);
		break;
    
	    case ATSResidualBandItem::ePropEnergy:
		qvValue = QString::number(dValue, 'f', 4);
		break;

	    // As columnCount() returns ePropNumber - 1, should never happen.
            case ATSResidualBandItem::ePropTime:
		break;
    
	    // N/A.
            case ATSResidualBandItem::ePropNumber:
		break;
	}
    }

    // Always align right (we only have numerical values).
    else if (role == Qt::TextAlignmentRole)
	qvValue = Qt::AlignRight;

    return qvValue;
}

int ATSResidualsFrameProxyModel::columnCount(const QModelIndex &miParent) const
{
    int nCount = 0;

    if (!miParent.isValid()) // Only the root item is supported.
    {
	nCount = ATSResidualBandItem::nbProperties() - 1; // We don't display the "time" column.
    }

//     std::cout << "ATSResidualsFrameProxyModel::columnCount(" 
// 	      << miParent.row() << ',' << miParent.column() 
// 	      << ':' << miParent.internalPointer() << ") = " << nCount << std::endl;

    return nCount;
}

int ATSResidualsFrameProxyModel::rowCount(const QModelIndex &miParent) const
{
    int nCount = 0;

    if (!miParent.isValid()) // Only the root item is supported.
    {
	nCount = nbResiduals();
    }

//     std::cout << "ATSResidualsFrameProxyModel::rowCount(" 
// 	      << miParent.row() << ',' << miParent.column() 
// 	      << ':' << miParent.internalPointer() << ") = " << nCount << std::endl;

    return nCount;
}

QVariant ATSResidualsFrameProxyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    switch (orientation)
    {
        case Qt::Horizontal:
	    if (role == Qt::DisplayRole)
		return ATSResidualBandItem::propertyName((ATSResidualBandItem::EPropertyId)section);
	    else if (role == Qt::ToolTipRole)
		return ATSResidualBandItem::propertyToolTip((ATSResidualBandItem::EPropertyId)section);
	case Qt::Vertical:
	    if (role == Qt::DisplayRole)
		return section+1;
    }

    return QVariant();
}

QModelIndex ATSResidualsFrameProxyModel::parent(const QModelIndex &miChild) const
{
    return QModelIndex();
}
