// PartialsSpecgramGraphicsFrameItem class definition
//
//  The QGraphicsItem-derived partial frame graphics item in a Spectrogram scene
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef PartialsSpecgramGraphicsFrameItem_H
#define PartialsSpecgramGraphicsFrameItem_H

#include <QGraphicsLineItem>

class ColorMap;
class QGraphicsSceneHoverEvent;
class QGraphicsSceneMouseEvent;


class PartialsSpecgramGraphicsFrameItem : public QGraphicsLineItem
{
 public:

	PartialsSpecgramGraphicsFrameItem(qreal x1, qreal y1, qreal x2, qreal y2,
									  double dAmpl, const ColorMap* pcmAmplColorMap,
									  int nPartialIndex, int nFrameIndex, QGraphicsItem* parent = 0);
	virtual ~PartialsSpecgramGraphicsFrameItem();

	virtual QRectF boundingRect() const;
	// virtual QPainterPath shape () const;
	double amplitude() const;
	void setAmplitude(double dAmpl);
	int partialIndex() const;
	void setPartialIndex(int nPartialIndex);
	int frameIndex() const;
	void setFrameIndex(int nFrameIndex);

	void highlight(bool bOn = true);
	void select(bool bOn = true);

 protected:

	virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* qEvent);
 	virtual void mousePressEvent(QGraphicsSceneMouseEvent* qEvent);
 	virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* qEvent);

 	virtual void hoverEnterEvent(QGraphicsSceneHoverEvent* qEvent);
 	virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent* qEvent);

 private:

 	// Amplitude.
 	double _dAmplitude;

 	// Partial index.
 	int _nPartialIndex;

 	// Frame index.
 	int _nFrameIndex;

    // Amplitude color map.
    const ColorMap* _pcmAmplColorMap;
};

#endif // PartialsSpecgramGraphicsFrameItem_H_
