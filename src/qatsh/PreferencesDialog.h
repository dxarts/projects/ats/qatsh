// PreferencesDialog class definition
//
//  The preferences dialog
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef PreferencesDialog_h
#define PreferencesDialog_h

#include <QDialog>

namespace Ui {
    class PreferencesDialog;
}

class PreferencesDialog : public QDialog
{
    Q_OBJECT
    Q_DISABLE_COPY(PreferencesDialog)

 public:

    explicit PreferencesDialog(QWidget *parent = 0);
    virtual ~PreferencesDialog();

    // Prepare default values (1st time).
    static void initializeSettings();

 protected:

    virtual void changeEvent(QEvent *pqEvent);

 public slots:

    virtual void accept();
    void reset();

 private:

    Ui::PreferencesDialog *_pui;
};

#endif // PreferencesDialog_h
