// ATSPartialsProxyModel class definition
//
//  ATS proxy model for the residual data of the ATS model :
//  - at the root level, a list of ATSResidualBandItem (1 row, nResiduals columns)
//  - for a given ATSResidualBandItem, Row = Frame, Column = property id)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <iostream>

#include "ATSModel.h"
#include "ATSModelItems.h"
#include "ATSResidualsProxyModel.h"


// Constructors / Destructor ===============================================
ATSResidualsProxyModel::ATSResidualsProxyModel(QObject *parent)
: ATSDataProxyModel(parent)
{
}

ATSResidualsProxyModel::~ATSResidualsProxyModel()
{
}

// Proxy <-> Main model index conversions ==================================
QModelIndex ATSResidualsProxyModel::mapFromSource(const QModelIndex& miSource) const
{
    if (miSource.isValid())
    {
	const ATSModelItem *pmiSourceItem = 
	    static_cast<const ATSModelItem*>(miSource.internalPointer());
	switch (pmiSourceItem->type()) 
	{
            case ATSModelItem::eResidualBands: // Row ignored, Column = partial index
		return createIndex(miSource.row(), miSource.column());

	    case ATSModelItem::eResidualBand: // Row = frame index, Column = property id
		return miSource;

	    default:
		return QModelIndex();
	}
    }

    return QModelIndex();
}

QModelIndex ATSResidualsProxyModel::mapToSource(const QModelIndex& miProxy) const
{ 
    if (miProxy.isValid())
    {
	// If proxy parent is the root item => ResidualsItem
	if (!miProxy.internalPointer())
	{
	    ATSResidualBandsItem* pResidsItem = 
		static_cast<ATSModel*>(sourceModel())->rootItem()->residualBands();
	    if (miProxy.row() == 0 
		&& miProxy.column() >= 0 && miProxy.column() < pResidsItem->count())
		return createIndex(0, miProxy.column(), pResidsItem->residualBand(miProxy.column()));
	    else
		return QModelIndex();
	}

	// If proxy parent is not the root item => ResidualBandItem
	else
	{
	    const ATSResidualBandItem *pmiResidItem = 
		static_cast<const ATSResidualBandItem*>(miProxy.internalPointer());
	    if (pmiResidItem && pmiResidItem->type() == ATSModelItem::eResidualBand)
		return miProxy;
	}
    }

    return QModelIndex();
}

// QAbstractItemModel implementation =======================================
QModelIndex ATSResidualsProxyModel::index(int row, int column, const QModelIndex &miParent) const
{
    // If parent is the root item
    if (!miParent.isValid())
    {
	ATSResidualBandsItem* pResidsItem = 
	    static_cast<ATSModel*>(sourceModel())->rootItem()->residualBands();
	if (row == 0 && column >= 0 && column < pResidsItem->count())
	    return createIndex(0, column, pResidsItem->residualBand(column));
	else
	    return QModelIndex();
    }

    // If parent is not the root item
    const ATSModelItem *pmiParentItem = 
	static_cast<const ATSModelItem*>(miParent.internalPointer());

    if (pmiParentItem && pmiParentItem->type() == ATSModelItem::eResidualBand)
    {
	// Row = frame, Column = property id
	const ATSResidualBandItem *pResidItem = 
	    static_cast<const ATSResidualBandItem*>(miParent.internalPointer());
	if (row >= 0 && row < pResidItem->nbFrames()
	    && column >= 0 && column < ATSResidualBandItem::nbProperties())
	    return createIndex(row, column, miParent.internalPointer());
	else
	    return QModelIndex();
    }

    return QModelIndex();
}

int ATSResidualsProxyModel::columnCount(const QModelIndex &miParent) const
{
    if (!miParent.isValid())
    {
	ATSModel* pSourceModel = static_cast<ATSModel*>(sourceModel());
	return pSourceModel->rootItem()->residualBands()->count();
    }

    // If parent is not the root item
    const ATSModelItem *pmiParent = static_cast<const ATSModelItem*>(miParent.internalPointer());
    if (pmiParent && pmiParent->type() == ATSModelItem::eResidualBand)
	// Column = property id
	return ATSResidualBandItem::nbProperties();

    return 0;
}

int ATSResidualsProxyModel::rowCount(const QModelIndex &miParent) const
{
    if (!miParent.isValid())
    {
	return 1; // Column = Residuals, 1 row => the partials list
    }

    // If parent is not the root item
    const ATSModelItem *pmiParent = static_cast<const ATSModelItem*>(miParent.internalPointer());
    if (pmiParent && pmiParent->type() == ATSModelItem::eResidualBand)
    {
	const ATSResidualBandItem *pmiPart = static_cast<const ATSResidualBandItem*>(pmiParent);
	return pmiPart->nbFrames();
    }

    return 0;
}

QModelIndex ATSResidualsProxyModel::parent(const QModelIndex &miChild) const
{
    return QModelIndex(); // 2 level model => always the same parent
}

// Get number of frames ====================================================
int ATSResidualsProxyModel::nbResiduals() const
{
    return static_cast<ATSModel*>(sourceModel())->rootItem()->residualBands()->count();
}

