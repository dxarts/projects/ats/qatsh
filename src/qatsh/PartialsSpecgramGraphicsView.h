// PartialsSpecgramGraphicsView class definition
//
//  The QGraphicsView-derived partials spectrogram view
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef PARTIALSSPECTRUMGRAPHICSVIEW_H
#define PARTIALSSPECTRUMGRAPHICSVIEW_H

#include <QGraphicsView>

class ATSModelManager;
class ColorMap;
class RulerSpec;


class PartialsSpecgramGraphicsView : public QGraphicsView
{
    Q_OBJECT

 public:

    PartialsSpecgramGraphicsView(QWidget *parent = 0);
    virtual ~PartialsSpecgramGraphicsView();

    void setModelManager(ATSModelManager* pModelMgr);

    // Set amplitude color contrast/brightness in [0, 1]
    void setAmplitudeContrast(double dContrast);
    void setAmplitudeBrightness(double dBrightness);
    
 signals:

    void colorMapChanged(const ColorMap* pColorMap);
    void timeRangeChanged(const RulerSpec* pRulerSpec);
    void frequencyRangeChanged(const RulerSpec* pRulerSpec);

 public slots:

    void zoomShiftTime(double dAbsFactor, double dAbsShiftValue);
    void zoomShiftFrequency(double dAbsFactor, double dAbsShiftValue);

 protected:

    virtual void resizeEvent(QResizeEvent* pqEvent);
    virtual void wheelEvent(QWheelEvent *pqEvent);
    virtual void keyPressEvent(QKeyEvent *pqEvent);

 private:

    // Flag for size/geometry/scene fit intialization
    bool _bInitialized;
};

#endif // PARTIALSSPECTRUMGRAPHICSVIEW_H
