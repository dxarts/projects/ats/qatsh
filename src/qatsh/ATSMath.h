// Wrapper for cmath include f
//
//  A wrapper for math.h include file, to add lacking functions.
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef ATSMath_h
#define ATSMath_h

#include <cmath>

// Here's a round function.
#ifdef _MSC_VER
inline int round(double x)
{
   return int(x > 0.0 ? x + 0.5 : x - 0.5);
}
#endif

#endif // ATSMath_h

