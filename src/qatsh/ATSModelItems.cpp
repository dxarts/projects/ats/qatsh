// ATSModel item classes implementation
//
// * ATSModelItem (base class)
//   * ATSFilePropertiesItem (file properties)
//   * ATSPartialItem (one partial with all its frames)
//   * ATSResidualBandItem (one residual band with all its frames)
//   * ATSRootItem (Root item of the model, agregates items from the 3 previous types)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include "ATSModelItems.h"


//====================================================================
ATSModelItem::ATSModelItem(EType eType, ATSSound* pATSSound)
: _eType(eType), _pATSSound(pATSSound)
{
}

ATSModelItem::EType ATSModelItem::type() const
{
    return _eType;
}

//====================================================================
ATSFilePropertiesItem::ATSFilePropertiesItem(ATSSound* pATSSound)
: ATSModelItem(ATSModelItem::eFileProperties, pATSSound)
{
}

int ATSFilePropertiesItem::nbProperties() const
{
    return eSoundPropNumber;
}

QVariant ATSFilePropertiesItem::property(ESoundPropertyId ePropId) const
{
    switch (ePropId)
    {
        case eSoundPropSamplingRate:
	    return _pATSSound->samplingRate();
    
        case eSoundPropFrameSize:
	    return _pATSSound->frameSize();
    
	case eSoundPropWindowSize:
	    return _pATSSound->windowSize();
    
	case eSoundPropNbFrames:
	    return _pATSSound->nbFrames();
    
	case eSoundPropNbPartials:
	    return _pATSSound->nbPartials();
    
	case eSoundPropNbResidualBands:
	    return _pATSSound->nbResidualBands();
    
	case eSoundPropMaxPartialAmplitude:
	    return _pATSSound->partialsMaxAmplitude();
    
	case eSoundPropMaxPartialFrequency:
	    return _pATSSound->partialsMaxFrequency();
    
	case eSoundPropMaxResidualEnergy:
	    return _pATSSound->residualBandsMaxEnergy();
    
	case eSoundPropMaxResidualFrequency:
	    return _pATSSound->residualBandsMaxFrequency();
    
	case eSoundPropDuration:
	    return _pATSSound->duration();
    
	case eSoundPropType:
	    return _pATSSound->type();

	case eSoundPropFileSize:
	    return _pATSSound->fileSize();

	// N/A.
	case eSoundPropNumber:
	    break;
    }

    return QVariant();
}


//====================================================================
ATSPartialItem::ATSPartialItem(ATSSound* pATSSound, int nPartIndex)
: ATSModelItem(ATSModelItem::ePartial, pATSSound), _nPartIndex(nPartIndex)
{
}

int ATSPartialItem::partialIndex() const
{
    return _nPartIndex;
}

int ATSPartialItem::nbFrames() const
{
    return _pATSSound->nbFrames();
}

int ATSPartialItem::nbProperties()
{
    return ePropNumber;
}

QVariant ATSPartialItem::property(int nFrameIndex, EPropertyId ePropId) const
{
    switch (ePropId)
    {
        case ePropFrequency:
	    return _pATSSound->partialFrequency(_nPartIndex, nFrameIndex);
    
	case ePropAmplitude:
	    return _pATSSound->partialAmplitude(_nPartIndex, nFrameIndex);
    
	case ePropPhase:
	    return _pATSSound->partialPhase(_nPartIndex, nFrameIndex);
    
	case ePropSMR:
	    return _pATSSound->partialSMR(_nPartIndex, nFrameIndex);

        case ePropTime:
	    return _pATSSound->partialTime(nFrameIndex);
    
	// N/A.
        case ePropNumber:
	    break;
    }

    return QVariant();
}

static const char* KPSZPartPropertyName[ATSPartialItem::ePropNumber] =
{
    "Frequency",
    "Amplitude",
    "Phase",
    "SMR",
    "Time"
};

const char* ATSPartialItem::propertyName(EPropertyId ePropId)
{
    return KPSZPartPropertyName[ePropId];
}

static const char* KPSZPartPropertyToolTip[ATSPartialItem::ePropNumber] =
{
    "Partial frequency (Hz)",
    "Partial amplitude (Unit ?)",
    "Partial phase (radians)",
    "Partial Signal to Mask Ratio",
    "Partial frame start time (s)"
};

const char* ATSPartialItem::propertyToolTip(EPropertyId ePropId)
{
    return KPSZPartPropertyToolTip[ePropId];
}


//====================================================================
ATSResidualBandItem::ATSResidualBandItem(ATSSound* pATSSound, int nBandIndex)
: ATSModelItem(ATSModelItem::eResidualBand, pATSSound), _nBandIndex(nBandIndex)
{
}

int ATSResidualBandItem::bandIndex() const
{
    return _nBandIndex;
}

int ATSResidualBandItem::nbFrames() const
{
    return _pATSSound->nbFrames();
}

int ATSResidualBandItem::nbProperties()
{
    return ePropNumber;
}

QVariant ATSResidualBandItem::property(int nFrameIndex, EPropertyId ePropId) const
{
    switch (ePropId)
    {
        case ePropMinFrequency:
	    return _pATSSound->residualBandMinFrequency(_nBandIndex);
    
	case ePropMaxFrequency:
	    return _pATSSound->residualBandMaxFrequency(_nBandIndex);
    
	case ePropEnergy:
	    return _pATSSound->residualBandEnergy(_nBandIndex, nFrameIndex);
    
        case ePropTime:
	    return _pATSSound->residualBandTime(nFrameIndex);
    
	// N/A.
        case ePropNumber:
	    break;
    }

    return QVariant();
}

static const char* KPSZBandPropertyName[ATSResidualBandItem::ePropNumber] =
{
    "Min. frequency",
    "Max. frequency",
    "Energy",
    "Time"
};

const char* ATSResidualBandItem::propertyName(EPropertyId ePropId)
{
    return KPSZBandPropertyName[ePropId];
}

static const char* KPSZResBandPropertyToolTip[ATSResidualBandItem::ePropNumber] =
{
    "Residual band minimum frequency (Hz)",
    "Residual band maximum frequency (Hz)",
    "Residual nabd energy (Unit ?)",
    "Residual band frame start time (s)"
};

const char* ATSResidualBandItem::propertyToolTip(EPropertyId ePropId)
{
    return KPSZResBandPropertyToolTip[ePropId];
}

//====================================================================
ATSPartialsItem::ATSPartialsItem(ATSSound* pATSSound)
: ATSModelItem(ATSModelItem::ePartials, pATSSound)
{
    for (int nPartIndex = 0; nPartIndex < _pATSSound->nbPartials(); nPartIndex++)
	_qlPartials.append(new ATSPartialItem(_pATSSound, nPartIndex));
}

ATSPartialsItem::~ATSPartialsItem()
{
    for (int nPartIndex = 0; nPartIndex < _qlPartials.size(); nPartIndex++)
    {
	if (_qlPartials[nPartIndex])
	{
	    delete _qlPartials[nPartIndex];
	    _qlPartials[nPartIndex] = 0;
	}
    }
}

int ATSPartialsItem::count() const
{
    return _qlPartials.size();
}

ATSPartialItem* ATSPartialsItem::partial(int nPartIndex)
{
    return _qlPartials[nPartIndex];
}

//====================================================================
ATSResidualBandsItem::ATSResidualBandsItem(ATSSound* pATSSound)
: ATSModelItem(ATSModelItem::eResidualBands, pATSSound)
{
    for (int nBandIndex = 0; nBandIndex < _pATSSound->nbResidualBands(); nBandIndex++)
        _qlResidualBands.append(new ATSResidualBandItem(_pATSSound, nBandIndex));
}

ATSResidualBandsItem::~ATSResidualBandsItem()
{
    for (int nBandIndex = 0; nBandIndex < _qlResidualBands.size(); nBandIndex++)
    {
        if (_qlResidualBands[nBandIndex])
        {
            delete _qlResidualBands[nBandIndex];
            _qlResidualBands[nBandIndex] = 0;
        }
    }
}

int ATSResidualBandsItem::count() const
{
    return _qlResidualBands.size();
}

ATSResidualBandItem* ATSResidualBandsItem::residualBand(int nBandIndex)
{
    return _qlResidualBands[nBandIndex];
}

//====================================================================
ATSRootItem::ATSRootItem(ATSSound* pATSSound)
: ATSModelItem(ATSModelItem::eRoot, pATSSound)
{
    _pmiFileProps = new ATSFilePropertiesItem(_pATSSound);
    _pmiPartials = new ATSPartialsItem(_pATSSound);
    _pmiResidualBands  = new ATSResidualBandsItem(_pATSSound);
}

ATSRootItem::~ATSRootItem()
{
    if (_pmiFileProps)
    {
        delete _pmiFileProps;
        _pmiFileProps = 0;
    }
    if (_pmiPartials)
    {
        delete _pmiPartials;
        _pmiPartials = 0;
    }
    if (_pmiResidualBands)
    {
        delete _pmiResidualBands;
        _pmiResidualBands = 0;
    }
}

void ATSRootItem::resetPartials()
{
    if (_pmiPartials)
    {
        delete _pmiPartials;
        _pmiPartials = 0;
    }
   
    _pmiPartials = new ATSPartialsItem(_pATSSound);
}

ATSFilePropertiesItem* ATSRootItem::fileProperties()
{
    return _pmiFileProps;
}

ATSPartialsItem* ATSRootItem::partials()
{
    return _pmiPartials;
}

ATSResidualBandsItem* ATSRootItem::residualBands()
{
    return _pmiResidualBands;
}
