// SynthesisParamsWidget class implementation
//
//  The ATS synthesis parameters widget
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <QPushButton>
#include <QDoubleValidator>

#include "SynthesisParamsWidget.h"
#include "ui_SynthesisParamsWidget.h"

#include "ATSSynthesis.h"


SynthesisParamsWidget::SynthesisParamsWidget(QWidget *parent)
: QWidget(parent), _pui(new Ui::SynthesisParamsWidget), _dMaxDuration(1.0)
{
    _pui->setupUi(this);

    // Initialize synthesis params and dialog controls
    _pSynthesis = 0;
    onRestoreDefaults();
}

SynthesisParamsWidget::~SynthesisParamsWidget()
{
    delete _pui;
	delete _pSynthesis;
}

void SynthesisParamsWidget::changeEvent(QEvent *pqEvent)
{
    QWidget::changeEvent(pqEvent);
    switch (pqEvent->type()) {
    case QEvent::LanguageChange:
        _pui->retranslateUi(this);
        break;
    default:
        break;
    }
}

//====================================================================================
void SynthesisParamsWidget::onRestoreDefaults()
{
    ATSSynthesis synt; // A standard synthesis with default parameters.

    _pui->qsbStartTime->setValue(0.0);
    _pui->qsbDuration->setValue(_dMaxDuration);
    _pui->qrbAllPartials->setChecked(!synt.useOnlySelectedPartials());
    _pui->qrbUsePhase->setChecked(synt.usePartialsPhase());
    _pui->qsbPartAmplFactor->setValue(synt.partialsAmplitudeFactor());
    _pui->qsbResidAmplFactor->setValue(synt.residualAmplitudeFactor());
    _pui->qsbFreqFactor->setValue(synt.frequencyFactor());
    _pui->qsbSamplingRate->setValue(synt.samplingRate());
}

//====================================================================================
void SynthesisParamsWidget::setMaxDuration(double dMaxDuration)
{
    // Save new max duration.
    _dMaxDuration = dMaxDuration;

    // Reset start time and duration.
    _pui->qsbStartTime->setValue(0.0);
    _pui->qsbDuration->setValue(_dMaxDuration);
    
    // Enforce max duration constraint by spin-boxes.
    _pui->qsbStartTime->setMaximum(_dMaxDuration*.999);
    _pui->qsbDuration->setMaximum(_dMaxDuration);
}

//====================================================================================
void SynthesisParamsWidget::setupSynthesis()
{
    if (!_pSynthesis)
	_pSynthesis = new ATSSynthesis();

    _pSynthesis->setStartTime(_pui->qsbStartTime->value());
    _pSynthesis->setDuration(_pui->qsbDuration->value());
    _pSynthesis->setUseOnlySelectedPartials(!_pui->qrbAllPartials->isChecked());
    _pSynthesis->setUsePartialsPhase(_pui->qrbUsePhase->isChecked());
    _pSynthesis->setPartialsAmplitudeFactor(_pui->qsbPartAmplFactor->value());
    _pSynthesis->setResidualAmplitudeFactor(_pui->qsbResidAmplFactor->value());
    _pSynthesis->setFrequencyFactor(_pui->qsbFreqFactor->value());
    _pSynthesis->setSamplingRate(_pui->qsbSamplingRate->value());
}

ATSSynthesis* SynthesisParamsWidget::synthesis()
{
    return _pSynthesis;
}
