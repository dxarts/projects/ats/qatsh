// ResidualsFrameTableView class definition
//
//  The table view for 1 frame of the residual bands.
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef ResidualsFrameTableView_h
#define ResidualsFrameTableView_h

#include <QFrame>

namespace Ui {
    class ResidualsFrameTableView;
}

class ATSModelManager;
class QItemSelection;


class ResidualsFrameTableView : public QFrame 
{
    Q_OBJECT
    Q_DISABLE_COPY(ResidualsFrameTableView)

 public:

    explicit ResidualsFrameTableView(QWidget *parent = 0);
    virtual ~ResidualsFrameTableView();

    void setModelManager(ATSModelManager* pModelMgr);

    int currentFrameIndex() const;

 signals:

    void currentFrameChanged(int nNewIndex);

 private slots:

    // Slots to synchronize control values
    void onCurrentIndexChanged(int nNewIndex);
    void onCurrentStartTimeChanged(double dNewTime);
    void onCurrentEndTimeChanged(double dNewTime);

    void onSelectionChanged(const QItemSelection& qisSelected, 
			    const QItemSelection& qisDeselected);
    
 private:

    Ui::ResidualsFrameTableView *_pui;

    // Model manager.
    ATSModelManager *_pModelMgr;

    // Convenience copies of model data.
    int _nMaxFrameIndex;
    double _dFrameDuration;
    
    // Sliders / Spin-boxes syncronization lock.
    bool _bSynchronizeControls;
};

#endif // ResidualsFrameTableView_h
