// Ruler class definition
//
//  A QWidget displaying a ruler and allowing user zooming and shifting
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef RULER_H
#define RULER_H

#include <QWidget>

class RulerSpec;


class Ruler : public QWidget
{
    Q_OBJECT

 public:

    Ruler(QWidget *parent = 0);

    void zoom(double dRelativeAnchor, double dRelativeFactor);
    void zoomAll();

 signals:

    void zoomShift(double dRelFactor, double dShiftValue);

 public slots:

    void onSpecChanged(const RulerSpec* pSpec);

 protected:

    virtual void mousePressEvent(QMouseEvent *pqEvent);
    virtual void mouseMoveEvent(QMouseEvent *pqEvent);
    virtual void mouseReleaseEvent(QMouseEvent *pqEvent);
    virtual void wheelEvent(QWheelEvent* pqEvent);
    virtual void paintEvent(QPaintEvent* pqEvent);
    virtual void resizeEvent(QResizeEvent *pqEvent);

    void propagateZoomShift(int nViewLength);
    void zoom(const QPoint& qpAnchorPos, double dRelativeFactor);
    void shift(const QPoint& qpNewPos);
    void resize(const QSize& qSize);

    void draw(QPainter& qPainter);

 private:

    // Specification of the ruler.
    const RulerSpec* _pSpec;

    // Shifting (=translating) data
    bool _bShifting;
    QPoint _qpLastShiftingPos;

    // Current absolute zoom factor
    double _dZoomFactor;

    // Current offset from rulerSpec.min
    double _dValueOffset;
};

#endif // RULER_H
