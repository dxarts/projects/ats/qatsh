// DataSelectionWidget class definition
//
//  The partials / residual bands selector widget
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef DataSelectionWidget_h
#define DataSelectionWidget_h

#include <QWidget>

#include "TypesAndConstants.h"

namespace Ui {
    class DataSelectionWidget;
}

class ATSModelManager;


class DataSelectionWidget : public QWidget 
{
    Q_OBJECT
    Q_DISABLE_COPY(DataSelectionWidget)

public:

    explicit DataSelectionWidget(QWidget *parent = 0);
    virtual ~DataSelectionWidget();

    void resetControls();

    void switchDataType(TypesAndConstants::EDataType eDataType);
    
    void setModelManager(ATSModelManager* pModelMgr);

    // Get/Set selector state
    struct SState 
    { 
	int nFromIndex;
	int nToIndex;
	int nIndexStep;

	int nValueTypeIndex;
	bool bUseMinValue;
	double dMinValue;
	bool bUseMaxValue;
	double dMaxValue;

	SState() : nFromIndex(0), nToIndex(-1), nIndexStep(1),
	           nValueTypeIndex(0), 
		   bUseMinValue(true), dMinValue(-30), 
		   bUseMaxValue(false), dMaxValue(0) {};
    };

    SState getState() const;
    void setState(const SState& state);

    typedef enum { ePeakAmplitude=0, eRMSPowerAmplitude,
		   eSignalToMaskRatio, eFrequency, eThresholdTypeNumber } EValueThresholdType;

signals:

    void dataIndexSelectionChanged(TypesAndConstants::EDataType eDataType, 
				   int nNewFromIndex, int nNewToIndex);

    void dataValueSelectionChanged(TypesAndConstants::EDataType eDataType, 
				   EValueThresholdType eThreshType,
				   bool bUseMinValue, double dMinValue,
				   bool bUseMaxValue, double dMaxValue);
protected:

    virtual void changeEvent(QEvent *e);

private slots:

    // Slots to synchronize with model
    void onModelReset();

    // Slots to synchronize control values
    void onFromIndexChanged(int nNewIndex);
    void onToIndexChanged(int nNewIndex);
    void onUseMinValueChanged(int nNewState);
    void onUseMaxValueChanged(int nNewState);
    void onValueTypeChanged(int nNewIndex);

private:

    Ui::DataSelectionWidget *_pui;

    // Model information.
    ATSModelManager* _pModelMgr;

    // Convenience copies of model data.
    TypesAndConstants::EDataType _eDataType;
    int _nMaxItemIndex;

    // Form / to sliders / spin-boxes syncronization lock.
    bool _bSynchronizeControls;
};

#endif // DataSelectionWidget_h
