// TableAndToolsWidget class definition
//
//  The left widget that holds :
//  - the table views for partials and residual frames properties
//  - the tab widget holding the frame selector and threshold selector tools
//  - the frame properties editor tool
//  - the synthetizer tool
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef TableAndToolsWidget_h
#define TableAndToolsWidget_h

#include <QWidget>

#include "TypesAndConstants.h"

#include "FrameSelectionWidget.h"
#include "DataSelectionWidget.h"
#include "SampledSound.h"

namespace Ui {
    class TableAndToolsWidget;
}

class ATSModelManager;
class IATSAnalysis;
class ATSSynthesis;
class FrameAndDataProcessor;


class TableAndToolsWidget : public QWidget 
{
    Q_OBJECT
    Q_DISABLE_COPY(TableAndToolsWidget)

 public:

    explicit TableAndToolsWidget(QWidget *parent = 0);
    virtual ~TableAndToolsWidget();

    void setModelManager(ATSModelManager* pModelMgr);

    // Switch to the given tools widget (in the top level stacked widget)
    typedef enum { eSelectionEdition = 0, eAnalysis, eSynthesis, eToolsNumber } ETool;
    void switchTool(ETool eTool);

    // Switch to given data table view (in the SelectorEditor stacked widget)
    void switchSelectEditDataList(TypesAndConstants::EDataType eDataType);

    // Enable/disable analysis parameters dialog.
    void enableAnalysis(bool bOnOff);

    // Set the sample file for analysis.
    void setAnalysisSampleFile(const QString& qsFileName);

     // Set the parameters for analysis.
    void setAnalysisParams(const IATSAnalysis* pAnaParams);

   // Set the sample file for analysis.
    void setSynthesisSampleFile(const QString& qsFileName);

    void getSynthesisParams(QString& qsFileName, SampledSound::ESampleFormat& eFormat,
							ATSSynthesis*& pSynthesis);
 signals:

    void reAnalyse(const QString& qsFileName, IATSAnalysis* pAnaParams);

    void reSynthesise();
    void startPlayingSynthesisResult();
    void stopPlayingSynthesisResult();

 protected:

    virtual void changeEvent(QEvent *e);

 private slots:

    void onModelReset();

    // Frame selector, partials/residual table and editor widget
    void onFirstSelectedFrameChanged(int nNewIndex);
    void onLastSelectedFrameChanged(int nNewIndex);

    // Analysis params widget.
    void onChangeAnalysisSampleFile();
    void onApplyAnalysisParams();

    // Synthesis params widget.
    void onChangeSynthesisSampleFile();
    void onApplySynthesisParams();
    void onStartPlayingSynthesisResult();
    void onStopPlayingSynthesisResult();

 private:

    Ui::TableAndToolsWidget *_pui;

    // Model manager.
    ATSModelManager* _pModelMgr;

    // Currently "displayed" data type (partials or residual) in the SelectEdit tool.
    TypesAndConstants::EDataType _eCurrentDataType;

    // States for frame and data selectors for each possible data type.
    FrameSelectionWidget::SState _frameSelectStates[TypesAndConstants::eDataTypeNumber];
    DataSelectionWidget::SState _dataSelectStates[TypesAndConstants::eDataTypeNumber];

    // Frame and data processor (for editor tool).
    FrameAndDataProcessor* _pProcessor;
    
    // Sliders / Spin-boxes syncronization lock.
    bool _bSynchronizeControls;
};

#endif // TableAndToolsWidget_h
