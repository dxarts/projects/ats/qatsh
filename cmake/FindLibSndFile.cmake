# Find libsndfile.
# Written by Jeremy Tregunna <jeremy.tregunna@me.com>
# Grabbed from the Io language" project :
#   http://code.ohloh.net/project?pid=hdDtNKDX0FI&browser=Default&did=modules&cid=BtNjAy9sPF4
# Input vars:
# * LIBSNDFILE_NAMES : suggested lib names
# * 
# Output vars:
# * LIBSNDFILE_FOUND : True if found, False otherwise.
# * LIBSNDFILE_INCLUDE_DIR : If found, include dir.
# * LIBSNDFILE_LIBRARY : If found, library file.

set(_pfx86 "ProgramFiles(x86)") # due to policy CMP0053

# Try and find header file.
FIND_PATH(LIBSNDFILE_INCLUDE_DIR
          NAMES
            sndfile.h
          PATH_SUFFIXES 
            include
          PATHS
            ~/Library/Frameworks /Library/Frameworks
            /usr/local /usr
            /sw # Fink
            /opt/local # DarwinPorts
            /opt/csw # Blastwave
            /opt
            "$ENV{ProgramFiles}\\libsndfile"
            "$ENV{${_pfx86}}\\libsndfile"
            "$ENV{ProgramFiles}\\Mega-Nerd\\libsndfile" 
            "$ENV{${_pfx86}}\\Mega-Nerd\\libsndfile")

#MESSAGE(STATUS "LIBSNDFILE_INCLUDE_DIR=${LIBSNDFILE_INCLUDE_DIR}")

# Add standard hints for library name.
SET(LIBSNDFILE_NAMES ${LIBSNDFILE_NAMES} sndfile libsndfile libsndfile-1)

# Try and find the library file.
FIND_LIBRARY(LIBSNDFILE_LIBRARY 
             NAMES 
               ${LIBSNDFILE_NAMES}
             PATH_SUFFIXES 
               lib64 lib libs64 libs libs/Win32 libs/Win64
             PATHS
               ~/Library/Frameworks /Library/Frameworks
               /usr/local /usr
               /sw # Fink
               /opt/local  # DarwinPorts
               /opt/csw  # Blastwave
               /opt
               "$ENV{ProgramFiles}\\libsndfile" 
               "$ENV{${_pfx86}}\\libsndfile"
               "$ENV{ProgramFiles}\\Mega-Nerd\\libsndfile" 
               "$ENV{${_pfx86}}\\Mega-Nerd\\libsndfile")

#MESSAGE(STATUS "LIBSNDFILE_LIBRARY=${LIBSNDFILE_LIBRARY}")

IF(LIBSNDFILE_INCLUDE_DIR AND LIBSNDFILE_LIBRARY)
	SET(LIBSNDFILE_FOUND TRUE)
	MARK_AS_ADVANCED(LIBSNDFILE_LIBRARY LIBSNDFILE_INCLUDE_DIR LibSndFile_DIR)
ENDIF(LIBSNDFILE_INCLUDE_DIR AND LIBSNDFILE_LIBRARY)

IF(LIBSNDFILE_FOUND)
	IF(NOT LibSndFile_FIND_QUIETLY)
		MESSAGE(STATUS "Found LibSndFile: ${LIBSNDFILE_LIBRARY}")
	ENDIF (NOT LibSndFile_FIND_QUIETLY)
ELSE(LIBSNDFILE_FOUND)
	IF(LibSndFile_FIND_REQUIRED)
		MESSAGE(FATAL_ERROR "Could not find sndfile")
	ENDIF(LibSndFile_FIND_REQUIRED)
ENDIF (LIBSNDFILE_FOUND)
