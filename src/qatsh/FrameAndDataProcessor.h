// FrameAndDataProcessor class definition
//
//  The class that runs the automatic changes to selected frames and data
//  that are specified by the user through various tools
//  - add a new partial / remove selected ones
//  - apply a formula to a given magnitude of the selected frames and data 
//  - normalize selected frames and data amplitude
//  - ...
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef FRAMEANDDATAPROCESSOR_H
#define FRAMEANDDATAPROCESSOR_H

//#include "TypesAndConstants.h"

class ATSModel;


class FrameAndDataProcessor
{
public:

    explicit FrameAndDataProcessor(ATSModel* pModel);
    virtual ~FrameAndDataProcessor();

//    void addPartial(int nSrcPartIndex = -1);
//    void removePartials(const int aPartIndexes[], unsigned int nParts);
    
private:

    // Model information.
    ATSModel* _pATSModel;
};

#endif // FRAMEANDDATAPROCESSOR_H
