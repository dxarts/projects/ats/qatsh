// FrameSelectionWidget class definition
//
//  The frame selection widget for partials and residual
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef FrameSelectionWidget_h
#define FrameSelectionWidget_h

#include <QWidget>

#include "TypesAndConstants.h"

namespace Ui {
    class FrameSelectionWidget;
}

class ATSModelManager;


class FrameSelectionWidget : public QWidget 
{
    Q_OBJECT
    Q_DISABLE_COPY(FrameSelectionWidget)

public:

    explicit FrameSelectionWidget(QWidget *parent = 0);
    virtual ~FrameSelectionWidget();

    void switchDataType(TypesAndConstants::EDataType eDataType);
    
    void setModelManager(ATSModelManager* pModelMgr);
    
    
    // Get/Set selector state
    struct SState
    { 
	int nFromFrameIndex;
	int nToFrameIndex;

	SState() : nFromFrameIndex(0), nToFrameIndex(-1) {};
    };

    SState getState() const;
    void setState(const SState& state);

signals:

    void frameSelectionChanged(int nNewFromIndex, int nNewToIndex);

public slots:

    void onCurrentIndexChanged(int nNewIndex);

protected:

    virtual void changeEvent(QEvent *e);

private slots:

    // Slots to synchronize control values
    void onFromIndexChanged(int nNewIndex);
    void onToIndexChanged(int nNewIndex);

    void onFromTimeChanged(double dNewTime);
    void onToTimeChanged(double dNewTime);

    // Slots for current to from/to user synchronization buttons
    void onSetCurrentToFrom();
    void onSetCurrentToTo();

private:

    Ui::FrameSelectionWidget *_pui;

    // Model information.
    ATSModelManager* _pModelMgr;

    // Convenience copies of model data.
    TypesAndConstants::EDataType _eDataType;
    int _nMaxFrameIndex;
    double _dFrameDuration;
    
    // Sliders / Spin-boxes syncronization lock.
    bool _bSynchronizeControls;

    // Current frame (used for "setFromCurrent" buttons
    int _nCurrentFrameIndex;
};

#endif // FrameSelectionWidget_h
