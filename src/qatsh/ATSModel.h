// ATSModel class definition
//
//  The main and raw ATS document "model" (in the Qt 4 meaning)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef ATSMODEL_H
#define ATSMODEL_H

#include <QAbstractItemModel>

class ATSRootItem;
class ATSSound;

class ATSModel : public QAbstractItemModel
{
    Q_OBJECT

public:

    ATSModel(QObject *parent = 0);
    virtual ~ATSModel();

    // Model re-initialization.
    enum EReInitType { eEverything, ePartialsListOnly };
    void reInitialize(EReInitType eReInitType = eEverything);
    
    // QAbstractItemModel implementation
    Qt::ItemFlags flags(const QModelIndex &index) const;

    virtual QModelIndex index(int row, int column,
			      const QModelIndex &parent = QModelIndex()) const;

    virtual QVariant data(const QModelIndex &index, int role) const;

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;

    //virtual QVariant headerData(int section, Qt::Orientation orientation,
    //		int role = Qt::DisplayRole) const;

    virtual QModelIndex parent(const QModelIndex &index) const;

    // .ats files input/output
    bool load(const QString& qsATSFileName);
    bool store(const QString& qsATSFileName) const;

    // Acessor to the root item.
    ATSRootItem* rootItem();

    // Accessors to the ATS sound.
    void setSound(ATSSound* pSound);
    ATSSound* sound();

private:

    ATSSound* _pATSSound;

    ATSRootItem* _pmiRootItem;
};

#endif // ATSMODEL_H
