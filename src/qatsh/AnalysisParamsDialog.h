// AnalysisParamsDialog class definition
//
//  The analysis parameters dialog
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef AnalysisParamsDialog_h
#define AnalysisParamsDialog_h

#include <QDialog>

namespace Ui {
    class AnalysisParamsDialog;
}

class IATSAnalysis;

class AnalysisParamsDialog : public QDialog
{
    Q_OBJECT
    Q_DISABLE_COPY(AnalysisParamsDialog)

 public:

    explicit AnalysisParamsDialog(QWidget *parent = 0);
    virtual ~AnalysisParamsDialog();

    IATSAnalysis* analysisParams();

 protected:

    virtual void changeEvent(QEvent *pqEvent);

 public slots:

    virtual void accept();

 private:

    Ui::AnalysisParamsDialog *_pui;
};

#endif // AnalysisParamsDialog_h
