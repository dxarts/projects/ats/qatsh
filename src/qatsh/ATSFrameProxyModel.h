// ATSFrameProxyModel abstract base class declaration
//
//  Abstract proxy model for ATS partials/residual per frame data
//  (source model = ATSDataProxyModel)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef ATSFRAMEDATAPROXYMODEL_H
#define ATSFRAMEDATAPROXYMODEL_H

#include <QAbstractProxyModel>
#include <QModelIndex>


class ATSFrameProxyModel : public QAbstractProxyModel
{
    Q_OBJECT

 public:

    ATSFrameProxyModel(QObject *parent = 0);
    virtual ~ATSFrameProxyModel();

    virtual void setSourceModel(QAbstractItemModel* pSourceModel);

    // Set the currently selected frame in the main model.
    void setCurrentFrame(int nFrameIndex);

    // Number of selectable frames in the main model (0 <= nFrameIndex < nbFrames).
    int nbFrames() const;

    // Frame duration (s)
    double frameDuration() const;

 protected slots:

    void onSourceModelReset();

 protected:

    int _nCurrFrameIndex;
};

#endif // ATSFRAMEDATAPROXYMODEL_H
