// FrameSelectionWidget class definition
//
//  The frame selection widget for partials and residual
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <iostream>

#include "ATSMath.h"
#include "ATSModelManager.h"
#include "ATSPartialsFrameProxyModel.h"
#include "ATSResidualsFrameProxyModel.h"
#include "ATSPartialsProxyModel.h"
#include "ATSResidualsProxyModel.h"

#include "FrameSelectionWidget.h"
#include "ui_FrameSelectionWidget.h"


FrameSelectionWidget::FrameSelectionWidget(QWidget *parent)
: QWidget(parent), _pui(new Ui::FrameSelectionWidget), _pModelMgr(0),
  _eDataType(TypesAndConstants::ePartial), _nMaxFrameIndex(0), _dFrameDuration(0),
  _nCurrentFrameIndex(0)
{
    _pui->setupUi(this);

    // Enable control value synchronization
    _bSynchronizeControls = true;

    // Connect controls to local changed() slots for synchronisation
    // 1) Index sliders
    connect(_pui->qsdFromIndex, SIGNAL(valueChanged(int)),
            this, SLOT(onFromIndexChanged(int)));
    connect(_pui->qsdToIndex, SIGNAL(valueChanged(int)),
            this, SLOT(onToIndexChanged(int)));

    // 2) Index spin-boxes
    connect(_pui->qsbFromIndex, SIGNAL(valueChanged(int)),
            this, SLOT(onFromIndexChanged(int)));
    connect(_pui->qsbToIndex, SIGNAL(valueChanged(int)),
            this, SLOT(onToIndexChanged(int)));

    // 3) Time spin-boxes
    connect(_pui->qsbFromTime, SIGNAL(valueChanged(double)),
            this, SLOT(onFromTimeChanged(double)));
    connect(_pui->qsbToTime, SIGNAL(valueChanged(double)),
            this, SLOT(onToTimeChanged(double)));

    // Connect current to from/to user synchronization buttons to local slots
    connect(_pui->qpbSetFromFromCurrent, SIGNAL(clicked()),
	    this, SLOT(onSetCurrentToFrom()));
    connect(_pui->qpbSetToFromCurrent, SIGNAL(clicked()),
	    this, SLOT(onSetCurrentToTo()));
}

FrameSelectionWidget::~FrameSelectionWidget()
{
    delete _pui;
}

// Model management ==================================================
void FrameSelectionWidget::setModelManager(ATSModelManager* pModelMgr)
{
    _pModelMgr = pModelMgr;
}

void FrameSelectionWidget::switchDataType(TypesAndConstants::EDataType eDataType)
{
    // Save new current data type.
    _eDataType = eDataType;

    // Determine new current number of data items.
    switch(eDataType)
    {
        case TypesAndConstants::ePartial:
	    _nMaxFrameIndex = _pModelMgr->partialsFrameModel()->nbFrames() - 1;
	    _dFrameDuration = _pModelMgr->partialsFrameModel()->frameDuration();
	    break;
        case TypesAndConstants::eResidualBand:
	    _nMaxFrameIndex = _pModelMgr->residualsFrameModel()->nbFrames() - 1;
	    _dFrameDuration = _pModelMgr->residualsFrameModel()->frameDuration();
	    break;
        default:
	    break;
    }

    // Reset maximum value for sliders
    _pui->qsdFromIndex->setMaximum(_nMaxFrameIndex);
    _pui->qsdToIndex->setMaximum(_nMaxFrameIndex);

    // Reset maximum value for index spin-boxes
    _pui->qsbFromIndex->setMaximum(_nMaxFrameIndex);
    _pui->qsbToIndex->setMaximum(_nMaxFrameIndex);

    // Reset minimum/maximum/step value for time spin-boxes
    _pui->qsbFromTime->setSingleStep(_dFrameDuration);
    _pui->qsbFromTime->setMaximum(_nMaxFrameIndex*_dFrameDuration);

    _pui->qsbToTime->setSingleStep(_dFrameDuration);
    _pui->qsbToTime->setMinimum(_dFrameDuration);
    _pui->qsbToTime->setMaximum((_nMaxFrameIndex+1)*_dFrameDuration);
}

// State management ==================================================
FrameSelectionWidget::SState FrameSelectionWidget::getState() const
{
    // Get state from controls
    SState state;

    state.nFromFrameIndex = _pui->qsbFromIndex->value();
    state.nToFrameIndex = _pui->qsbToIndex->value();

    return state;
}

void FrameSelectionWidget::setState(const SState& state)
{
    // Check/fix/initialize the selector state as needed.
    SState fixedState = state;
    if (fixedState.nToFrameIndex < 0 || fixedState.nToFrameIndex > _nMaxFrameIndex)
	fixedState.nToFrameIndex = _nMaxFrameIndex;
    if (fixedState.nFromFrameIndex > fixedState.nToFrameIndex)
	fixedState.nFromFrameIndex = fixedState.nToFrameIndex;

    // Set state to index spin-boxes (auto-syncronization will do its job).
    _pui->qsbFromIndex->setValue(fixedState.nFromFrameIndex);
    _pui->qsbToIndex->setValue(fixedState.nToFrameIndex);
}

// Slots to update current frame index ==============================
void FrameSelectionWidget::onCurrentIndexChanged(int nNewIndex)
{
    std::cout << "FrameSelectionWidget::onCurrentIndexChanged(" 
	      << nNewIndex << ")" << std::endl;
    _nCurrentFrameIndex = nNewIndex;
}

// Slots to synchronize control values ===============================
void FrameSelectionWidget::onFromIndexChanged(int nNewIndex)
{
    // Nothing to do if control synchronization is blocked.
    if (!_bSynchronizeControls)
	return;

    // Synchronize attached controls.
    _bSynchronizeControls = false;
    _pui->qsbFromIndex->setValue(nNewIndex);
    _pui->qsdFromIndex->setValue(nNewIndex);
    _pui->qsbFromTime->setValue(nNewIndex*_dFrameDuration);
    _bSynchronizeControls = true;

    // From can't be greater than To
    if (nNewIndex > _pui->qsbToIndex->value())
	_pui->qsbToIndex->setValue(nNewIndex);

    emit frameSelectionChanged(nNewIndex, _pui->qsbToIndex->value());    
}

void FrameSelectionWidget::onToIndexChanged(int nNewIndex)
{
    // Nothing to do if control synchronization is blocked.
    if (!_bSynchronizeControls)
	return;

    // Synchronize attached controls.
    _bSynchronizeControls = false;
    _pui->qsbToIndex->setValue(nNewIndex);
    _pui->qsdToIndex->setValue(nNewIndex);
    _pui->qsbToTime->setValue((nNewIndex+1)*_dFrameDuration);
    _bSynchronizeControls = true;

    // To can't be lower than From
    if (nNewIndex < _pui->qsbFromIndex->value())
	_pui->qsbFromIndex->setValue(nNewIndex);

    emit frameSelectionChanged(_pui->qsbFromIndex->value(), nNewIndex);    
}

void FrameSelectionWidget::onFromTimeChanged(double dNewTime)
{
    // Nothing to do if control synchronization is blocked.
    if (!_bSynchronizeControls)
	return;

    // Synchronize attached controls.
    _bSynchronizeControls = false;
    const int nFrameIndex = (int)round(dNewTime / _dFrameDuration);

    _pui->qsbFromIndex->setValue(nFrameIndex);
    _pui->qsdFromIndex->setValue(nFrameIndex);
    _bSynchronizeControls = true;

    if (nFrameIndex > _pui->qsbToIndex->value())
	_pui->qsbToIndex->setValue(nFrameIndex);

    emit frameSelectionChanged(nFrameIndex, _pui->qsbToIndex->value());    
}

void FrameSelectionWidget::onToTimeChanged(double dNewTime)
{
    // Nothing to do if control synchronization is blocked.
    if (!_bSynchronizeControls)
	return;

    // Synchronize attached controls.
    _bSynchronizeControls = false;
    const int nFrameIndex = (int)round(dNewTime / _dFrameDuration) - 1;

    _pui->qsbToIndex->setValue(nFrameIndex);
    _pui->qsdToIndex->setValue(nFrameIndex);
    _bSynchronizeControls = true;

    if (nFrameIndex < _pui->qsbFromIndex->value())
	_pui->qsbFromIndex->setValue(nFrameIndex);

    emit frameSelectionChanged(_pui->qsbFromIndex->value(), nFrameIndex);    
}

// Slots for current to from/to user synchronization buttons =========
void FrameSelectionWidget::onSetCurrentToFrom()
{
    _pui->qsbFromIndex->setValue(_nCurrentFrameIndex);
}

void FrameSelectionWidget::onSetCurrentToTo()
{
    _pui->qsbToIndex->setValue(_nCurrentFrameIndex);
}

// Event management ==================================================
void FrameSelectionWidget::changeEvent(QEvent *e)
{
    switch (e->type()) {
    case QEvent::LanguageChange:
        _pui->retranslateUi(this);
        break;
    default:
        break;
    }
}

