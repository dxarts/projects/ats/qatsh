// StandardAnalysis class implementation
//
//  The "standard" ATS analysis (as originaly implemented in ATSH)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include "StandardAnalysis.h"

#include "SampledSound.h"


StandardAnalysis::StandardAnalysis(double dStartTime, double dDuration, 
				   double dMinFreq, double dMaxFreq, double dMaxFreqDeviation, 
				   double dAmpThreshold, 
				   int nFFTWinType, int nFFTWinMinFreqCycles, 
				   double dFFTWinOverlap, 
				   int nTrackWinLen, int nMinTrackLen, double dMinTrackAvgSMR, 
				   int nMaxFixedGapsLen, double dMinFixedGapsSMR, 
				   double dLastPeakWeight, double dSMRFreqWeight, 
				   ATSSound::ESoundTypeId eTargetSoundType)
{
    // Save user parameters (others fields are computed during analysis).
    setStartTime(dStartTime);
    setDuration(dDuration);
    setMinFreq(dMinFreq);
    setMaxFreq(dMaxFreq);
    setMaxFreqDeviation(dMaxFreqDeviation);
    setAmpThreshold(dAmpThreshold);
    setFFTWinType(nFFTWinType);
    setFFTWinMinFreqCycles(nFFTWinMinFreqCycles);
    setFFTWinOverlap(dFFTWinOverlap);
    setTrackWinLen(nTrackWinLen);
    setMinTrackLen(nMinTrackLen);
    setMinTrackAverageSMR(dMinTrackAvgSMR);
    setMaxFixedGapsLen(nMaxFixedGapsLen);
    setMinFixedGapsSMR(dMinFixedGapsSMR);
    setLastPeakWeight(dLastPeakWeight);
    setSMRFreqWeight(dSMRFreqWeight);
    setTargetSoundType(eTargetSoundType);
}

StandardAnalysis::~StandardAnalysis()
{
}

// Implementation of IATSAnalysis ===========================================
const QString StandardAnalysis::typeName()
{
    return "Standard";
}

const QString StandardAnalysis::type() const
{
    return typeName();
}

ATSSound* StandardAnalysis::operator()(SampledSound* pSampSound)
{
    // TODO : Merge multiple channels, here, or upwards, when loading sampled sound.
    const int nSampRate = pSampSound->samplingRate();
    const int nSampFrames = pSampSound->nbFrames();

    double* adSamples = pSampSound->channel(0);

    double** adResidual = 0;
    int nResidualFrames = 0;

    dumpParams(std::cout);

    ATS_SOUND* pSoundData = tracker(&_params, adSamples, nSampRate, nSampFrames, 
                             &adResidual, &nResidualFrames);

    ATSSound* pATSSound = new ATSSound(pSoundData);

    // TODO : Do something with adResidual

    return pATSSound;
}

// Accessors ================================================================
double StandardAnalysis::startTime() const
{
    return (double)_params.start;
}

void StandardAnalysis::setStartTime(double dValue)
{
    _params.start = dValue;
}

double StandardAnalysis::duration() const
{
    return _params.duration;
}

void StandardAnalysis::setDuration(double dValue)
{
    _params.duration = dValue;
}

double StandardAnalysis::minFreq() const
{
    return _params.lowest_freq;
}

void StandardAnalysis::setMinFreq(double dValue)
{
    _params.lowest_freq = dValue;
}

double StandardAnalysis::maxFreq() const
{
    return _params.highest_freq;
}

void StandardAnalysis::setMaxFreq(double dValue)
{
    _params.highest_freq = dValue;
}

double StandardAnalysis::maxFreqDeviation() const
{
    return _params.freq_dev * 100;
}

void StandardAnalysis::setMaxFreqDeviation(double dValue)
{
    _params.freq_dev = dValue / 100;
}

double StandardAnalysis::ampThreshold() const
{
    return _params.lowest_mag;
}

void StandardAnalysis::setAmpThreshold(double dValue)
{
    _params.lowest_mag = dValue;
}

int StandardAnalysis::FFTWinType() const
{
    return (int)_params.win_type;
}

void StandardAnalysis::setFFTWinType(int nValue)
{
    _params.win_type = nValue;
}

int StandardAnalysis::FFTWinMinFreqCycles() const
{
    return _params.win_cycles;
}

void StandardAnalysis::setFFTWinMinFreqCycles(int nValue)
{
    _params.win_cycles = nValue;
}

double StandardAnalysis::FFTWinOverlap() const
{
    return (1.0 - _params.hop_size) * 100;
}

void StandardAnalysis::setFFTWinOverlap(double dValue)
{
    _params.hop_size = 1.0 - dValue / 100;
}

int StandardAnalysis::trackWinLen() const
{
    return _params.track_len;
}

void StandardAnalysis::setTrackWinLen(int nValue)
{
    _params.track_len = nValue;
}

int StandardAnalysis::minTrackLen() const
{
    return _params.min_seg_len;
}

void StandardAnalysis::setMinTrackLen(int nValue)
{
    _params.min_seg_len = nValue;
}

double StandardAnalysis::minTrackAverageSMR() const
{
    return _params.SMR_thres;
}

void StandardAnalysis::setMinTrackAverageSMR(double dValue)
{
    _params.SMR_thres = dValue;
}

int StandardAnalysis::maxFixedGapsLen() const
{
    // Yes, ANARGS is wrong when it calls this a "min" : it's a "max".
    return _params.min_gap_len;
}

void StandardAnalysis::setMaxFixedGapsLen(int nValue)
{
    // Yes, ANARGS is wrong when it calls this a "min" : it's a "max".
    _params.min_gap_len = nValue;
}

double StandardAnalysis::minFixedGapsSMR() const
{
    return _params.min_seg_SMR;
}

void StandardAnalysis::setMinFixedGapsSMR(double dValue)
{
    _params.min_seg_SMR = dValue;
}

double StandardAnalysis::lastPeakWeight() const
{
    return _params.last_peak_cont * 100;
}

void StandardAnalysis::setLastPeakWeight(double dValue)
{
    _params.last_peak_cont = dValue / 100;
}

double StandardAnalysis::SMRFreqWeight() const
{
    return _params.SMR_cont * 100;
}

void StandardAnalysis::setSMRFreqWeight(double dValue)
{
    _params.SMR_cont = dValue / 100;
}

ATSSound::ESoundTypeId StandardAnalysis::targetSoundType() const
{
    return (ATSSound::ESoundTypeId)_params.type;
}

void StandardAnalysis::setTargetSoundType(ATSSound::ESoundTypeId eValue)
{
    _params.type = (int)eValue;
}

//========================================================================
void StandardAnalysis::dumpParams(std::ostream& ostream) const
{
    ostream << "Standard analysis : " << std::endl;

    ostream << "  StartTime           : " << startTime() << " s" << std::endl;
    ostream << "  Duration            : " << duration() << " s" << std::endl;
    ostream << "  MinFreq             : " << minFreq() << " Hz" << std::endl;
    ostream << "  MaxFreq             : " << maxFreq() << " Hz" << std::endl;
    ostream << "  MaxFreqDeviation    : " << maxFreqDeviation() << " %" << std::endl;
    ostream << "  AmpThreshold        : " << ampThreshold() << " dB" << std::endl;
    ostream << "  FFTWinType          : " << FFTWinType() << std::endl;
    ostream << "  FFTWinMinFreqCycles : " << FFTWinMinFreqCycles() << std::endl;
    ostream << "  FFTWinOverlap       : " << FFTWinOverlap() << " %" << std::endl;
    ostream << "  TrackWinLen         : " << trackWinLen() << " (frames)" << std::endl;
    ostream << "  MinTrackLen         : " << minTrackLen() << " (frames)" << std::endl;
    ostream << "  MinTrackAvgSMR      : " << minTrackAverageSMR() << " dB SPL" << std::endl;
    ostream << "  MaxFixedGapsLen     : " << maxFixedGapsLen() << "(frames)" << std::endl;
    ostream << "  MinFixedGapsSMR     : " << minFixedGapsSMR() << " dB SPL" << std::endl;
    ostream << "  LastPeakWeight      : " << lastPeakWeight() << " %" << std::endl;
    ostream << "  SMRFreqWeight       : " << SMRFreqWeight() << " %" << std::endl;
    ostream << "  TargetSoundType     : " << targetSoundType() << std::endl;
}
