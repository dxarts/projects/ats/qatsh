// ATSDataProxyModel abstract base class definition
//
//  Abstract proxy model for ATS partials/residual data
//  (source model = ATSModel)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <iostream>

#include <QModelIndex>

#include "ATSModel.h"
#include "ATSDataProxyModel.h"
#include "ATSModelItems.h"


// Constructors / Destructor ===============================================
ATSDataProxyModel::ATSDataProxyModel(QObject *parent)
: QAbstractProxyModel(parent)
{
}

ATSDataProxyModel::~ATSDataProxyModel()
{
}

// Set source model ========================================================
void ATSDataProxyModel::setSourceModel(QAbstractItemModel* pSourceModel)
{
    // Normal base class job.
    QAbstractProxyModel::setSourceModel(pSourceModel);

    // And also prepare for future source model resets.
    connect(pSourceModel, SIGNAL(modelReset()), this, SLOT(onSourceModelReset()));
}

void ATSDataProxyModel::onSourceModelReset()
{
    //std::cout << "ATSDataProxyModel::onSourceModelReset" << std::endl;
    
    // Warn attached views that we were reset
    beginResetModel();
    endResetModel();
}

// Get number of frames ====================================================
int ATSDataProxyModel::nbFrames() const
{
    const QModelIndex miFileProps = sourceModel()->index(0, 0, QModelIndex());
    const QModelIndex qmiNbFrames =
	sourceModel()->index(0, ATSFilePropertiesItem::eSoundPropNbFrames, miFileProps);

    return sourceModel()->data(qmiNbFrames).toInt();
}

// Get frame duration ======================================================
double ATSDataProxyModel::frameDuration() const
{
    const QModelIndex miFileProps = sourceModel()->index(0, 0, QModelIndex());
    const QModelIndex qmiSoundDur =
	sourceModel()->index(0, ATSFilePropertiesItem::eSoundPropDuration, miFileProps);
    const QModelIndex qmiNbFrames =
	sourceModel()->index(0, ATSFilePropertiesItem::eSoundPropNbFrames, miFileProps);

    return sourceModel()->data(qmiSoundDur).toDouble() 
	   / sourceModel()->data(qmiNbFrames).toInt();
}

