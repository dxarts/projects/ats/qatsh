// ResidualsSpecgramGraphicsView class definition
//
//  The QGraphicsView-derived residual bands spectrogram view
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef RESIDUALSSPECTRUMGRAPHICSSCENE_H
#define RESIDUALSSPECTRUMGRAPHICSSCENE_H

#include <QGraphicsScene>
#include "ColorMap.h"
#include "RulerSpec.h"

class QObject;
class QModelIndex;
class ATSModelManager;


class ResidualsSpecgramGraphicsScene : public QGraphicsScene
{
    Q_OBJECT

public:

    ResidualsSpecgramGraphicsScene(QObject *parent = 0);
    virtual ~ResidualsSpecgramGraphicsScene();

    void setModelManager(ATSModelManager* pModelMgr);
    
    // Set amplitude color contrast/brightness in [0, 1]
    void setAmplitudeContrast(double dContrast);
    void setAmplitudeBrightness(double dBrightness);
    
 signals:
    
    void colorMapChanged(const ColorMap* pColorMap);
    void timeRangeChanged(const RulerSpec* pRulerSpec);
    void frequencyRangeChanged(const RulerSpec* pRulerSpec);

 private:

    void loadFromModel();
    void loadResidual();

 private slots:

    void onModelReset();
    void onDataChanged(const QModelIndex& qmiTopLeft, const QModelIndex& qmiBotRight);

 private:
    
    ATSModelManager *_pModelMgr;

    // Amplitude color map.
    ColorMap _cmAmplColorMap;

    // Time ruler spec.
    RulerSpec _rsTimeRulerSpec;

    // Frequency ruler spec.
    RulerSpec _rsFreqRulerSpec;
};

#endif // RESIDUALSSPECTRUMGRAPHICSSCENE_H
