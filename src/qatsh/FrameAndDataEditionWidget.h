// FrameAndDataEditionWidget class definition
//
//  The widget for selected frame / data edition (transform via a formula, normalise, ...)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef FrameAndDataEditionWidget_h
#define FrameAndDataEditionWidget_h

#include <QWidget>

#include "TypesAndConstants.h"

namespace Ui {
    class FrameAndDataEditionWidget;
}

class QItemSelection;
class QItemSelectionModel;
class ATSModelManager;


class FrameAndDataEditionWidget : public QWidget 
{
    Q_OBJECT
    Q_DISABLE_COPY(FrameAndDataEditionWidget)

public:

    explicit FrameAndDataEditionWidget(QWidget *parent = 0);
    virtual ~FrameAndDataEditionWidget();

    void setModelManager(ATSModelManager* pModelMgr);

    void switchDataType(TypesAndConstants::EDataType eDataType);
    
protected:

    virtual void changeEvent(QEvent *e);

private slots:

    // Slots to synchronize control values
    void onMagnitudeChanged(int nNewIndex);
    void onFormulaChanged(const QString& qsNewFormula);
    void onSelectionChanged(const QItemSelection& qisSelected, 
			    const QItemSelection& qisDeselected);

    // Slots for buttons actions.
    void onApplyFormula();

private:

    Ui::FrameAndDataEditionWidget *_pui;

    // Model manager.
    ATSModelManager* _pModelMgr;

    // Current data type.
    TypesAndConstants::EDataType _eDataType;

    // Current selection model for convenience.
    QItemSelectionModel* _pSelectionModel;

};

#endif // FrameAndDataEditionWidget_h
