// AnalysisParamsDialog class implementation
//
//  The ATS analysis parameters dialog
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <QPushButton>
#include <QDoubleValidator>

#include "AnalysisParamsDialog.h"
#include "ui_AnalysisParamsDialog.h"

#include "StandardAnalysis.h"


AnalysisParamsDialog::AnalysisParamsDialog(QWidget *parent)
: QDialog(parent), _pui(new Ui::AnalysisParamsDialog)
{
    // TODO : When more than one analysis type available, select the first wizard page here.
    _pui->setupUi(this);

    // Connections for the dialog buttons.
    connect(_pui->qbbButtons, SIGNAL(accepted()), this, SLOT(accept()));
    connect(_pui->qbbButtons, SIGNAL(rejected()), this, SLOT(reject()));
    connect(_pui->qbbButtons->button(QDialogButtonBox::Reset), SIGNAL(clicked()),
	    _pui->qwParams, SLOT(onRestoreDefaults()));
}

AnalysisParamsDialog::~AnalysisParamsDialog()
{
    delete _pui;
}

void AnalysisParamsDialog::changeEvent(QEvent *pqEvent)
{
    QDialog::changeEvent(pqEvent);
    switch (pqEvent->type()) {
    case QEvent::LanguageChange:
	// TODO : When more than one analysis type available, do that also for each wizard page.
        _pui->retranslateUi(this);
        break;
    default:
        break;
    }
}

IATSAnalysis* AnalysisParamsDialog::analysisParams()
{
    return _pui->qwParams->analysisParams();
}

void AnalysisParamsDialog::accept()
{
    _pui->qwParams->setupAnalysisParams();

    QDialog::accept();
}
