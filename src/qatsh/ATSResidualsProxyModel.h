// ATSResidualsProxyModel class definition
//
//  Proxy for the ATS document "model" for the residual part of the model
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef ATSRESIDUALSPROXYMODEL_H
#define ATSRESIDUALSPROXYMODEL_H

#include <QModelIndex>

#include "ATSDataProxyModel.h"


class ATSResidualsProxyModel : public ATSDataProxyModel
{
    Q_OBJECT

public:

    ATSResidualsProxyModel(QObject *parent = 0);
    virtual ~ATSResidualsProxyModel();

    // QAbstractProxyModel implementation.
    virtual QModelIndex mapFromSource(const QModelIndex& sourceIndex) const;
    virtual QModelIndex mapToSource(const QModelIndex& proxyIndex) const;

    //virtual QItemSelection mapSelectionFromSource (const QItemSelection& sourceSelection) const;
    //virtual QItemSelection mapSelectionToSource (const QItemSelection& proxySelection) const;

    virtual QModelIndex index(int row, int column, const QModelIndex &miParent) const;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QModelIndex parent(const QModelIndex &index) const;

    // Get number of residual bands.
    int nbResiduals() const;
};

#endif // ATSRESIDUALSPROXYMODEL_H
