// ResidualsSpecgramWidget class definition
//
//  The widget that holds the residual bands spectrogram graphics view
//  and the associated controls (contrast/brightness sliders, color scale, 
//  time and frequency rulers ...)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef ResidualsSpecgramWidget_h
#define ResidualsSpecgramWidget_h

#include <QWidget>

namespace Ui {
    class ResidualsSpecgramWidget;
}

class ATSModelManager;

class ResidualsSpecgramWidget : public QWidget 
{
    Q_OBJECT
    Q_DISABLE_COPY(ResidualsSpecgramWidget)

public:

    explicit ResidualsSpecgramWidget(QWidget *parent = 0);
    virtual ~ResidualsSpecgramWidget();

    void setModelManager(ATSModelManager* pModelMgr);

    void zoom(double dRelTimeAnchor, double dRelTimeFactor,
              double dRelFreqAnchor, double dRelFreqFactor);
    void zoomAll();

protected:

    virtual void changeEvent(QEvent *pqEvent);

 private slots:

    void onContrastSliderChanged(int nValue);
    void onBrightnessSliderChanged(int nValue);
    
private:

    Ui::ResidualsSpecgramWidget *_pui;
};

#endif // ResidualsSpecgramWidget_h
