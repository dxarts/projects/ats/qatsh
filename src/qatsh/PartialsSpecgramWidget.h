// PartialsSpecgramWidget class definition
//
//  The widget that holds the partials spectrogram graphics view
//  and the associated controls (contrast/brightness sliders, color scale, 
//  time and frequency rulers ...)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef PartialsSpecgramWidget_h
#define PartialsSpecgramWidget_h

#include <QWidget>

namespace Ui {
    class PartialsSpecgramWidget;
}

class ATSModelManager;

class PartialsSpecgramWidget : public QWidget 
{
    Q_OBJECT
    Q_DISABLE_COPY(PartialsSpecgramWidget)

 public:

    explicit PartialsSpecgramWidget(QWidget *parent = 0);
    virtual ~PartialsSpecgramWidget();

    void setModelManager(ATSModelManager* pModelMgr);

    void zoom(double dRelTimeAnchor, double dRelTimeFactor,
              double dRelFreqAnchor, double dRelFreqFactor);
    void zoomAll();

protected:

    virtual void changeEvent(QEvent *pqEvent);

 private slots:

    void onContrastSliderChanged(int nValue);
    void onBrightnessSliderChanged(int nValue);
    
 private:

    Ui::PartialsSpecgramWidget *_pui;
};

#endif // PartialsSpecgramWidget_h
