// FrameAndDataProcessor class definition
//
//  The class that runs the automatic changes to selected frames and data
//  that are specified by the user through various tools
//  - add a new partial / remove selected ones
//  - apply a formula to a given magnitude of the selected frames and data 
//  - normalize selected frames and data amplitude
//  - ...
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <iostream>

#include "ATSModel.h"
#include "ATSSound.h"

#include "FrameAndDataProcessor.h"


// Constructor / Destructor ==========================================
FrameAndDataProcessor::FrameAndDataProcessor(ATSModel* pModel)
: _pATSModel(pModel)
{
}

FrameAndDataProcessor::~FrameAndDataProcessor()
{
}

// Add/remove partials ===============================================
//void FrameAndDataProcessor::addPartial(int nSrcPartIndex)
//{
//    _pATSModel->sound()->addPartial(nSrcPartIndex);
//    _pATSModel->reset();
//}

//void FrameAndDataProcessor::removePartials(const int aPartIndexes[], unsigned int nParts)
//{
//    _pATSModel->sound()->removePartials(aPartIndexes, nParts);
//    _pATSModel->reset();
//}
