// ATSSynthesis class implementation
//
//  The "standard" ATS synthesis (as originaly implemented in ATSH)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <cstring>
#include <algorithm>

#include "ATSSynthesis.h"

#include "ATSSound.h"
#include "SampledSound.h"


ATSSynthesis::ATSSynthesis(double dStartTime, double dDuration,
						   bool bAllParts, bool bUsePartsPhase,
						   double dPartsAmplFactor, double dResidAmplFactor,
						   double dFreqFactor, double dTimeFactor,
                           double dSampRate)
{
    setStartTime(dStartTime);
    setDuration(dDuration);
    setUseOnlySelectedPartials(!bAllParts);
    setUsePartialsPhase(bUsePartsPhase);
    setPartialsAmplitudeFactor(dPartsAmplFactor);
    setResidualAmplitudeFactor(dResidAmplFactor);
    setFrequencyFactor(dFreqFactor);
    setTimeFactor(dTimeFactor);
    setSamplingRate(dSampRate);
}

ATSSynthesis& ATSSynthesis::operator=(const ATSSynthesis& src)
{
    setStartTime(src.startTime());
    setDuration(src.duration());
    setUseOnlySelectedPartials(src.useOnlySelectedPartials());
    setUsePartialsPhase(src.usePartialsPhase());
    setPartialsAmplitudeFactor(src.partialsAmplitudeFactor());
    setResidualAmplitudeFactor(src.residualAmplitudeFactor());
    setFrequencyFactor(src.frequencyFactor());
    setTimeFactor(src.timeFactor());
    setSamplingRate(src.samplingRate());

    return *this;
}

bool ATSSynthesis::operator==(const ATSSynthesis& src) const
{
    return startTime() == src.startTime()
           && duration() == src.duration()
           && useOnlySelectedPartials() == src.useOnlySelectedPartials()
		   && usePartialsPhase() == src.usePartialsPhase()
		   && partialsAmplitudeFactor() == src.partialsAmplitudeFactor()
		   && residualAmplitudeFactor() == src.residualAmplitudeFactor()
		   && frequencyFactor() == src.frequencyFactor()
		   && timeFactor() == src.timeFactor()
		   && samplingRate() == src.samplingRate();
}

bool ATSSynthesis::operator!=(const ATSSynthesis& src) const
{
    return !(src == *this);
}

SampledSound* ATSSynthesis::operator()(ATSSound* pATSSound, const std::vector<int>& vecSelPartIndexes)
{
    // Setup the selection array for partials, if necessary.
    int *aSelectedPartials = 0;
    if (useOnlySelectedPartials() && !vecSelPartIndexes.empty())
    {
        aSelectedPartials = new int[pATSSound->nbPartials()];
        memset(aSelectedPartials, 0, sizeof(int) * pATSSound->nbPartials());
        for (size_t nSelPartIndInd = 0; nSelPartIndInd < vecSelPartIndexes.size(); nSelPartIndInd++)
            aSelectedPartials[vecSelPartIndexes[nSelPartIndInd]] = 1;
    }

    // TODO ? Real time envelop managment.
    CURVE timeEnvelop;
    curve_init(&timeEnvelop);

    // Generate the monophonic sound samples.
    double* adSamples[1] = { 0 };
    int nSamples = 0;

//#define ATSH_SYNTH
#ifdef ATSH_SYNTH
    do_atsh_synthesis
#else
    do_synthesis
#endif
	(pATSSound->data(), &_params, &timeEnvelop, aSelectedPartials,
	 &adSamples[0], &nSamples);
    curve_free(&timeEnvelop);
	delete [] aSelectedPartials;

    // Build a SampleSound from these samples (monophonic => 1 channel).
    SampledSound* pSampSound = 0;
    if (adSamples[0] && nSamples > 0)
    {
        pSampSound = new SampledSound(1, nSamples, (int)samplingRate(), adSamples);
		free(adSamples[0]);
    }

    return pSampSound;
}

//========================================================================
double ATSSynthesis::startTime() const
{
    return (double)_params.beg;
}

void ATSSynthesis::setStartTime(double dStartTime)
{
    _params.beg = dStartTime;
}

double ATSSynthesis::duration() const
{
    return _params.end - _params.beg;
}

void ATSSynthesis::setDuration(double dDuration)
{
    _params.end = _params.beg + dDuration;
}

bool ATSSynthesis::useOnlySelectedPartials() const
{
    return _params.allorsel == 0;
}

void ATSSynthesis::setUseOnlySelectedPartials(bool bOn)
{
    _params.allorsel = bOn ? 0 : 1;
}

bool ATSSynthesis::usePartialsPhase() const
{
    return _params.upha != 0;
}

void ATSSynthesis::setUsePartialsPhase(bool bOn)
{
    _params.upha = bOn ? 1 : 0;
}

double ATSSynthesis::partialsAmplitudeFactor() const
{
    return _params.amp;
}

void ATSSynthesis::setPartialsAmplitudeFactor(double dMultFactor)
{
    _params.amp = dMultFactor;
}

double ATSSynthesis::residualAmplitudeFactor() const
{
    return _params.ramp;
}

void ATSSynthesis::setResidualAmplitudeFactor(double dMultFactor)
{
    _params.ramp = dMultFactor;
}

double ATSSynthesis::frequencyFactor() const
{
    return _params.frec;
}

void ATSSynthesis::setFrequencyFactor(double dMultFactor)
{
    _params.frec = dMultFactor;
}

double ATSSynthesis::timeFactor() const
{
    return _params.max_stretch;
}

void ATSSynthesis::setTimeFactor(double dMultFactor)
{
    _params.max_stretch = dMultFactor;
}

double ATSSynthesis::samplingRate() const
{
    return _params.sr;
}

void ATSSynthesis::setSamplingRate(double dFrequency)
{
    _params.sr = dFrequency;
}


//========================================================================
void ATSSynthesis::dumpParams(const char* pszHeader, std::ostream& ostream) const
{
    ostream << pszHeader << "ATS synthesis : " << std::endl;

    ostream << "  Start time        (s) : " << startTime() << std::endl;
    ostream << "  Duration          (s) : " << duration() << std::endl;
    ostream << "  Synthesised partials  : " 
            << (useOnlySelectedPartials() ? "Only selected ones" : "All") << std::endl;
    ostream << "  Use partials phase    : " << (usePartialsPhase() ? "Yes" : "No")<< std::endl;
    ostream << "  Partials ampl. factor : " << partialsAmplitudeFactor() << std::endl;
    ostream << "  Residual ampl. factor : " << residualAmplitudeFactor() << std::endl;
    ostream << "  Frequency factor      : " << frequencyFactor() << std::endl;
    ostream << "  Time factor           : " << timeFactor() << std::endl;
    ostream << "  Sampling rate    (Hz) : " << samplingRate() << " Hz" << std::endl;
}
