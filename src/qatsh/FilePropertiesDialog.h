// FilePropertiesDialog class definition
//
//  The view/dialog that displays the current .ats document header data
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef FilePropertiesDialog_h
#define FilePropertiesDialog_h

#include <QDialog>

class QModelIndex;

namespace Ui {
    class FilePropertiesDialog;
}

class ATSModelManager;

class FilePropertiesDialog : public QDialog
{
    Q_OBJECT
    Q_DISABLE_COPY(FilePropertiesDialog)

public:

    explicit FilePropertiesDialog(QWidget *parent = 0);
    virtual ~FilePropertiesDialog();

    void setModelManager(ATSModelManager* pModelMgr);

protected:

    virtual void changeEvent(QEvent *e);

private slots:

    void onDataChanged(const QModelIndex& qmiTopLeft, const QModelIndex& qmiBotRight);

private:

    Ui::FilePropertiesDialog *_pui;

    ATSModelManager *_pModelMgr;
};

#endif // FilePropertiesDialog_h
