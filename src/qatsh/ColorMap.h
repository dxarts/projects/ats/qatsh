// ColorMap class implementation
//
//  A ubiquitous color map with contrast and brightness management.
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef COLORMAP_H
#define COLORMAP_H

#include <QList>
#include <QColor>


class ColorMap
{
 public:

    typedef enum
    {
	eStyleStandard,
	eStyleCold,
	eStyleHot,

	eStyleNumber
    } EStyle;

    // Constructors (1 < nColors <= 256, linear scaling).
    ColorMap();
    ColorMap(EStyle eMapTemp, int nColors,
	     double dMinValue, double dMaxValue, 
	     double dContrast = 0.5, double dBrightness = 0.5);

    // Change scale properties (1 < nColors <= 256, linear scaling).
    void reset(EStyle eMapTemp, int nColors,
	       double dMinValue, double dMaxValue, 
	       double dContrast = 0.5, double dBrightness = 0.5);

    // Map properties accessors.
    int nbColors() const;
    void setNbColors(int nColors);

    EStyle style() const;
    void setStyle(EStyle eStyle);

    double minValue() const;
    void setMinValue(double dValue);

    double maxValue() const;
    void setMaxValue(double dValue);

    double contrast() const;
    void setContrast(double dValue);

    double brightness() const;
    void setBrightness(double dValue);

    // Value to color conversion (linear from min to max)
    QColor color(double dValue) const;

    // Index to color conversion
    const QColor& color(int nIndex) const;

private:

    // The color map style.
    EStyle _eStyle;

    // Min and max values.
    double _dMinValue, _dMaxValue;

    // Contrast in [0, 1].
    double _dContrast;

    // Brightness in [0, 1].
    double _dBrightness;

    // The generated colors for the requested properties.
    QList<QColor> _qlColors;
};

#endif

