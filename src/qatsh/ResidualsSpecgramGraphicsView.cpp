// ResidualsSpecgramGraphicsView class implementation
//
//  The QGraphicsView-derived residual bands spectrogram view
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <QRectF>
#include <QBrush>
#include <QResizeEvent>
#include <QWheelEvent>
#include <QKeyEvent>

// #ifndef QT_NO_OPENGL
// # include <QtOpenGL/QGLWidget>
// #endif

#include <iostream>

#include "ResidualsSpecgramGraphicsView.h"
#include "ResidualsSpecgramGraphicsScene.h"

#include "ATSModel.h"


ResidualsSpecgramGraphicsView::ResidualsSpecgramGraphicsView(QWidget *parent)
: QGraphicsView(parent), _bInitialized(false)
{
// #ifndef QT_NO_OPENGL
//     setViewport(new QGLWidget);
// #endif

    ResidualsSpecgramGraphicsScene* pgsScene = 
	new ResidualsSpecgramGraphicsScene(parent);

    setScene(pgsScene);

    connect(pgsScene, SIGNAL(colorMapChanged(const ColorMap*)),
	    this, SIGNAL(colorMapChanged(const ColorMap*)));

    connect(pgsScene, SIGNAL(timeRangeChanged(const RulerSpec*)),
	    this, SIGNAL(timeRangeChanged(const RulerSpec*)));

    connect(pgsScene, SIGNAL(frequencyRangeChanged(const RulerSpec*)),
	    this, SIGNAL(frequencyRangeChanged(const RulerSpec*)));
}

ResidualsSpecgramGraphicsView::~ResidualsSpecgramGraphicsView()
{
}

void ResidualsSpecgramGraphicsView::setModelManager(ATSModelManager* pModelMgr)
{
    static_cast<ResidualsSpecgramGraphicsScene*>(scene())->setModelManager(pModelMgr);
}


// Event management ====================================================================

// Inhibit standard QGraphicView::wheelEvent behaviour (scrolling)
void ResidualsSpecgramGraphicsView::wheelEvent(QWheelEvent *pqEvent)
{
    Q_UNUSED(pqEvent);
}

// Inhibit standard QGraphicView::keyPressEvent behaviour (scrolling)
void ResidualsSpecgramGraphicsView::keyPressEvent(QKeyEvent *pqEvent)
{
    Q_UNUSED(pqEvent);
}

void ResidualsSpecgramGraphicsView::resizeEvent(QResizeEvent *pqEvent)
{
//     std::cout << "ResidualsSpecgramGraphicsView::resizeEvent(" 
// 	      << pqEvent->size().width() << "x" << pqEvent->size().height() << ") : "
// 	      << "oldSize = " << pqEvent->oldSize().width() 
// 	      << "x" << pqEvent->oldSize().height() << std::endl;

     QGraphicsView::resizeEvent(pqEvent);

     if (_bInitialized)
 	return;

     // Initialize view transform for full scene rest display.
     scale(1, -1);
     fitInView(scene()->sceneRect());

     _bInitialized = true;
}

//=======================================================================================

// Set amplitude color contrast in [0, 1]
void ResidualsSpecgramGraphicsView::setAmplitudeContrast(double dContrast)
{
    static_cast<ResidualsSpecgramGraphicsScene*>(scene())->setAmplitudeContrast(dContrast);
}

// Set amplitude color brightness in [0, 1]
void ResidualsSpecgramGraphicsView::setAmplitudeBrightness(double dBrightness)
{
    static_cast<ResidualsSpecgramGraphicsScene*>(scene())->setAmplitudeBrightness(dBrightness);
}

void ResidualsSpecgramGraphicsView::zoomShiftTime(double dAbsFactor, double dAbsShiftValue) 
{
    if (!_bInitialized)
    	return;

//     std::cout << "ResidualsSpecgramGraphicsView::zoomShiftTime(factor=" 
// 	      << dAbsFactor << ", shiftVal=" << dAbsShiftValue << ")" << std::endl;

    // Build new view transform.
    const QTransform qOldTrans = transform();
    QTransform qNewTrans(dAbsFactor, 0, 0, qOldTrans.m22(), dAbsShiftValue, qOldTrans.dy());

    // Apply it.
    setTransform(qNewTrans);
}

void ResidualsSpecgramGraphicsView::zoomShiftFrequency(double dAbsFactor, double dAbsShiftValue) 
{ 
    if (!_bInitialized)
    	return;

//     std::cout << "PartialsSpecgramGraphicsView::zoomShiftFreq(factor=" 
// 	      << dAbsFactor << ", shiftVal=" << dAbsShiftValue << ")" << std::endl;

    // Build new view transform.
    const QTransform qOldTrans = transform();
    QTransform qNewTrans(qOldTrans.m11(), 0, 0, dAbsFactor, qOldTrans.dx(), dAbsShiftValue);

    // Apply it.
    setTransform(qNewTrans);
}

