// FilePropertiesView class implementation
//
//  The view that displays the current .ats document header data
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include "FilePropertiesView.h"

#include "ui_FilePropertiesView.h"

#include <QModelIndex>

#include "ATSModelItems.h"
#include "ATSModel.h"


FilePropertiesView::FilePropertiesView(QWidget *parent) :
QWidget(parent), _pui(new Ui::FilePropertiesView), _pATSModel(0)
{
    _pui->setupUi(this);
}

FilePropertiesView::~FilePropertiesView()
{
    delete _pui;
}

void FilePropertiesView::setModel(ATSModel* pModel)
{
    _pATSModel = pModel;
    
    if (_pATSModel)
	connect(_pATSModel, SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&)), 
		this, SLOT(onDataChanged(const QModelIndex&, const QModelIndex&)));

    // Force first view update from model.
    onDataChanged(QModelIndex(), QModelIndex());
}

void FilePropertiesView::changeEvent(QEvent *e)
{
    switch (e->type()) {
    case QEvent::LanguageChange:
        _pui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void FilePropertiesView::onDataChanged(const QModelIndex& qmiTopLeft, 
				       const QModelIndex& qmiBotRight)
{
    // TODO : Check really changed rect if usefull here ...

    const QModelIndex miFileSize = 
	_pATSModel->index(0, ATSFilePropertiesItem::eSoundPropFileSize, QModelIndex());
    _pui->qleFileSize->setText(_pATSModel->data(miFileSize, Qt::DisplayRole).toString());

    const QModelIndex miType = 
	_pATSModel->index(0, ATSFilePropertiesItem::eSoundPropType, QModelIndex());
    _pui->qleFileType->setText(_pATSModel->data(miType, Qt::DisplayRole).toString());

    const QModelIndex miSamplingRate = 
	_pATSModel->index(0, ATSFilePropertiesItem::eSoundPropSamplingRate, QModelIndex());
    _pui->qleSamplingRate->setText(_pATSModel->data(miSamplingRate, Qt::DisplayRole).toString());

    const QModelIndex miFrameSize = 
	_pATSModel->index(0, ATSFilePropertiesItem::eSoundPropFrameSize, QModelIndex());
    _pui->qleFrameSize->setText(_pATSModel->data(miFrameSize, Qt::DisplayRole).toString());

    const QModelIndex miWindowSize = 
	_pATSModel->index(0, ATSFilePropertiesItem::eSoundPropWindowSize, QModelIndex());
    _pui->qleWindowSize->setText(_pATSModel->data(miWindowSize, Qt::DisplayRole).toString());

    const QModelIndex miPartPerFrm = 
	_pATSModel->index(0, ATSFilePropertiesItem::eSoundPropNbPartials, QModelIndex());
    _pui->qlePartialsPerFrame->setText(_pATSModel->data(miPartPerFrm, Qt::DisplayRole).toString());

    const QModelIndex miNbFrames = 
	_pATSModel->index(0, ATSFilePropertiesItem::eSoundPropNbFrames, QModelIndex());
    _pui->qleNbFrames->setText(_pATSModel->data(miNbFrames, Qt::DisplayRole).toString());

    const QModelIndex miDuration = 
	_pATSModel->index(0, ATSFilePropertiesItem::eSoundPropDuration, QModelIndex());
    _pui->qleDuration->setText(_pATSModel->data(miDuration, Qt::DisplayRole).toString());

    const QModelIndex miMaxAmplitude = 
	_pATSModel->index(0, ATSFilePropertiesItem::eSoundPropMaxPartialAmplitude, QModelIndex());
    _pui->qleMaxAmplitude->setText(_pATSModel->data(miMaxAmplitude, Qt::DisplayRole).toString());

    const QModelIndex miMaxFrequency = 
	_pATSModel->index(0, ATSFilePropertiesItem::eSoundPropMaxPartialFrequency, QModelIndex());
    _pui->qleMaxFrequency->setText(_pATSModel->data(miMaxFrequency, Qt::DisplayRole).toString());
}

