// MainWindow class definition
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef MainWindow_h
#define MainWindow_h

#include <QString>
#include <QMainWindow>

#include "DocumentWindow.h"

namespace Ui
{
    class MainWindow;
}
class DocumentWindow;
class IATSAnalysis;

class MainWindow : public QMainWindow
{
    Q_OBJECT

 public:

    MainWindow(QWidget *parent = 0);
    MainWindow(const QString &qsFileName, QWidget *parent = 0);
    MainWindow(const QString &qsFileName, IATSAnalysis* pAnalysis, QWidget *parent = 0);

    ~MainWindow();

    enum EActionId { eNewFile = 0, eOpenFile, eSaveFile, eSaveFileAs, eFileProperties,
		     ePreferences, 
		     eReAnalyse, eReSynthesize, 
		     eWinPartials, eWinResidual,
		     eAbout,
		     eActionNumber };
    void enableAction(EActionId eActionId, bool bEnable=true);

 private:

    void init();

    bool isUntitled() const;

    bool maybeSave();

    void setCurrentFile(const QString &qsFileName);
    QString simpleFileName(const QString &qsFullFileName);

    void layoutDocumentWindow(DocumentWindow* pqwDocWin);
    
    void analyseFile(const QString& qsFileName);
    void analyseFile(const QString &qsFileName, IATSAnalysis* pAnalysis);
    void loadFile(const QString &qsFileName);
    bool storeFile(const QString &qsFileName);

    void closeEvent(QCloseEvent *pqEvent);

 private slots:

    // File menu
    void newFile();
    void openFile();
    bool saveFile();
    bool saveFileAs();
    void showFileProperties();

    // Edit menu
    void preferences();

    // Switch between SelectionEdition/Analysis/Synthesis tool widgets
    void showSelectionEditionTool();
    void showAnalysisTool();
    void showSynthesisTool();
    void startPlayingSynthesisResult();

    // Switch between graphical views
    void showPartialsSpectrogram();
    void showResidualsSpectrogram();

    // Zoom managment
    void zoomIn();
    void zoomOut();
    void zoomAll();
    void zoomSelection();

    // Undo/redo
    void undo();
    void redo();

    // About
    void about();

 private:

    Ui::MainWindow *_pui;

    QString _qsCurFileName;
    bool _bIsUntitled;

    QList<QAction*> _qlActions;

    DocumentWindow *_pqwDocWin;
};

#endif // MainWindow_h
