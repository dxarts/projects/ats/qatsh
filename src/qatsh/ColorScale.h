// ColorScale class definition
//
//  A QGraphicsView displaying a color scale from a give color map
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef COLORSCALE_H
#define COLORSCALE_H

#include <QGraphicsView>

class QGraphicsSimpleTextItem;
class ColorMap;


class ColorScale : public QGraphicsView
{
    Q_OBJECT

 public:

    ColorScale(QWidget *parent = 0);
    virtual ~ColorScale();

 public slots:

    void onColorMapChanged(const ColorMap* pColorMap);

 protected:

    void clear();
    void redraw();

    virtual void resizeEvent(QResizeEvent* event);

 private:
    
    const ColorMap* _pColorMap;

    QGraphicsSimpleTextItem* _pqgiMinLabel;
    QGraphicsSimpleTextItem* _pqgiMaxLabel;
};

#endif // COLORSCALE_H
