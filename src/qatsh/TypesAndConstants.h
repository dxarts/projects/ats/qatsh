// TypesAndConstants definition
//
//  Definition of types and constants used everywhere
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef TYPESANDCONSTANTS_H
#define TYPESANDCONSTANTS_H

namespace TypesAndConstants
{

    // Data type for selection / edition.
    enum EDataType { ePartial=0, eResidualBand, eDataTypeNumber };

    // Partial editable magnitudes.
    enum EPartialMagnitude { eAmplitude=0, eFrequency, ePhase, ePartialMagnitudeNumber };

    // Residual band editable magnitudes.
    enum EResidualMagnitude { eEnergy=0, eResidualMagnitudeNumber };

};

#endif // TYPESANDCONSTANTS_H
