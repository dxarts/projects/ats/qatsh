// ATSAnalysis class definition
//
//  The "standard" ATS analysis (as originaly implemented in ATSH)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef STANDARDANALYSIS_H
#define STANDARDANALYSIS_H

#include <iostream>

#include "IATSAnalysis.h"

#include "ATSSound.h"

#include "atsa.h"


class StandardAnalysis : public IATSAnalysis
{
 public:
    
    // Constructor with analysis parameters.
    StandardAnalysis(double dStartTime = 0.0,
		     double dDuration = 10.0,
		     double dMinFreq = ATSA_LFREQ,
		     double dMaxFreq = ATSA_HFREQ,
		     double dMaxFreqDeviation = ATSA_FREQDEV*100,
		     double dAmpThreshold = ATSA_LMAG,
		     int nFFTWinType = ATSA_WTYPE,
		     int nFFTWinMinFreqCycles = ATSA_WCYCLES,
		     double dFFTWinOverlap = (1.0 - ATSA_HSIZE)*100,
		     int nTrackWinLen = ATSA_TRKLEN,
		     int nMinTrackLen = ATSA_MSEGLEN,
		     double dMinTrackAvgSMR = ATSA_SMRTHRES,
		     int nMaxFixedGapsLen = ATSA_MGAPLEN,
		     double dMinFixedGapsSMR = ATSA_MSEGSMR,
		     double dLastPeakWeight = ATSA_LPKCONT,
		     double dSMRFreqWeight = ATSA_SMRCONT,
		     ATSSound::ESoundTypeId eTargetSoundType 
		     = ATSSound::eSoundTypePartialsAmplFreqPhaseNoise);

    // Destructor.
    virtual ~StandardAnalysis();

    // Implementation of IATSAnalysis
    static const QString typeName();
    virtual const QString type() const;
    virtual ATSSound* operator()(SampledSound* pSampSound);

    // Detailed accessors.
    double startTime() const;
    void setStartTime(double dValue);

    double duration() const;
    void setDuration(double dValue);

    double minFreq() const;
    void setMinFreq(double dValue);

    double maxFreq() const;
    void setMaxFreq(double dValue);

    double maxFreqDeviation() const;
    void setMaxFreqDeviation(double dValue);

    double ampThreshold() const;
    void setAmpThreshold(double dValue);

    int FFTWinType() const;
    void setFFTWinType(int nValue);

    int FFTWinMinFreqCycles() const;
    void setFFTWinMinFreqCycles(int nValue);

    double FFTWinOverlap() const;
    void setFFTWinOverlap(double dValue);

    int trackWinLen() const;
    void setTrackWinLen(int nValue);

    int minTrackLen() const;
    void setMinTrackLen(int nValue);

    double minTrackAverageSMR() const;
    void setMinTrackAverageSMR(double dValue);

    int maxFixedGapsLen() const;
    void setMaxFixedGapsLen(int nValue);

    double minFixedGapsSMR() const;
    void setMinFixedGapsSMR(double dValue);

    double lastPeakWeight() const;
    void setLastPeakWeight(double dValue);

    double SMRFreqWeight() const;
    void setSMRFreqWeight(double dValue);

    ATSSound::ESoundTypeId targetSoundType() const;
    void setTargetSoundType(ATSSound::ESoundTypeId eValue);

    // Tools.
    void dumpParams(std::ostream& ostream) const;

 private:

    // Analysis parameters.
    ANARGS _params;
};

#endif // STANDARDANALYSIS_H
