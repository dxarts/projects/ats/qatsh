// ATSFrameProxyModel abstract base class definition
//
//  Abstract proxy model for ATS partials/residual per frame data
//  (source model = ATSDataProxyModel)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <iostream>

#include <QModelIndex>

#include "ATSDataProxyModel.h"
#include "ATSFrameProxyModel.h"
#include "ATSModelItems.h"


// Constructors / Destructor ===============================================
ATSFrameProxyModel::ATSFrameProxyModel(QObject *parent)
: QAbstractProxyModel(parent), _nCurrFrameIndex(0)
{
}

ATSFrameProxyModel::~ATSFrameProxyModel()
{
}

// Set source model ========================================================
void ATSFrameProxyModel::setSourceModel(QAbstractItemModel* pSourceModel)
{
    // Normal base class job.
    QAbstractProxyModel::setSourceModel(pSourceModel);

    // And also prepare for future source model resets.
    connect(pSourceModel, SIGNAL(modelReset()), this, SLOT(onSourceModelReset()));
}

void ATSFrameProxyModel::onSourceModelReset()
{
    // If the source model has been reset (from a newly analysed ATSSound, as an ex.),
    // we must check and may be fix the current frame index (may be larger than possible).
    const int nNbFrames = nbFrames();
    if (_nCurrFrameIndex >= nNbFrames)
	_nCurrFrameIndex = nNbFrames - 1;

//     std::cout << "ATSFrameProxyModel::onSourceModelReset : currFrameIdx = " 
// 	      << _nCurrFrameIndex << std::endl;
    
    // And also warn attached views that we were reset
    beginResetModel();
    endResetModel();
}

// Set current frame =======================================================
void ATSFrameProxyModel::setCurrentFrame(int nFrameIndex)
{
    // Save current frame index.
    _nCurrFrameIndex = nFrameIndex;

    // Warn attached views that the whole model has changed.
    beginResetModel();
    endResetModel();
}

// Get number of frames ====================================================
int ATSFrameProxyModel::nbFrames() const
{
    return static_cast<const ATSDataProxyModel*>(sourceModel())->nbFrames();
}

// Get frame duration ======================================================
double ATSFrameProxyModel::frameDuration() const
{
    return static_cast<const ATSDataProxyModel*>(sourceModel())->frameDuration();
}

