// ATSSynthesis class definition
//
//  The "standard" ATS synthesis (as originaly implemented in ATSH)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef ATSSYNTHESIS_H
#define ATSSYNTHESIS_H

#include <iostream>
#include <vector>

#include "atsa.h"

class SampledSound;
class ATSSound;


class ATSSynthesis
{
 public:
    ATSSynthesis(double dStartTime=0.0, double dDuration=0.0,
				 bool bAllParts=true, bool bUsePartsPhase=false,
				 double dPartsAmplFactor=1.0, double dResidAmplFactor=1.0,
				 double dFreqFactor=1.0, double dTimeFactor=1.0,
                 double dSampRate=44100);

    SampledSound* operator()(ATSSound* pATSSound, const std::vector<int>& vecSelPartIndexes);

    ATSSynthesis& operator=(const ATSSynthesis& src);
    bool operator==(const ATSSynthesis& src) const;
    bool operator!=(const ATSSynthesis& src) const;

    // Accessors to parameters.
    double startTime() const;
    void setStartTime(double dStartTime);

    double duration() const;
    void setDuration(double dDuration);

    bool useOnlySelectedPartials() const;
    void setUseOnlySelectedPartials(bool bOn);

    bool usePartialsPhase() const;
    void setUsePartialsPhase(bool bOn);

    double partialsAmplitudeFactor() const;
    void setPartialsAmplitudeFactor(double dMultFactor);

    double residualAmplitudeFactor() const;
    void setResidualAmplitudeFactor(double dMultFactor);

    double frequencyFactor() const;
    void setFrequencyFactor(double dMultFactor);

    double timeFactor() const;
    void setTimeFactor(double dMultFactor);

    double samplingRate() const;
    void setSamplingRate(double nFrequency);

    // Tools.
    void dumpParams(const char* pszHeader, std::ostream& ostream) const;

 private:

    // Synthesis parameters.
    SPARAMS _params;
};

#endif // ATSSYNTHESIS_H
