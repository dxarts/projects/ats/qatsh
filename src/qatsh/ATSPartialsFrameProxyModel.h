// ATSPartialsFrameProxyModel class definition
//
//  Proxy for the ATS document "model" for a given partials frame
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef ATSPARTIALSFRAMEPROXYMODEL_H
#define ATSPARTIALSFRAMEPROXYMODEL_H

#include <QModelIndex>

#include "ATSFrameProxyModel.h"


class ATSPartialsFrameProxyModel : public ATSFrameProxyModel
{
    Q_OBJECT

public:

    ATSPartialsFrameProxyModel(QObject *parent = 0);
    virtual ~ATSPartialsFrameProxyModel();

    // Get number of partials.
    int nbPartials() const;

    virtual QModelIndex mapFromSource(const QModelIndex& sourceIndex) const;
    virtual QModelIndex mapToSource(const QModelIndex& proxyIndex) const;

    virtual QVariant data(const QModelIndex &index, int role) const;

    virtual QItemSelection mapSelectionFromSource (const QItemSelection& sourceSelection) const;
    virtual QItemSelection mapSelectionToSource (const QItemSelection& proxySelection) const;

    virtual QModelIndex index(int row, int column, const QModelIndex &miParent) const;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QModelIndex parent(const QModelIndex &index) const;

    virtual QVariant headerData(int section, Qt::Orientation orientation,
				int role = Qt::DisplayRole) const;

};

#endif // ATSPARTIALSFRAMEPROXYMODEL_H
