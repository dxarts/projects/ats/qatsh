// FilePropertiesView class definition
//
//  The view that displays the current .ats document header data
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef FilePropertiesView_h
#define FilePropertiesView_h

#include <QWidget>

class QModelIndex;

namespace Ui {
    class FilePropertiesView;
}

class ATSModel;

class FilePropertiesView : public QWidget 
{
    Q_OBJECT
    Q_DISABLE_COPY(FilePropertiesView)

public:

    explicit FilePropertiesView(QWidget *parent = 0);
    virtual ~FilePropertiesView();

    void setModel(ATSModel* pModel);

protected:

    virtual void changeEvent(QEvent *e);

private slots:

    void onDataChanged(const QModelIndex& qmiTopLeft, const QModelIndex& qmiBotRight);

private:

    Ui::FilePropertiesView *_pui;

    ATSModel *_pATSModel;
};

#endif // FilePropertiesView_h
