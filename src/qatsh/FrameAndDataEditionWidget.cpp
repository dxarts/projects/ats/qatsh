// FrameAndDataEditionWidget class definition
//
//  The widget for selected frame / data edition (transform via a formula, normalise, ...)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <iostream>

#include <QSettings>
#include <QApplication>
#include <QMessageBox>
#include <QItemSelection>
#include <QItemSelectionModel>

#include "FrameAndDataEditionWidget.h"
#include "ui_FrameAndDataEditionWidget.h"

#include "TypesAndConstants.h"
#include "ATSModelManager.h"
#include "ATSPartialsProxyModel.h"


static const int KNMaxMagnitudes =
    ((int)TypesAndConstants::ePartialMagnitudeNumber 
     > (int)TypesAndConstants::eResidualMagnitudeNumber) 
    ? (int)TypesAndConstants::ePartialMagnitudeNumber 
    : (int)TypesAndConstants::eResidualMagnitudeNumber;
static const int KNMagnitudes[TypesAndConstants::eDataTypeNumber] = 
    { TypesAndConstants::ePartialMagnitudeNumber, TypesAndConstants::eResidualMagnitudeNumber };
static const char* KPSZMagnitudeNames[TypesAndConstants::eDataTypeNumber][KNMaxMagnitudes] =
{ 
    // ePartial
    { "Amplitude (a)", "Frequency (f)", "Phase (p)" },

    // eResidualBand
    { "Energy (e)", 0, 0 }
};

// Constructor / Destructor ==========================================
FrameAndDataEditionWidget::FrameAndDataEditionWidget(QWidget *parent)
: QWidget(parent), _pui(new Ui::FrameAndDataEditionWidget), 
  _eDataType(TypesAndConstants::ePartial), _pSelectionModel(0)
{
    _pui->setupUi(this);

    // Connect controls to local changed() slots for synchronisation
    connect(_pui->qcbMagnitude, SIGNAL(currentIndexChanged(int)),
            this, SLOT(onMagnitudeChanged(int)));
    connect(_pui->qcbFormula, SIGNAL(editTextChanged(const QString&)),
	    this, SLOT(onFormulaChanged(const QString&)));

    // Connect push-buttons to local slots
    connect(_pui->qpbApplyFormula, SIGNAL(clicked()), this, SLOT(onApplyFormula()));
}

FrameAndDataEditionWidget::~FrameAndDataEditionWidget()
{
    delete _pui;
}

// Model management ==================================================
void FrameAndDataEditionWidget::setModelManager(ATSModelManager* pModelMgr)
{
    _pModelMgr = pModelMgr;
}

void FrameAndDataEditionWidget::switchDataType(TypesAndConstants::EDataType eDataType)
{
    // Save new current data type.
    _eDataType = eDataType;
    
    // Get new current selection model for convenience.
    QItemSelectionModel* pOldSelModel = _pSelectionModel;
    switch (_eDataType)
    {
        case TypesAndConstants::ePartial:
	    _pSelectionModel = _pModelMgr->partialsSelectionModel();
	    break;

        case TypesAndConstants::eResidualBand:
	    _pSelectionModel = _pModelMgr->residualsSelectionModel();
	    break;
	    
        default: // N/A.
	    break;
    }

    // Connect relevant selection model change signal to local slot.
    if (pOldSelModel)
	disconnect(pOldSelModel, 
		   SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
		   this, SLOT(onSelectionChanged(const QItemSelection&, const QItemSelection&)));
    connect(_pSelectionModel, 
	    SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)),
	    this, SLOT(onSelectionChanged(const QItemSelection&, const QItemSelection&)));

    // Reset value type combo-box.
    _pui->qcbMagnitude->clear();
    for (int nMagInd = 0; nMagInd < KNMagnitudes[eDataType]; nMagInd++)
	_pui->qcbMagnitude->addItem(tr(KPSZMagnitudeNames[eDataType][nMagInd]));
}

// Slots to synchronize control values/state ===========================
void FrameAndDataEditionWidget::onMagnitudeChanged(int nNewIndex)
{
    QSettings qSettings;

    // Save formula list for current magnitude to settings.
    //TODO

    // Load formula list for new magnitude from settings.
    //TODO
}

void FrameAndDataEditionWidget::onSelectionChanged(const QItemSelection& qisSelected, 
						   const QItemSelection& qisDeselected)
{
    Q_UNUSED(qisSelected);
    Q_UNUSED(qisDeselected);
    _pui->qpbApplyFormula->setEnabled(_pSelectionModel->hasSelection()
				      && !_pui->qcbFormula->currentText().isEmpty());
}

void FrameAndDataEditionWidget::onFormulaChanged(const QString& qsNewFormula)
{
    _pui->qpbApplyFormula->setEnabled(_pSelectionModel->hasSelection()
				      && !qsNewFormula.isEmpty());
}

// Slots for real actions ============================================
void FrameAndDataEditionWidget::onApplyFormula()
{
    const QString qsFormula = _pui->qcbFormula->currentText();

    //std::cout << "FrameAndDataEditionWidget::onApplyFormula(" << qsFormula.toStdString() 
    //	      << ")" << std::endl;

    QApplication::setOverrideCursor(Qt::WaitCursor);
	
    std::string strParsedFormula;

    switch (_eDataType)
    {
        case TypesAndConstants::ePartial:
	{
	    const TypesAndConstants::EPartialMagnitude eMagnitude =
		(TypesAndConstants::EPartialMagnitude)_pui->qcbMagnitude->currentIndex();
	    strParsedFormula =
		_pModelMgr->partialsModel()->modifyPartials(_pSelectionModel->selectedIndexes(),
							    qsFormula, eMagnitude);
	}
	break;

        case TypesAndConstants::eResidualBand:
	{
	    std::cout << "WARNING: Residual bands modification not yet implemented" << std::endl;
// 	    const TypesAndConstants::EResidualMagnitude eMagnitude =
// 		(TypesAndConstants::EResidualMagnitude)_pui->qcbMagnitude->currentIndex();
//	    strParsedFormula =
//		_pModelMgr->residualsModel()->modifyBands(_pSelectionModel->selectedIndexes(),
// 							  qsFormula, eMagnitude);
	}
	break;
	    
        default: // N/A.
	    break;
    }
    
    if (strParsedFormula.find("Error") != std::string::npos)
    {
	QMessageBox::warning(this, qApp->applicationName(), 
			     tr("Error in formula :\n\n - source : %1\n\n - parsed : %2\n\n"
				"No partial modified.")
			     .arg(qsFormula).arg(strParsedFormula.c_str()));
    }

    QApplication::restoreOverrideCursor();
    
}

// Event management ==================================================
void FrameAndDataEditionWidget::changeEvent(QEvent *e)
{
    switch (e->type()) {
    case QEvent::LanguageChange:
        _pui->retranslateUi(this);
        break;
    default:
        break;
    }
}

