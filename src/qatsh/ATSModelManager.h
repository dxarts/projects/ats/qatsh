// ATSModelManager class definition
//
//  The class to centralize management for all the different models used in Qatsh
//  as well as the associated selection models that are shared between views.
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef ATSMODELMANAGER_H
#define ATSMODELMANAGER_H

class QItemSelectionModel;

class ATSModel;
class ATSPropertiesProxyModel;
class ATSPartialsFrameProxyModel;
class ATSResidualsFrameProxyModel;
class ATSPartialsProxyModel;
class ATSResidualsProxyModel;


class ATSModelManager
{
 public:

    // Constructor : creates all the models.
    ATSModelManager();

    virtual ~ATSModelManager();

    // Accessor to main model.
    ATSModel* mainModel();

    // Accessors to proxy models.
    ATSPropertiesProxyModel* propertiesModel();

    ATSPartialsFrameProxyModel* partialsFrameModel();
    ATSResidualsFrameProxyModel* residualsFrameModel();

    ATSPartialsProxyModel* partialsModel();
    ATSResidualsProxyModel* residualsModel();
    
    // Accessors to shared selection models.
    QItemSelectionModel* partialsSelectionModel();
    //void setPartialsSelectionModel(QItemSelectionModel* pSelModel);
    QItemSelectionModel* residualsSelectionModel();
    //void setResidualsSelectionModel(QItemSelectionModel* pSelModel);

 private:

    // The main ATS model.
    ATSModel* _pMainModel;

    // The ATS proxy models.
    ATSPropertiesProxyModel*    _pPropsModel;

    ATSPartialsFrameProxyModel* _pPartsFrameModel;
    ATSResidualsFrameProxyModel* _pResidsFrameModel;

    ATSPartialsProxyModel* _pPartsModel;
    ATSResidualsProxyModel* _pResidsModel;

    // The shared selection models (not owned here).
    QItemSelectionModel* _pPartsSelModel;
    QItemSelectionModel* _pResidsSelModel;
    
};

#endif // ATSMODELMANAGER_H
