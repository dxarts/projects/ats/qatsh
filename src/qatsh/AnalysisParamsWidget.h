// AnalysisParamsWidget class definition
//
//  The analysis parameters widget
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef AnalysisParamsWidget_h
#define AnalysisParamsWidget_h

#include <QWidget>

namespace Ui {
    class AnalysisParamsWidget;
}

class IATSAnalysis;

class AnalysisParamsWidget : public QWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(AnalysisParamsWidget)

 public:

    explicit AnalysisParamsWidget(QWidget *parent = 0);
    virtual ~AnalysisParamsWidget();

    // Set fields values from a given analysis parameter set
    void reset(const IATSAnalysis* pAnaParams);

    // 
    void setupAnalysisParams();
    IATSAnalysis* analysisParams();

    typedef enum { eStandard = 0, eTypeNumber } EAnalysisType;

 protected:

    virtual void changeEvent(QEvent *pqEvent);

    void restoreDefaults(EAnalysisType eAnaType);

 public slots:

    void onAnalysisTypeChanged(int eAnaType);
    void onRestoreDefaults();

 private:

    Ui::AnalysisParamsWidget *_pui;

    // The analysis parameters, for each type.
    IATSAnalysis* _pAnalyses[eTypeNumber];
};

#endif // AnalysisParamsWidget_h
