############################################################################
#
#   file        : thirdparty.cmake
#   copyright   : (C) 2009 by Brian Gavin, 2012 Jean-Philippe Meuret
#
############################################################################

############################################################################
#                                                                          #
#   This program is free software; you can redistribute it and/or modify   #
#   it under the terms of the GNU General Public License as published by   #
#   the Free Software Foundation; either version 2 of the License, or      #
#   (at your option) any later version.                                    #
#                                                                          #
############################################################################


################################################################################################
# Under Windows, install needed 3rd party DLLs close to Qatsh executable

# Find the full path-name of the 3rd party DLL corresponding to the given 3rd party link library
#
# Parameters :
# * LIB_PATH_NAMES : The link library (or list of link libraries) path-name.
# * LIB_NAME_HINTS : Hints for retrieving in LIB_PATH_NAMES the only lib we are taking care of,
#                    and for retrieving on disk the corresponding DLL.
# * DLL_NAME_PREFIXES : Possible prefixes (to the lib name hint) for retrieving the DLL.
#                       Note: the empty "" prefix is always tried at the end.
#                       Ex: "lib;xx" for "lib" and "xx" prefixes.
# * DLL_PATHNAME_VAR : Name of the output variable for the retrieved DLL path-name.

MACRO(_FIND_3RDPARTY_DLL LIB_PATH_NAMES LIB_NAME_HINTS DLL_NAME_PREFIXES DLL_PATHNAME_VAR)

	MESSAGE(STATUS "_FIND_3RDPARTY_DLL : Libs=${LIB_PATH_NAMES} Hints=${LIB_NAME_HINTS}")

	FOREACH(_LIB_NAME_HINT ${LIB_NAME_HINTS})

		# Must handle the case of multiple libs listed in ${LIB_PATH_NAMES} :
		# Use LIB_NAME_HINTS to retrieve the one we are interested in here.
		SET(_LIB_PATHNAME ${LIB_PATH_NAMES})
		FOREACH(_LIB_PATHNAME_ ${LIB_PATH_NAMES})
			IF(${_LIB_PATHNAME_} MATCHES "${_LIB_NAME_HINT}\\.")
				SET(_LIB_PATHNAME ${_LIB_PATHNAME_})
				#MESSAGE(STATUS "Match: _LIB_PATHNAME=${_LIB_PATHNAME_}")
				BREAK()
			ENDIF(${_LIB_PATHNAME_} MATCHES "${_LIB_NAME_HINT}\\.")
		ENDFOREACH(_LIB_PATHNAME_ ${LIB_PATH_NAMES})

		# Got the link library pathname : check if any corresponding DLL around (try all prefixes).
		# 1) Check the empty prefix
		#    (CMake ignores it when specified at the beginning of DLL_NAME_PREFIXES ... bull shit).
		GET_FILENAME_COMPONENT(_LIB_PATH "${_LIB_PATHNAME}" PATH)
		SET(${DLL_PATHNAME_VAR} "${_LIB_PATH}/../bin/${_LIB_NAME_HINT}${CMAKE_SHARED_LIBRARY_SUFFIX}")
		#MESSAGE(STATUS "Trying 3rdParty DLL ${${DLL_PATHNAME_VAR}}")
		IF(EXISTS "${${DLL_PATHNAME_VAR}}")
			MESSAGE(STATUS "Will install 3rdParty DLL ${${DLL_PATHNAME_VAR}}")
			BREAK() # First found is the one.
		ELSE(EXISTS "${${DLL_PATHNAME_VAR}}")
			UNSET(${DLL_PATHNAME_VAR})
		ENDIF(EXISTS "${${DLL_PATHNAME_VAR}}")

		# 2) Check other (specified) prefixes.
		FOREACH(_DLL_NAME_PREFIX ${DLL_NAME_PREFIXES})
			SET(${DLL_PATHNAME_VAR} "${_LIB_PATH}/../bin/${_DLL_NAME_PREFIX}${_LIB_NAME_HINT}${CMAKE_SHARED_LIBRARY_SUFFIX}")
			#MESSAGE(STATUS "Trying 3rdParty DLL ${${DLL_PATHNAME_VAR}}")
			IF(EXISTS "${${DLL_PATHNAME_VAR}}")
				BREAK() # First found is the one.
			ELSE(EXISTS "${${DLL_PATHNAME_VAR}}")
				UNSET(${DLL_PATHNAME_VAR})
			ENDIF(EXISTS "${${DLL_PATHNAME_VAR}}")
		ENDFOREACH(_DLL_NAME_PREFIX ${DLL_NAME_PREFIXES})

		IF(EXISTS "${${DLL_PATHNAME_VAR}}")
			MESSAGE(STATUS "Will install 3rdParty DLL ${${DLL_PATHNAME_VAR}}")
			BREAK() # First found is the one.
		ELSE(EXISTS "${${DLL_PATHNAME_VAR}}")
			UNSET(${DLL_PATHNAME_VAR})
		ENDIF(EXISTS "${${DLL_PATHNAME_VAR}}")

	ENDFOREACH(_LIB_NAME_HINT ${LIB_NAME_HINTS})

	#IF(NOT EXISTS "${${DLL_PATHNAME_VAR}}")
	#	MESSAGE(STATUS "Could not find 3rdParty DLL for lib ${LIB_NAME_HINTS} (prefixes ${DLL_NAME_PREFIXES})")
	#ENDIF()

ENDMACRO(_FIND_3RDPARTY_DLL DLL_PATHNAME)

MACRO(INSTALL_3RDPARTY_RUNTIME)

	SET(_THIRDPARTY_DLL_PATHNAMES)

	# Qt.
	SET(_QT_DBG_SFX "")
	IF(CMAKE_BUILD_TYPE STREQUAL "Debug" OR CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo")
		SET(_QT_DBG_SFX "d")
    ENDIF()

    IF(OPTION_QT5)

		GET_TARGET_PROPERTY(_THIRDPARTY_LIB_PATHNAME "${Qt5Core_LIBRARIES}" LOCATION)
		_FIND_3RDPARTY_DLL("${_THIRDPARTY_LIB_PATHNAME}" "Qt5Core${_QT_DBG_SFX}" "" _DLL_PATHNAME)
		LIST(APPEND _THIRDPARTY_DLL_PATHNAMES "${_DLL_PATHNAME}")

		GET_TARGET_PROPERTY(_THIRDPARTY_LIB_PATHNAME "${Qt5Widgets_LIBRARIES}" LOCATION)
		_FIND_3RDPARTY_DLL("${_THIRDPARTY_LIB_PATHNAME}" "Qt5Gui${_QT_DBG_SFX}" "" _DLL_PATHNAME)
		LIST(APPEND _THIRDPARTY_DLL_PATHNAMES "${_DLL_PATHNAME}")

		STRING(REPLACE "Qt5Gui" "Qt5Widgets" _THIRDPARTY_LIB_PATHNAME "${_THIRDPARTY_LIB_PATHNAME}")
		_FIND_3RDPARTY_DLL("${_THIRDPARTY_LIB_PATHNAME}" "Qt5Widgets${_QT_DBG_SFX}" "" _DLL_PATHNAME)
		LIST(APPEND _THIRDPARTY_DLL_PATHNAMES "${_DLL_PATHNAME}")

    ELSE()

		_FIND_3RDPARTY_DLL("${QT_QTCORE_LIBRARY}" "QtCore${_QT_DBG_SFX}4" "" _DLL_PATHNAME)
		LIST(APPEND _THIRDPARTY_DLL_PATHNAMES "${_DLL_PATHNAME}")

		_FIND_3RDPARTY_DLL("${QT_QTGUI_LIBRARY}" "QtGui${_QT_DBG_SFX}4" "" _DLL_PATHNAME)
		LIST(APPEND _THIRDPARTY_DLL_PATHNAMES "${_DLL_PATHNAME}")

    ENDIF()

	# LibSndFile
	_FIND_3RDPARTY_DLL("${LIBSNDFILE_LIBRARY}" "sndfile;sndfile-1" "lib;" _DLL_PATHNAME)
	LIST(APPEND _THIRDPARTY_DLL_PATHNAMES "${_DLL_PATHNAME}")

	# Compilers run-times.
	IF(MSVC)

		# We do it ourselves, but use InstallRequiredSystemLibraries to figure out what they are
		SET(CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS_SKIP TRUE)
		INCLUDE(InstallRequiredSystemLibraries)
		IF(CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS)
			SET(_DLL_PATHNAME "${CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS}")
			MESSAGE(STATUS "Will install compiler run-times ${_DLL_PATHNAME}")
			LIST(APPEND _THIRDPARTY_DLL_PATHNAMES "${_DLL_PATHNAME}")
		ENDIF(CMAKE_INSTALL_SYSTEM_RUNTIME_LIBS)

    ELSEIF(MINGW)

    	IF(OPTION_QT5)

			# Works with MinGW 4.7 shipped with QtCreator >= 2.6 and Qt 5.
			SET(_DLL_PATHNAME "${_MINGW_BINDIR}/libstdc++-6.dll")
			MESSAGE(STATUS "Will install compiler run-time ${_DLL_PATHNAME}")
			LIST(APPEND _THIRDPARTY_DLL_PATHNAMES "${_DLL_PATHNAME}")

			SET(_DLL_PATHNAME "${_MINGW_BINDIR}/libgcc_s_sjlj-1.dll")
			MESSAGE(STATUS "Will install compiler run-time ${_DLL_PATHNAME}")
			LIST(APPEND _THIRDPARTY_DLL_PATHNAMES "${_DLL_PATHNAME}")

			SET(_DLL_PATHNAME "${_MINGW_BINDIR}/libwinpthread-1.dll")
			MESSAGE(STATUS "Will install compiler run-time ${_DLL_PATHNAME}")
			LIST(APPEND _THIRDPARTY_DLL_PATHNAMES "${_DLL_PATHNAME}")

			SET(_DLL_PATHNAME "${_MINGW_BINDIR}/libgcc_s_dw2-1.dll")
			MESSAGE(STATUS "Will install compiler run-time ${_DLL_PATHNAME}")
			LIST(APPEND _THIRDPARTY_DLL_PATHNAMES "${_DLL_PATHNAME}")

    	ELSE()

			# Works with MinGW 4.4 shipped with QtCreator < 2.6 and with stock MinGW 4.7.
			GET_FILENAME_COMPONENT(_MINGW_BINDIR "${CMAKE_CXX_COMPILER}" PATH)
			SET(_DLL_PATHNAME "${_MINGW_BINDIR}/mingwm10.dll")
			MESSAGE(STATUS "Will install compiler run-time ${_DLL_PATHNAME}")
			LIST(APPEND _THIRDPARTY_DLL_PATHNAMES "${_DLL_PATHNAME}")

			SET(_DLL_PATHNAME "${_MINGW_BINDIR}/libgcc_s_dw2-1.dll")
			MESSAGE(STATUS "Will install compiler run-time ${_DLL_PATHNAME}")
			LIST(APPEND _THIRDPARTY_DLL_PATHNAMES "${_DLL_PATHNAME}")

    	ENDIF()

  	ENDIF(MSVC)

	INSTALL(FILES ${_THIRDPARTY_DLL_PATHNAMES} DESTINATION bin)

ENDMACRO(INSTALL_3RDPARTY_RUNTIME)

