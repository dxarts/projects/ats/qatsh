// ATSPropertiesProxyModel class definition
//
//  ATS proxy model for the file properties
//  (Row = ignored, Column = property id)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <iostream>

#include "ATSModel.h"
#include "ATSModelItems.h"
#include "ATSPropertiesProxyModel.h"


// Constructors / Destructor ===============================================
ATSPropertiesProxyModel::ATSPropertiesProxyModel(QObject *parent)
: QAbstractProxyModel(parent)
{
}

ATSPropertiesProxyModel::~ATSPropertiesProxyModel()
{
}

// Proxy <-> Main model index conversions ==================================
QModelIndex ATSPropertiesProxyModel::mapFromSource(const QModelIndex& miSource) const
{
    if (miSource.isValid())
    {
	const ATSModelItem *pmiSourceItem = 
	    static_cast<const ATSModelItem*>(miSource.internalPointer());
	if (pmiSourceItem->type() == ATSModelItem::eFileProperties)
	    return createIndex(0, miSource.column());
    }

    return QModelIndex();
}

QModelIndex ATSPropertiesProxyModel::mapToSource(const QModelIndex& miProxy) const
{ 
    if (miProxy.isValid())
    {
	ATSModel* pSourceModel = static_cast<ATSModel*>(sourceModel());
        ATSFilePropertiesItem *pmiPropItem = pSourceModel->rootItem()->fileProperties();
	return createIndex(0, miProxy.column(), pmiPropItem);
    }

    return QModelIndex();
}

// QAbstractItemModel implementation =======================================
QModelIndex ATSPropertiesProxyModel::index(int row, int column, const QModelIndex &miParent) const
{
    if (!miParent.isValid()) // Only the root item is supported.
    {
	return createIndex(row, column); // And (row, column) is sufficient as an identifier.
    }

    return QModelIndex();
}

int ATSPropertiesProxyModel::columnCount(const QModelIndex &miParent) const
{
    if (!miParent.isValid()) // Only the root item is supported.
    {
	return ATSFilePropertiesItem::eSoundPropNumber;
    }

    return 0;
}

int ATSPropertiesProxyModel::rowCount(const QModelIndex &miParent) const
{
    if (!miParent.isValid()) // Only the root item is supported.
    {
	return 1; // Model <=> list
    }

    return 0;
}

QModelIndex ATSPropertiesProxyModel::parent(const QModelIndex &miChild) const
{
    return QModelIndex();
}
