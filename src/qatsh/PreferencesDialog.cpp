// PreferencesDialog class implementation
//
//  The preferences dialog
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <QSettings>
#include <QPushButton>

#include "PreferencesDialog.h"
#include "ui_PreferencesDialog.h"


PreferencesDialog::PreferencesDialog(QWidget *parent)
: QDialog(parent), _pui(new Ui::PreferencesDialog)
{
    // TODO : When more than one analysis type available, select the first wizard page here.
    _pui->setupUi(this);

    // Connections for the dialog buttons.
    connect(_pui->qbbButtons, SIGNAL(accepted()), this, SLOT(accept()));
    connect(_pui->qbbButtons, SIGNAL(rejected()), this, SLOT(reject()));
    connect(_pui->qbbButtons->button(QDialogButtonBox::Reset), SIGNAL(clicked()),
	    this, SLOT(reset()));

    // Load from settings.
    reset();
}

PreferencesDialog::~PreferencesDialog()
{
    delete _pui;
}

void PreferencesDialog::changeEvent(QEvent *pqEvent)
{
    QDialog::changeEvent(pqEvent);
    switch (pqEvent->type()) {
    case QEvent::LanguageChange:
	// TODO : When more than one analysis type available, do that also for each wizard page.
        _pui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void PreferencesDialog::reset()
{
    // Load dialog from settings.
    QSettings qSettings;
    QString qsAudioPlayerCmd = qSettings.value("AudioPlayer/Command").toString();

    _pui->qleAudioPlayerCmd->setText(qsAudioPlayerCmd);
}

void PreferencesDialog::accept()
{
    //Store dialog values to settings.
    QSettings qSettings;
    qSettings.setValue("AudioPlayer/Command", _pui->qleAudioPlayerCmd->text());

    // Normal dialog accept().
    QDialog::accept();
}

// Static members ===========================================================
void PreferencesDialog::initializeSettings()
{
    QSettings qSettings;

    QString qsAudioPlayerCmd = qSettings.value("AudioPlayer/Command").toString();
    if (qsAudioPlayerCmd.isEmpty())
    {
#if WIN32
        qsAudioPlayerCmd = "\"C:\\Program Files\\Windows Media Player\\wmplayer.exe\" \"%1\"";
#else
        //qsAudioPlayerCmd = "realplay \"%1\"";
        qsAudioPlayerCmd = "gst-launch-0.10 filesrc location=\"%1\" ! wavparse ! audioconvert ! audioresample ! osssink";
#endif
	qSettings.setValue("AudioPlayer/Command", qsAudioPlayerCmd);
    }
}
