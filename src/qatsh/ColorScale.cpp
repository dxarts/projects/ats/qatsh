// ColorScale class implementation
//
//  A QGraphicsView displaying a color scale from a give color map
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <QResizeEvent>
#include <QGraphicsSimpleTextItem>

#include <iostream>

#include "ColorScale.h"
#include "ColorMap.h"


ColorScale::ColorScale(QWidget *parent)
: QGraphicsView(parent), _pColorMap(0), _pqgiMinLabel(0), _pqgiMaxLabel(0)
{
    setAlignment(Qt::AlignTop|Qt::AlignLeft);
    setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
    setBackgroundBrush(QBrush(Qt::black));

    setScene(new QGraphicsScene(parent));

    //std::cout << "ColorScale::ColorScale : viewRect = "
    //      << width() << "x" << height() << std::endl;
}

ColorScale::~ColorScale()
{
    clear();
}

void ColorScale::onColorMapChanged(const ColorMap* pColorMap)
{
    _pColorMap = pColorMap;

    redraw();
}

void ColorScale::clear()
{
    QGraphicsScene* pqgvScene = scene();
    foreach(QGraphicsItem* pqgiItem, pqgvScene->items())
    {
	pqgvScene->removeItem(pqgiItem);
	delete pqgiItem;
    }

    _pqgiMinLabel = 0;
    _pqgiMaxLabel = 0;
    
}

void ColorScale::redraw()
{
    //std::cout << "ColorScale::redraw" << std::endl;

    double dLabelScaleX = 1.0;
    double dLabelScaleY = 1.0;
    if (_pqgiMinLabel)
    {
	const QTransform qtLabelTrans = _pqgiMinLabel->transform();
	dLabelScaleX = qtLabelTrans.m11();
	dLabelScaleY = qtLabelTrans.m22();
    }
    //std::cout << "ColorScale::redraw : Label scale (" << _pqgiMinLabel << ") x=" 
    //      << dLabelScaleX << ", y=" << dLabelScaleY << std::endl;

    // Clear the scene.
    clear();

    // Reset scene rectangle.
    QGraphicsScene* pqgvScene = scene();
    const int nColors = _pColorMap->nbColors();
    pqgvScene->setSceneRect(0.0, 0.0, 24.0, nColors);

    // Draw the scale : 1 colored rectangle for each color.
    for (int nColorInd = 0; nColorInd < nColors; nColorInd++)
    {
	const QColor& qColor = _pColorMap->color(nColorInd);
	QGraphicsRectItem* pgiRect = 
	    new QGraphicsRectItem(0.0, 0.0, 24.0, 1.0);
	pgiRect->setPos(0.0, nColors - 1 - nColorInd);
	pgiRect->setPen(QPen(qColor));
	pgiRect->setBrush(QBrush(qColor));
	pgiRect->setZValue(-1.0); // Ensure min/max labels not drawn behind this one.
	pqgvScene->addItem(pgiRect);
    }

    // Draw min and max values (as fixed size labels).
    QFont qFont("Tahoma", 8); // Note: Serif, Courier, Times, Fixed also OK under Windows.
    qFont.setStretch(80);
    //std::cout << "ColorScale::redraw : Font " << qFont.rawName().toStdString() 
    //      << ", size pt " << qFont.pointSize() 
    //      << ", pix " << qFont.pixelSize() << std::endl;
    
    _pqgiMaxLabel = 
	new QGraphicsSimpleTextItem(QString("%1").arg(_pColorMap->maxValue(), 0, 'g', 2));
    _pqgiMaxLabel->setPos(1.0, 0.0);
    _pqgiMaxLabel->setPen(QPen(Qt::white));
    _pqgiMaxLabel->setFont(qFont);
    _pqgiMaxLabel->setTransform(QTransform::fromScale(dLabelScaleX, dLabelScaleY), true);
	_pqgiMaxLabel->setFlag(QGraphicsItem::ItemIgnoresTransformations); // Lock size.
    pqgvScene->addItem(_pqgiMaxLabel);


    _pqgiMinLabel = 
    	new QGraphicsSimpleTextItem(QString("%1").arg(_pColorMap->minValue(), 0, 'g', 2));
    _pqgiMinLabel->setPos(1.0, nColors - _pqgiMinLabel->sceneBoundingRect().height());
    _pqgiMinLabel->setPen(QPen(Qt::white));
    _pqgiMinLabel->setFont(qFont);
    _pqgiMinLabel->setTransform(QTransform::fromScale(dLabelScaleX, dLabelScaleY), true);
	_pqgiMinLabel->setFlag(QGraphicsItem::ItemIgnoresTransformations); // Lock size.
    pqgvScene->addItem(_pqgiMinLabel);

    QResizeEvent qre(size(), size());
    resizeEvent(&qre);
}


void ColorScale::resizeEvent(QResizeEvent *pqEvent)
{
    //std::cout << "ColorScale::resizeEvent(" 
    //      << pqEvent->size().width() << "x" << pqEvent->size().height()<< ")"<< std::endl;

    // Apply new scale because of resizing.
    setTransform(QTransform::fromScale(pqEvent->size().width() / scene()->sceneRect().width(), 
									   pqEvent->size().height() / scene()->sceneRect().height()));

    // Keep min. value label at the bottom (as if it was ignoring the scaling).
    _pqgiMinLabel->setPos(1.0, _pColorMap->nbColors() 
			       - _pqgiMinLabel->boundingRect().height());
	
    //QTransform qtLabelTrans = _pqgiMinLabel->transform();
    //std::cout << "ColorScale::resizeEvent : Label trans m11=" 
    //      << qtLabelTrans.m11() << ", m22=" << qtLabelTrans.m22() << std::endl;

    QGraphicsView::resizeEvent(pqEvent);
}
