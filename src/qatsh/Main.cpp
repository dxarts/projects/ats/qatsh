// Qatsh application
// - a Qt 4 port of ATSH (Gtk 1.2)
// - enhanced GUI
// - added partials and residual editing features
//
// Original ATSH code and design from Oscar Pablo Di Liscia / Juan Pampin / Pete Moss
//  <http://musica.unq.edu.ar/personales/odiliscia/software/ATSH-doc.htm>
//
// Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>


#include <QApplication>

#include "MainWindow.h"
#include "PreferencesDialog.h"

int main(int argc, char *argv[])
{
    // Application identification.
    QApplication::setOrganizationName("jpm");
    QApplication::setOrganizationDomain("jpm.com");
    QApplication::setApplicationName("Qatsh");
    QApplication::setApplicationVersion("0.9.0-dev");

    // Settings initialization (dafault values).
    PreferencesDialog::initializeSettings();

    // Create and run application with given command line arguments.
    QApplication qApplication(argc, argv);

    MainWindow* pMainWin;
    if (argc > 1)
		pMainWin = new MainWindow(argv[1]);
    else
		pMainWin = new MainWindow;

    pMainWin->show();

    return qApplication.exec();
}
