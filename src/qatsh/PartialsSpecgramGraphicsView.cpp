// PartialsSpecgramGraphicsView class implementation
//
//  The QGraphicsView-derived partials spectrogram view
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <QRectF>
#include <QBrush>
#include <QResizeEvent>
#include <QWheelEvent>
#include <QKeyEvent>

// #ifndef QT_NO_OPENGL
// # include <QtOpenGL/QGLWidget>
// #endif

#include <iostream>

#include "PartialsSpecgramGraphicsView.h"
#include "PartialsSpecgramGraphicsScene.h"

#include "ATSModelManager.h"


PartialsSpecgramGraphicsView::PartialsSpecgramGraphicsView(QWidget *parent)
: QGraphicsView(parent), _bInitialized(false)
{
// #ifndef QT_NO_OPENGL
//     setViewport(new QGLWidget);
// #endif

    PartialsSpecgramGraphicsScene* pgsScene = 
	new PartialsSpecgramGraphicsScene(parent);

    setScene(pgsScene);

    connect(pgsScene, SIGNAL(colorMapChanged(const ColorMap*)),
	    this, SIGNAL(colorMapChanged(const ColorMap*)));

    connect(pgsScene, SIGNAL(timeRangeChanged(const RulerSpec*)),
	    this, SIGNAL(timeRangeChanged(const RulerSpec*)));

    connect(pgsScene, SIGNAL(frequencyRangeChanged(const RulerSpec*)),
	    this, SIGNAL(frequencyRangeChanged(const RulerSpec*)));
}

PartialsSpecgramGraphicsView::~PartialsSpecgramGraphicsView()
{
}

void PartialsSpecgramGraphicsView::setModelManager(ATSModelManager* pModelMgr)
{
    PartialsSpecgramGraphicsScene* pScene =     
	static_cast<PartialsSpecgramGraphicsScene*>(scene());

    pScene->setModelManager(pModelMgr);
}

// Event management ====================================================================

// Inhibit standard QGraphicView::wheelEvent behaviour (scrolling)
void PartialsSpecgramGraphicsView::wheelEvent(QWheelEvent *pqEvent)
{
	//std::cout << "PartialsSpecgramGraphicsView::wheelEvent" << std::endl;
    Q_UNUSED(pqEvent);
}

// Inhibit standard QGraphicView::keyPressEvent behaviour (scrolling)
void PartialsSpecgramGraphicsView::keyPressEvent(QKeyEvent *pqEvent)
{
    Q_UNUSED(pqEvent);
}

void PartialsSpecgramGraphicsView::resizeEvent(QResizeEvent *pqEvent)
{
//     std::cout << "PartialsSpecgramGraphicsView::resizeEvent(" 
// 	      << pqEvent->size().width() << "x" << pqEvent->size().height() << ") : "
// 	      << "oldSize = " << pqEvent->oldSize().width() 
// 	      << "x" << pqEvent->oldSize().height() << std::endl;

    QGraphicsView::resizeEvent(pqEvent);

    if (_bInitialized)
 	return;
    
    // Initialize view transform for full scene rect display.
    scale(1, -1);
    fitInView(scene()->sceneRect());
    
    _bInitialized = true;
}

//=====================================================================================

// Set amplitude color contrast in [0, 1]
void PartialsSpecgramGraphicsView::setAmplitudeContrast(double dContrast)
{
    static_cast<PartialsSpecgramGraphicsScene*>(scene())->setAmplitudeContrast(dContrast);
}

// Set amplitude color brightness in [0, 1]
void PartialsSpecgramGraphicsView::setAmplitudeBrightness(double dBrightness)
{
    static_cast<PartialsSpecgramGraphicsScene*>(scene())->setAmplitudeBrightness(dBrightness);
}

void PartialsSpecgramGraphicsView::zoomShiftTime(double dAbsFactor, double dAbsShiftValue) 
{
    if (!_bInitialized)
    	return;

	//std::cout << "PartialsSpecgramGraphicsView::zoomShiftTime(factor=" 
	//		   << dAbsFactor << ", shiftVal=" << dAbsShiftValue << ") : newTrans=..." << std::endl;

    // Build new view transform.
    const QTransform qOldTrans = transform();
    QTransform qNewTrans(dAbsFactor, 0, 0, qOldTrans.m22(), dAbsShiftValue, qOldTrans.dy());

    // Apply it.
    setTransform(qNewTrans);

//	std::cout << qNewTrans.m11() << ", " << qNewTrans.m12() << ", " << qNewTrans.m13() << std::endl
//			  << qNewTrans.m21() << ", " << qNewTrans.m22() << ", " << qNewTrans.m23() << std::endl
//			  << qNewTrans.m31() << ", " << qNewTrans.m32() << ", " << qNewTrans.m33() << std::endl;
}

void PartialsSpecgramGraphicsView::zoomShiftFrequency(double dAbsFactor, double dAbsShiftValue) 
{ 
    if (!_bInitialized)
    	return;

	//std::cout << "PartialsSpecgramGraphicsView::zoomShiftFreq(factor=" 
	//		   << dAbsFactor << ", shiftVal=" << dAbsShiftValue << ") : newTrans=..." << std::endl;

    // Build new view transform.
    const QTransform qOldTrans = transform();
    QTransform qNewTrans(qOldTrans.m11(), 0, 0, dAbsFactor, qOldTrans.dx(), dAbsShiftValue);

    // Apply it.
    setTransform(qNewTrans);
	
//	std::cout << qNewTrans.m11() << ", " << qNewTrans.m12() << ", " << qNewTrans.m13() << std::endl
//			  << qNewTrans.m21() << ", " << qNewTrans.m22() << ", " << qNewTrans.m23() << std::endl
//			  << qNewTrans.m31() << ", " << qNewTrans.m32() << ", " << qNewTrans.m33() << std::endl;
}

