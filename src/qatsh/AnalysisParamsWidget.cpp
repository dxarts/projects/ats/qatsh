// AnalysisParamsWidget class implementation
//
//  The ATS analysis parameters widget
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <QPushButton>
#include <QDoubleValidator>

#include "AnalysisParamsWidget.h"
#include "ui_AnalysisParamsWidget.h"

#include "StandardAnalysis.h"


AnalysisParamsWidget::AnalysisParamsWidget(QWidget *parent)
: QWidget(parent), _pui(new Ui::AnalysisParamsWidget)
{
    _pui->setupUi(this);

    // Initialize analysis array and dialog controlms for each analysis type
    for (int eAnaType = eStandard; eAnaType < eTypeNumber; eAnaType++)
    {
	_pAnalyses[eAnaType] = 0;
	restoreDefaults((EAnalysisType)eAnaType);
    }

    // Connect analysis type combo-box to the stacked widget switcher
    connect(_pui->qcbAnalysisType, SIGNAL(currentIndexChanged(int)),
	    this, SLOT(onAnalysisTypeChanged(int)));
}

AnalysisParamsWidget::~AnalysisParamsWidget()
{
    delete _pui;

    for (int eAnaType = eStandard; eAnaType < eTypeNumber; eAnaType++)
	if (_pAnalyses[eAnaType])
	    delete _pAnalyses[eAnaType];
}

void AnalysisParamsWidget::changeEvent(QEvent *pqEvent)
{
    QWidget::changeEvent(pqEvent);
    switch (pqEvent->type()) {
    case QEvent::LanguageChange:
        _pui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void AnalysisParamsWidget::onAnalysisTypeChanged(int eAnaType)
{
    _pui->qswAnalyses->setCurrentIndex(eAnaType);
}

void AnalysisParamsWidget::onRestoreDefaults()
{
    const EAnalysisType eAnaType = (EAnalysisType)_pui->qswAnalyses->currentIndex();

    restoreDefaults(eAnaType);
}

//====================================================================================
void AnalysisParamsWidget::restoreDefaults(AnalysisParamsWidget::EAnalysisType eAnaType)
{
    switch(eAnaType)
    {
        case eStandard:
	{
	    StandardAnalysis ana; // A standard analysis with default parameters.

	    _pui->qsbStdStartTime->setValue(ana.startTime());
	    _pui->qsbStdStartTime->setValue(ana.startTime());
	    _pui->qsbStdDuration->setValue(ana.duration());
	    _pui->qsbStdMinFreq->setValue(ana.minFreq());
	    _pui->qsbStdMaxFreq->setValue(ana.maxFreq());
	    _pui->qsbStdMaxFreqDeviation->setValue(ana.maxFreqDeviation());
	    _pui->qsbStdAmpThreshold->setValue(ana.ampThreshold());
	    _pui->qcbStdFFTWinType->setCurrentIndex(ana.FFTWinType());
	    _pui->qsbStdFFTWinMinFreqCycles->setValue(ana.FFTWinMinFreqCycles());
	    _pui->qsbStdFFTWinOverlap->setValue(ana.FFTWinOverlap());
	    _pui->qsbStdTrackWinLen->setValue(ana.trackWinLen());
	    _pui->qsbStdMinTrackLen->setValue(ana.minTrackLen());
	    _pui->qsbStdMinTrackAvgSMR->setValue(ana.minTrackAverageSMR());
	    _pui->qsbStdMaxFixedGapsLen->setValue(ana.maxFixedGapsLen());
	    _pui->qsbStdMinFixedGapsSMR->setValue(ana.minFixedGapsSMR());
	    _pui->qsbStdLastPeakWeight->setValue(ana.lastPeakWeight());
	    _pui->qsbStdSMRFreqWeight->setValue(ana.SMRFreqWeight());
	    //_pui->qcbStdTargetSoundType->setCurrentIndex((int)ana.targetSoundType);

	    break;
	}

	// N/A.
        case eTypeNumber:
	    break;
    }
}

void AnalysisParamsWidget::reset(const IATSAnalysis* pAnaParams)
{
    if (pAnaParams->isOfType(StandardAnalysis::typeName()))
    {
	const StandardAnalysis* pAna = static_cast<const StandardAnalysis*>(pAnaParams);

	_pui->qsbStdStartTime->setValue(pAna->startTime());
	_pui->qsbStdStartTime->setValue(pAna->startTime());
	_pui->qsbStdDuration->setValue(pAna->duration());
	_pui->qsbStdMinFreq->setValue(pAna->minFreq());
	_pui->qsbStdMaxFreq->setValue(pAna->maxFreq());
	_pui->qsbStdMaxFreqDeviation->setValue(pAna->maxFreqDeviation());
	_pui->qsbStdAmpThreshold->setValue(pAna->ampThreshold());
	_pui->qcbStdFFTWinType->setCurrentIndex(pAna->FFTWinType());
	_pui->qsbStdFFTWinMinFreqCycles->setValue(pAna->FFTWinMinFreqCycles());
	_pui->qsbStdFFTWinOverlap->setValue(pAna->FFTWinOverlap());
	_pui->qsbStdTrackWinLen->setValue(pAna->trackWinLen());
	_pui->qsbStdMinTrackLen->setValue(pAna->minTrackLen());
	_pui->qsbStdMinTrackAvgSMR->setValue(pAna->minTrackAverageSMR());
	_pui->qsbStdMaxFixedGapsLen->setValue(pAna->maxFixedGapsLen());
	_pui->qsbStdMinFixedGapsSMR->setValue(pAna->minFixedGapsSMR());
	_pui->qsbStdLastPeakWeight->setValue(pAna->lastPeakWeight());
	_pui->qsbStdSMRFreqWeight->setValue(pAna->SMRFreqWeight());
	//_pui->qcbStdTargetSoundType->setCurrentIndex((int)pAna->targetSoundType);
    }
}

void AnalysisParamsWidget::setupAnalysisParams()
{
    const EAnalysisType eAnaType = (EAnalysisType)_pui->qswAnalyses->currentIndex();

    switch(eAnaType)
    {
        case eStandard:
	{
	    if (!_pAnalyses[eAnaType])
		_pAnalyses[eAnaType] = new StandardAnalysis();

	    StandardAnalysis* pAna = static_cast<StandardAnalysis*>(_pAnalyses[eAnaType]);

	    pAna->setStartTime(_pui->qsbStdStartTime->value());
	    pAna->setDuration(_pui->qsbStdDuration->value());
	    pAna->setMinFreq(_pui->qsbStdMinFreq->value());
	    pAna->setMaxFreq(_pui->qsbStdMaxFreq->value());
	    pAna->setMaxFreqDeviation(_pui->qsbStdMaxFreqDeviation->value());
	    pAna->setAmpThreshold(_pui->qsbStdAmpThreshold->value());
	    pAna->setFFTWinType(_pui->qcbStdFFTWinType->currentIndex());
	    pAna->setFFTWinMinFreqCycles(_pui->qsbStdFFTWinMinFreqCycles->value());
	    pAna->setFFTWinOverlap(_pui->qsbStdFFTWinOverlap->value());
	    pAna->setTrackWinLen(_pui->qsbStdTrackWinLen->value());
	    pAna->setMinTrackLen(_pui->qsbStdMinTrackLen->value());
	    pAna->setMinTrackAverageSMR(_pui->qsbStdMinTrackAvgSMR->value());
	    pAna->setMaxFixedGapsLen(_pui->qsbStdMaxFixedGapsLen->value());
	    pAna->setMinFixedGapsSMR(_pui->qsbStdMinFixedGapsSMR->value());
	    pAna->setLastPeakWeight(_pui->qsbStdLastPeakWeight->value());
	    pAna->setSMRFreqWeight(_pui->qsbStdSMRFreqWeight->value());
	    //     pAna->setTargetSoundType
	    // 	            ((ATSSound::ESoundTypeId)_pui->qcbStdTargetSoundType->currentIndex());

	    break;
	}

	// N/A.
        case eTypeNumber:
	    break;
    }
}

IATSAnalysis* AnalysisParamsWidget::analysisParams()
{
    const EAnalysisType eAnaType = (EAnalysisType)_pui->qswAnalyses->currentIndex();

    return _pAnalyses[eAnaType];
}
