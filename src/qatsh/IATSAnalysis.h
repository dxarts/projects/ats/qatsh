// IATSAnalysis interface definition
//
//  The interface of any ATS analysis : generate 1 ATSSound from 1 sampled sound
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef IATSANALYSIS_H
#define IATSANALYSIS_H

#include <QString>

class SampledSound;
class ATSSound;


class IATSAnalysis
{
 public:

	// Destructor.
    virtual ~IATSAnalysis();

    // Static type name : to be re-implemented in every derived class.
    static const QString typeName();

    // Type name (idem : must return typeName()).
    virtual const QString type() const = 0;

    // Type checking : use static <derived>::typeName() as an argument.
    bool isOfType(const QString& qsType) const;

    virtual ATSSound* operator()(SampledSound* pSampSound) = 0;
};

#endif // IATSANALYSIS_H
