// ATSResidualsFrameProxyModel class definition
//
//  ATS proxy model for the residual bands data associated to 1 given frame 
//  (Row = Band index, Column = property id)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef ATSRESIDUALSFRAMEPROXYMODEL_H
#define ATSRESIDUALSFRAMEPROXYMODEL_H

#include <QModelIndex>

#include "ATSFrameProxyModel.h"


class ATSResidualsFrameProxyModel : public ATSFrameProxyModel
{
    Q_OBJECT

public:

    ATSResidualsFrameProxyModel(QObject *parent = 0);
    virtual ~ATSResidualsFrameProxyModel();

    // Get number of residual bands.
    int nbResiduals() const;

    virtual QModelIndex mapFromSource(const QModelIndex& sourceIndex) const;
    virtual QModelIndex mapToSource(const QModelIndex& proxyIndex) const;

    virtual QVariant data(const QModelIndex &index, int role) const;

    //virtual QItemSelection mapSelectionFromSource (const QItemSelection& sourceSelection) const;
    //virtual QItemSelection mapSelectionToSource (const QItemSelection& proxySelection) const;

    virtual QModelIndex index(int row, int column, const QModelIndex &miParent) const;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QModelIndex parent(const QModelIndex &index) const;

    virtual QVariant headerData(int section, Qt::Orientation orientation,
				int role = Qt::DisplayRole) const;

};

#endif // ATSRESIDUALSFRAMEPROXYMODEL_H
