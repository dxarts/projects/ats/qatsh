// ATS model item classes definition
//
// * ATSModelItem (base class)
//   * ATSFilePropertiesItem (file properties)
//   * ATSPartialItem (one partial with all its frames)
//   * ATSResidualBandItem (one residual band with all its frames)
//   * ATSRootItem (Root item of the model, agregates items from the 3 previous types)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef ATSMODELITEMS_H
#define ATSMODELITEMS_H

#include <QVariant>

#include "ATSSound.h"


//====================================================================
class ATSModelItem
{
 public:

    typedef enum
    {
        eRoot,           // Root item
        eFileProperties, // List of file properties
        ePartials,       // List of partials
        eResidualBands,  // List of residual bands
        ePartial,        // 1 partial = matrix : row=frame, col=property
        eResidualBand    // 1 residual band = matrix : row=frame, col=property
    } EType;

    ATSModelItem(EType eType, ATSSound* pATSSound);

    EType type() const;

 protected:

    EType _eType;

    ATSSound* _pATSSound;
};

//====================================================================
class ATSFilePropertiesItem : public ATSModelItem
{
 public:

    // Sound properties ids (don't change order/values, QModelIndel are built on it).
    typedef enum
    {
	eSoundPropType = 0,
	eSoundPropFileSize,

	eSoundPropDuration,
	eSoundPropSamplingRate,
	eSoundPropWindowSize,
	eSoundPropFrameSize,
	eSoundPropNbFrames,

	eSoundPropNbPartials,
	eSoundPropMaxPartialAmplitude,
	eSoundPropMaxPartialFrequency,

	eSoundPropNbResidualBands,
	eSoundPropMaxResidualEnergy,
	eSoundPropMaxResidualFrequency,
	
	eSoundPropNumber // Number of enum values (must stay the last enum value).
    } ESoundPropertyId;
    
 public:

    ATSFilePropertiesItem(ATSSound* pATSSound);

    int nbProperties() const;

    QVariant property(ESoundPropertyId ePropId) const;
    
};

//====================================================================
class ATSPartialItem : public ATSModelItem
{
 public:

    // Partial properties ids.
    typedef enum
    {
	ePropFrequency = 0,
	ePropAmplitude = 1,
	ePropPhase = 2,
	ePropSMR = 3,
	ePropTime = 4,

	ePropNumber
    } EPropertyId;

 public:

    ATSPartialItem(ATSSound* pATSSound, int nPartIndex);

    int partialIndex() const;

    int nbFrames() const;

    QVariant property(int nFrameIndex, EPropertyId ePropId) const;

    static int nbProperties();

    static const char* propertyName(EPropertyId ePropId);
    static const char* propertyToolTip(EPropertyId ePropId);

 private:
    
    int _nPartIndex;
};

//====================================================================
class ATSResidualBandItem : public ATSModelItem
{
 public:

    // Residual band properties ids.
    typedef enum
    {
	ePropMinFrequency = 0,
	ePropMaxFrequency = 1,
	ePropEnergy = 2,
	ePropTime = 3,

	ePropNumber
    } EPropertyId;

 public:

    ATSResidualBandItem(ATSSound* pATSSound, int nBandInd);

    int bandIndex() const;

    int nbFrames() const;

    QVariant property(int nFrameIndex, EPropertyId ePropId) const;

    static int nbProperties();

    static const char* propertyName(EPropertyId ePropId);
    static const char* propertyToolTip(EPropertyId ePropId);

 private:
    
    int _nBandIndex;
};

//====================================================================
class ATSPartialsItem : public ATSModelItem
{
 public:

    ATSPartialsItem(ATSSound* pATSSound);
    ~ATSPartialsItem();

    ATSPartialItem* partial(int nPartIndex);
    int count() const;

 private:

    QList<ATSPartialItem*> _qlPartials;
};

//====================================================================
class ATSResidualBandsItem : public ATSModelItem
{
 public:

    ATSResidualBandsItem(ATSSound* pATSSound);
    ~ATSResidualBandsItem();

    ATSResidualBandItem* residualBand(int nBandIndex);
    int count() const;

 private:

    QList<ATSResidualBandItem*> _qlResidualBands;
};

//====================================================================
class ATSRootItem : public ATSModelItem
{
 public:

    ATSRootItem(ATSSound* pATSSound);
    ~ATSRootItem();

    void resetPartials();

    ATSFilePropertiesItem* fileProperties();

    ATSPartialsItem* partials();

    ATSResidualBandsItem* residualBands();

 private:

    ATSFilePropertiesItem* _pmiFileProps;
    ATSPartialsItem*       _pmiPartials;
    ATSResidualBandsItem*  _pmiResidualBands;
};

#endif // ATSMODELITEMS_H
