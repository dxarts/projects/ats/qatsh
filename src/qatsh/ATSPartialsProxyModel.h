// ATSPartialsProxyModel class definition
//
//  Proxy for the ATS document "model" for the partials part of the model
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef ATSPARTIALSPROXYMODEL_H
#define ATSPARTIALSPROXYMODEL_H

#include <string>

#include "TypesAndConstants.h"
#include "ATSDataProxyModel.h"


class ATSPartialsProxyModel : public ATSDataProxyModel
{
    Q_OBJECT

public:

    ATSPartialsProxyModel(QObject *parent = 0);
    virtual ~ATSPartialsProxyModel();

    // QAbstractProxyModel implementation.
    virtual QModelIndex mapFromSource(const QModelIndex& sourceIndex) const;
    virtual QModelIndex mapToSource(const QModelIndex& proxyIndex) const;

    virtual QItemSelection mapSelectionFromSource(const QItemSelection& sourceSel) const;
    //virtual QItemSelection mapSelectionToSource(const QItemSelection& proxySel) const;

    virtual QModelIndex index(int row, int column, const QModelIndex &miParent) const;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
    virtual QModelIndex parent(const QModelIndex &index) const;

    // Get number of partials.
    int nbPartials() const;

    // Add/remove partials.
    void addPartial(const QModelIndex& qmiToCopy);
    void removePartials(const QModelIndexList& qlmiToRemove);
    std::string modifyPartials(const QModelIndexList& qlmiToModify,
			       const QString& qsFormula,
			       TypesAndConstants::EPartialMagnitude eMagnitude);

};

#endif // ATSPARTIALSPROXYMODEL_H
