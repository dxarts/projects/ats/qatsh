// FilePropertiesDialog class implementation
//
//  The view/dialog that displays the current .ats document header data
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <QModelIndex>

#include "ATSModelItems.h"
#include "ATSPropertiesProxyModel.h"
#include "ATSModelManager.h"

#include "FilePropertiesDialog.h"
#include "ui_FilePropertiesDialog.h"


FilePropertiesDialog::FilePropertiesDialog(QWidget *parent) :
QDialog(parent), _pui(new Ui::FilePropertiesDialog), _pModelMgr(0)
{
    _pui->setupUi(this);
}

FilePropertiesDialog::~FilePropertiesDialog()
{
    delete _pui;
}

void FilePropertiesDialog::setModelManager(ATSModelManager* pModelMgr)
{
    _pModelMgr = pModelMgr;
    
    if (_pModelMgr)
	connect(_pModelMgr->propertiesModel(), 
		SIGNAL(dataChanged(const QModelIndex&, const QModelIndex&)), 
		this, SLOT(onDataChanged(const QModelIndex&, const QModelIndex&)));

    // Force first view update from model.
    onDataChanged(QModelIndex(), QModelIndex());
}

void FilePropertiesDialog::changeEvent(QEvent *e)
{
    switch (e->type()) {
    case QEvent::LanguageChange:
        _pui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void FilePropertiesDialog::onDataChanged(const QModelIndex& qmiTopLeft,
					 const QModelIndex& qmiBotRight)
{
    ATSPropertiesProxyModel* pModel = _pModelMgr->propertiesModel();

    // Then get each property value and set it to the associated control.
    const QModelIndex miType =
        pModel->index(0, ATSFilePropertiesItem::eSoundPropType, QModelIndex());
    _pui->qleFileType->setText(pModel->data(miType, Qt::DisplayRole).toString());

    // TODO: Display a user-friendly string for file type.
    const QModelIndex miFileSize =
	pModel->index(0, ATSFilePropertiesItem::eSoundPropFileSize, QModelIndex());
    const double dFileSize = pModel->data(miFileSize, Qt::DisplayRole).toInt() / 1024.0;
    _pui->qleFileSize->setText(QString::number(dFileSize, 'f', 3));

    const QModelIndex miSamplingRate = 
	pModel->index(0, ATSFilePropertiesItem::eSoundPropSamplingRate, QModelIndex());
    _pui->qleSamplingRate->setText(pModel->data(miSamplingRate, Qt::DisplayRole).toString());

    const QModelIndex miFrameSize = 
	pModel->index(0, ATSFilePropertiesItem::eSoundPropFrameSize, QModelIndex());
    _pui->qleFrameSize->setText(pModel->data(miFrameSize, Qt::DisplayRole).toString());

    const QModelIndex miWindowSize = 
	pModel->index(0, ATSFilePropertiesItem::eSoundPropWindowSize, QModelIndex());
    _pui->qleWindowSize->setText(pModel->data(miWindowSize, Qt::DisplayRole).toString());

    const QModelIndex miPartPerFrm = 
	pModel->index(0, ATSFilePropertiesItem::eSoundPropNbPartials, QModelIndex());
    _pui->qlePartialsPerFrame->setText(pModel->data(miPartPerFrm, Qt::DisplayRole).toString());

    const QModelIndex miNbFrames = 
	pModel->index(0, ATSFilePropertiesItem::eSoundPropNbFrames, QModelIndex());
    _pui->qleNbFrames->setText(pModel->data(miNbFrames, Qt::DisplayRole).toString());

    const QModelIndex miDuration = 
	pModel->index(0, ATSFilePropertiesItem::eSoundPropDuration, QModelIndex());
    const double dDuration = pModel->data(miDuration, Qt::DisplayRole).toDouble();
    _pui->qleDuration->setText(QString::number(dDuration, 'f', 3));

    const QModelIndex miMaxAmplitude = 
	pModel->index(0, ATSFilePropertiesItem::eSoundPropMaxPartialAmplitude, QModelIndex());
    const double dMaxAmpl = pModel->data(miMaxAmplitude, Qt::DisplayRole).toDouble();
    _pui->qleMaxAmplitude->setText(QString::number(dMaxAmpl, 'g', 4));

    const QModelIndex miMaxFrequency = 
	pModel->index(0, ATSFilePropertiesItem::eSoundPropMaxPartialFrequency, QModelIndex());
    const double dMaxFreq = pModel->data(miMaxFrequency, Qt::DisplayRole).toDouble();
    _pui->qleMaxFrequency->setText(QString::number(dMaxFreq, 'f', 2));
}

