// RulerSpec class implementation
//
//  A ruler descriptor
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include "ATSMath.h"

#include "RulerSpec.h"


RulerSpec::RulerSpec()
{
    reset(eHorizontal, ePositive, ePositiveSide, 
	  0.0, 1.0, 0.0, 0.1, 2.0, 
	  QList<double>(), QList<double>());
}

RulerSpec::RulerSpec(RulerSpec::EOrientation eOrientation, RulerSpec::EDirection eDirection, 
		     RulerSpec::ESide eMarksSide,
		     double dMinValue, double dMaxValue, 
		     double dBaseOffset, double dBaseStep, double dLevelDivisor,
		     const QList<double>& qlSecondDivisors, const QList<double>& qlSecondSizes)
{
    reset(eOrientation, eDirection, eMarksSide, 
	  dMinValue, dMaxValue, dBaseOffset, dBaseStep, dLevelDivisor, 
	  qlSecondDivisors, qlSecondSizes);
}

RulerSpec::EOrientation RulerSpec::orientation() const
{
    return _eOrientation;
}

void RulerSpec::setOrientation(RulerSpec::EOrientation eOrientation)
{
    _eOrientation = eOrientation;
}

RulerSpec::EDirection RulerSpec::direction() const
{
    return _eDirection;
}

void RulerSpec::setDirection(RulerSpec::EDirection eDirection)
{
    _eDirection = eDirection;
}

RulerSpec::ESide RulerSpec::marksSide() const
{
    return _eMarksSide;
}

void RulerSpec::setMarksSide(RulerSpec::ESide eSide)
{
    _eMarksSide = eSide;
}

double RulerSpec::minValue() const
{
    return _dMinValue;
}

void RulerSpec::setMinValue(double dValue)
{
    resetBaseScale(dValue, _dMaxValue, _dBaseOffset, _dBaseStep);
}

double RulerSpec::maxValue() const
{
    return _dMaxValue;
}

void RulerSpec::setMaxValue(double dValue)
{
    resetBaseScale(_dMinValue, dValue, _dBaseOffset, _dBaseStep);
}

double RulerSpec::baseOffset() const
{
    return _dBaseOffset;
}

void RulerSpec::setBaseOffset(double dOffset)
{
    resetBaseScale(_dMinValue, _dMaxValue, dOffset, _dBaseStep);
}

double RulerSpec::baseStep() const
{
    return _dBaseStep;
}

void RulerSpec::setBaseStep(double dStep)
{
    resetBaseScale(_dMinValue, _dMaxValue, _dBaseOffset, dStep);
}

double RulerSpec::levelDivisor() const
{
    return _dLevelDivisor;
}

void RulerSpec::setLevelDivisor(double dDivisor)
{
    _dLevelDivisor = dDivisor;
}

const QList<double>& RulerSpec::secondaryDivisors() const
{
    return _qlSecondDivisors;
}

void RulerSpec::setSecondaryDivisors(const QList<double>& qlDivisors)
{
    _qlSecondDivisors = qlDivisors;
}

const QList<double>& RulerSpec::secondaryMarksSizes() const
{
    return _qlSecondSizes;
}

void RulerSpec::setSecondaryMarksSizes(const QList<double>& qlSizes)
{
    _qlSecondSizes = qlSizes;
}

void RulerSpec::resetBaseScale(double dMinValue, double dMaxValue, 
			       double dBaseOffset, double dBaseStep)
{
    // Some little checks.
    if (dMaxValue <= dMinValue)
	return;

    // Minimize dBaseOffset
    if (dBaseStep < dBaseOffset)
    {
	dBaseOffset -= dBaseStep * floor(dBaseOffset / dBaseStep);
    }

    // Save specs.
    _dMinValue = dMinValue;
    _dMaxValue = dMaxValue;
    _dBaseOffset = dBaseOffset;
    _dBaseStep = dBaseStep;

}

void RulerSpec::reset(RulerSpec::EOrientation eOrientation, RulerSpec::EDirection eDirection, 
		      RulerSpec::ESide eMarksSide,
		      double dMinValue, double dMaxValue, 
		      double dBaseOffset, double dBaseStep, double dLevelDivisor,
		      const QList<double>& qlSecondDivisors, const QList<double>& qlSecondSizes)
{
    _eOrientation = eOrientation;
    _eMarksSide = eMarksSide;
    _eDirection = eDirection;
    _dLevelDivisor = dLevelDivisor;

    resetBaseScale(dMinValue, dMaxValue, dBaseOffset, dBaseStep);

    _qlSecondDivisors = qlSecondDivisors;
    _qlSecondSizes = qlSecondSizes;
}
