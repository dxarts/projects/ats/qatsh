// ATSDataProxyModel abstract base class declaration
//
//  Abstract proxy model for ATS partials/residual data
//  (source model = ATSModel)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef ATSDATAPROXYMODEL_H
#define ATSDATAPROXYMODEL_H

#include <QAbstractProxyModel>
#include <QModelIndex>


class ATSDataProxyModel : public QAbstractProxyModel
{
    Q_OBJECT

 public:

    ATSDataProxyModel(QObject *parent = 0);
    virtual ~ATSDataProxyModel();

    virtual void setSourceModel(QAbstractItemModel* pSourceModel);

    // Number of selectable frames in the main model (0 <= nFrameIndex < nbFrames).
    int nbFrames() const;

    // Frame duration (s)
    double frameDuration() const;

 protected slots:

    void onSourceModelReset();
};

#endif // ATSDATAPROXYMODEL_H
