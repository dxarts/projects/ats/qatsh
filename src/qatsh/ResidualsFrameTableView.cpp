// ResidualsFrameTableView class implementation
//
//  The table view for 1 frame of the residual bands.
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <iostream>

#include <QHeaderView>
#include <QItemSelection>
#include <QItemSelectionModel>

#include "ATSMath.h"
#include "ATSModelManager.h"
#include "ATSResidualsProxyModel.h"
#include "ATSResidualsFrameProxyModel.h"

#include "ResidualsFrameTableView.h"
#include "ui_ResidualsFrameTableView.h"


ResidualsFrameTableView::ResidualsFrameTableView(QWidget *parent)
: QFrame(parent), _pui(new Ui::ResidualsFrameTableView), _pModelMgr(0)
{
    _pui->setupUi(this);

#if (QT_VERSION >= QT_VERSION_CHECK(5,0,0))
    _pui->qtvResiduals->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
#else
    _pui->qtvResiduals->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
#endif

    // Enable control value synchronization
    _bSynchronizeControls = true;

    // Connect controls to local changed() slots for synchronisation
    connect(_pui->qsdCurrentIndex, SIGNAL(valueChanged(int)),
            this, SLOT(onCurrentIndexChanged(int)));
    connect(_pui->qsbCurrentIndex, SIGNAL(valueChanged(int)),
            this, SLOT(onCurrentIndexChanged(int)));
    connect(_pui->qsbCurrentStartTime, SIGNAL(valueChanged(double)),
            this, SLOT(onCurrentStartTimeChanged(double)));
    connect(_pui->qsbCurrentEndTime, SIGNAL(valueChanged(double)),
            this, SLOT(onCurrentEndTimeChanged(double)));
}

ResidualsFrameTableView::~ResidualsFrameTableView()
{
    delete _pui;
}

void ResidualsFrameTableView::setModelManager(ATSModelManager* pModelMgr)
{
    _pModelMgr = pModelMgr;

    _pui->qtvResiduals->setModel(_pModelMgr->residualsFrameModel());

    connect(_pui->qtvResiduals->selectionModel(), SIGNAL(selectionChanged(const QItemSelection&, 
									 const QItemSelection&)), 
	    this, SLOT(onSelectionChanged(const QItemSelection&, const QItemSelection&)));

    _nMaxFrameIndex = _pModelMgr->residualsFrameModel()->nbFrames() - 1;
    _dFrameDuration = _pModelMgr->residualsFrameModel()->frameDuration();

    // Reset maximum value for sliders
    _pui->qsdCurrentIndex->setMaximum(_nMaxFrameIndex);

    // Reset maximum value for index spin-boxes
    _pui->qsbCurrentIndex->setMaximum(_nMaxFrameIndex);

    // Reset minimum/maximum/step value for time spin-boxes
    _pui->qsbCurrentStartTime->setSingleStep(_dFrameDuration);
    _pui->qsbCurrentStartTime->setMaximum(_nMaxFrameIndex*_dFrameDuration);

    _pui->qsbCurrentEndTime->setSingleStep(_dFrameDuration);
    _pui->qsbCurrentEndTime->setMinimum(_dFrameDuration);
    _pui->qsbCurrentEndTime->setMaximum((_nMaxFrameIndex+1)*_dFrameDuration);
}

int ResidualsFrameTableView::currentFrameIndex() const
{
    return _pui->qsdCurrentIndex->value();
}

void ResidualsFrameTableView::onSelectionChanged(const QItemSelection& qisSelected, 
						 const QItemSelection& qisDeselected)
{
    std::cout << "ResidualsFrameTableView::onSelectionChanged : To be implemented" << std::endl;

}

// Slots to synchronize control values ===============================
void ResidualsFrameTableView::onCurrentIndexChanged(int nNewIndex)
{
    // Nothing to do if control synchronization is blocked.
    if (!_bSynchronizeControls)
	return;

    // Synchronize attached controls.
    _bSynchronizeControls = false;
    _pui->qsbCurrentIndex->setValue(nNewIndex);
    _pui->qsdCurrentIndex->setValue(nNewIndex);
    _pui->qsbCurrentStartTime->setValue(nNewIndex*_dFrameDuration);
    _pui->qsbCurrentEndTime->setValue((nNewIndex+1)*_dFrameDuration);
    _bSynchronizeControls = true;

    // Update model.
    _pModelMgr->residualsFrameModel()->setCurrentFrame(nNewIndex);

    // Notify the world in case usefull.
    emit currentFrameChanged(nNewIndex);
}

void ResidualsFrameTableView::onCurrentStartTimeChanged(double dNewTime)
{
    // Nothing to do if control synchronization is blocked.
    if (!_bSynchronizeControls)
	return;

    // Synchronize attached controls.
    _bSynchronizeControls = false;
    const int nFrameIndex = (int)round(dNewTime / _dFrameDuration);

    _pui->qsbCurrentIndex->setValue(nFrameIndex);
    _pui->qsdCurrentIndex->setValue(nFrameIndex);
    _pui->qsbCurrentEndTime->setValue(dNewTime + _dFrameDuration);
    _bSynchronizeControls = true;

    // Update model.
   _pModelMgr->residualsFrameModel()->setCurrentFrame(nFrameIndex);

    // Notify the world in case usefull.
    emit currentFrameChanged(nFrameIndex);
}

void ResidualsFrameTableView::onCurrentEndTimeChanged(double dNewTime)
{
    // Nothing to do if control synchronization is blocked.
    if (!_bSynchronizeControls)
	return;

    // Synchronize attached controls.
    _bSynchronizeControls = false;
    const int nFrameIndex = (int)round(dNewTime / _dFrameDuration) - 1;

    _pui->qsbCurrentIndex->setValue(nFrameIndex);
    _pui->qsdCurrentIndex->setValue(nFrameIndex);
    _pui->qsbCurrentStartTime->setValue(dNewTime - _dFrameDuration);
    _bSynchronizeControls = true;

    // Update model.
    _pModelMgr->residualsFrameModel()->setCurrentFrame(nFrameIndex);

    // Notify the world in case usefull.
    emit currentFrameChanged(nFrameIndex);
}


