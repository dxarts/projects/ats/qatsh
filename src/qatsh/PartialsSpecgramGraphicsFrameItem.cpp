// PartialsSpecgramGraphicsFrameItem class implementation
//
//  The QGraphicsItem-derived partial frame graphics item in a Spectrogram scene
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <iostream>

#include <QItemSelectionModel>
#include <QPen>
#include <QGraphicsSceneHoverEvent>

#include "ColorMap.h"
#include "PartialsSpecgramGraphicsScene.h"
#include "PartialsSpecgramGraphicsFrameItem.h"


PartialsSpecgramGraphicsFrameItem::PartialsSpecgramGraphicsFrameItem(qreal t1, qreal f1, qreal t2, qreal f2,
																	 double ampl, const ColorMap* pcmAmplColorMap,
																	 int nPartialIndex, int nFrameIndex,
																	 QGraphicsItem* parent)
: QGraphicsLineItem(t1, f1, t2, f2, parent), _dAmplitude(ampl),
  _nPartialIndex(nPartialIndex), _nFrameIndex(nFrameIndex), _pcmAmplColorMap(pcmAmplColorMap)
{
	// Fixme: call setAmplitude ?
}

PartialsSpecgramGraphicsFrameItem::~PartialsSpecgramGraphicsFrameItem()
{
}

// Fixme: the following causes EXC_BAD_ACCESS error
// QPainterPath PartialsSpecgramGraphicsFrameItem::shape() const
// {
//     // Re-implement shape for user-friendly item selection
//     // (1 pixel height is really too thin to easily click on).
//     // As for Qt 4.5.2, seems default shape implementation doesn't work as documented ...
//     // that should be just what follows, IIUC.
//     QPainterPath qpPath = QGraphicsLineItem::shape();
//     qpPath.addRect(boundingRect());
//     return qpPath;
// }

QRectF PartialsSpecgramGraphicsFrameItem::boundingRect() const
{
    // Re-implement boundingRect for user-friendly item selection
    // (1 pixel height is really too thin to easily click on).
    QRectF qRect = QGraphicsLineItem::boundingRect();
    qRect.setBottom(5.0);
    qRect.setTop(-5.0);
    return qRect;
}

double PartialsSpecgramGraphicsFrameItem::amplitude() const
{
	return _dAmplitude;
}

void PartialsSpecgramGraphicsFrameItem::setAmplitude(double dAmpl)
{
	_dAmplitude = dAmpl;
	setPen(QPen(_pcmAmplColorMap->color(_dAmplitude)));
}

int PartialsSpecgramGraphicsFrameItem::partialIndex() const
{
	return _nPartialIndex;
}

void PartialsSpecgramGraphicsFrameItem::setPartialIndex(int nPartialIndex)
{
	_nPartialIndex = nPartialIndex;
}

int PartialsSpecgramGraphicsFrameItem::frameIndex() const
{
	return _nFrameIndex;
}

void PartialsSpecgramGraphicsFrameItem::setFrameIndex(int nFrameIndex)
{
	_nFrameIndex = nFrameIndex;
}

void PartialsSpecgramGraphicsFrameItem::highlight(bool bOn)
{
	setPen(bOn ? QPen(Qt::white) : QPen(_pcmAmplColorMap->color(_dAmplitude)));
}

void PartialsSpecgramGraphicsFrameItem::select(bool bOn)
{
    //std::cout << "PartialsSpecgramGraphicsFrameItem::select " << bOn << std::endl;
    highlight(bOn);
	setSelected(bOn);
}

void PartialsSpecgramGraphicsFrameItem::hoverEnterEvent(QGraphicsSceneHoverEvent* qEvent)
{
	const QPointF qpMse = qEvent->lastPos();
	const QPointF qpScn = qEvent->lastScenePos();
	//const QPointF qpScr = qEvent->lastScreenPos();
	const QPointF qpPos = scenePos();
	const QRectF qrRect = boundingRect();
//	std::cout << "PartialsSpecgramGraphicsFrameItem::hoverEnterEvent(t="
//			  << qpPos.x() << ", f=" << qpPos.y() << ") : "
//			  << "mse.t=" << qpMse.x() << ", mse.f=" << qpMse.y()
//			  << ", scn.t=" << qpScn.x() << ", scn.f=" << qpScn.y()
//			  << ", rec.t=" << qrRect.x() << ", rec.f=" << qrRect.y()
//			  << ", rec.dt=" << qrRect.width() << ", rec.df=" << qrRect.height() << std::endl;
			  //<< ", scr.t=" << qpScr.x() << ", scr.f=" << qpScr.y() << std::endl;
	highlight(pen().color() != Qt::white);
	//update();
	//QGraphicsLineItem::hoverEnterEvent(qEvent);
}

void PartialsSpecgramGraphicsFrameItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* qEvent)
{
//        std::cout << "PartialsSpecgramGraphicsFrameItem::hoverLeaveEvent : "
//                  << "isSelected=" << isSelected() << std::endl;
	highlight(pen().color() != Qt::white);
	//update();
	
	//GraphicsLineItem::hoverLeaveEvent(qEvent);
}

void PartialsSpecgramGraphicsFrameItem::mousePressEvent(QGraphicsSceneMouseEvent* qEvent)
{
//        std::cout << "PartialsSpecgramGraphicsFrameItem::mousePressEvent : "
//                  << "keyMods=" << std::hex << qEvent->modifiers() << std::dec << std::endl;
        //QGraphicsLineItem::mousePressEvent(qEvent); // usefull ?

	PartialsSpecgramGraphicsScene* pScene = static_cast<PartialsSpecgramGraphicsScene*>(scene());
    const Qt::KeyboardModifiers keyMods = qEvent->modifiers();
    QItemSelectionModel::SelectionFlags selCmd;
    if (keyMods & Qt::ControlModifier || keyMods & Qt::ShiftModifier)
        selCmd = QItemSelectionModel::Toggle;
    else
        selCmd = QItemSelectionModel::Clear | QItemSelectionModel::Select;
    pScene->selectPartialFrame(partialIndex(), frameIndex(), selCmd);
}

void PartialsSpecgramGraphicsFrameItem::mouseReleaseEvent(QGraphicsSceneMouseEvent* qEvent)
{
	QGraphicsLineItem::mouseReleaseEvent(qEvent);
}

void PartialsSpecgramGraphicsFrameItem::mouseMoveEvent(QGraphicsSceneMouseEvent* qEvent)
{
	QGraphicsLineItem::mouseMoveEvent(qEvent);
}
