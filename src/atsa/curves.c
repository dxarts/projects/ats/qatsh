/*
curves.c : CURVE structure managment
Jean-Philippe Meuret
(Inspired of atsh-curves.c Oscar Pablo Di Liscia / Juan Pampin)
 */

#include <stdlib.h>
#include <float.h>

#include "atsa.h"


/////////////////////////////////////////////////////////////////////////////
void curve_init(CURVE *curve)
{
  // Identity function.
  curve->num_ctlpoints = 2;
  curve->ctlpoint = malloc(curve->num_ctlpoints * sizeof(curve->ctlpoint[0]));
  curve->ctlpoint[0][0] = 0.0;
  curve->ctlpoint[0][1] = 0.0;
  curve->ctlpoint[1][0] = 1.0;
  curve->ctlpoint[1][1] = 1.0;
}

/////////////////////////////////////////////////////////////////////////////
void curve_free(CURVE *curve)
{
  if (curve->ctlpoint)
  {
    free(curve->ctlpoint);
    curve->ctlpoint = 0;
    curve->num_ctlpoints = 0;
  }
}

/////////////////////////////////////////////////////////////////////////////
//returns the number of control points of the curve
int get_nbp(CURVE *curve) 
{
  return curve->num_ctlpoints;
}

////////////////////////////////////////////////////////////////////////////
//returns the x value of the curve at the given control point 
double get_x_value(CURVE *curve, int i_point) 
{
  return curve->ctlpoint[i_point][0];
}

////////////////////////////////////////////////////////////////////////////
//returns the x value of the curve at the given control point 
double get_y_value(CURVE *curve, int i_point) 
{
  return curve->ctlpoint[i_point][1];
}

////////////////////////////////////////////////////////////////////////////
//returns the minimum x value of the curve
double get_minx_value(CURVE *curve) 
{
  double minx = DBL_MAX;
  int i_pt;
  for (i_pt = 0 ; i_pt < curve->num_ctlpoints; i_pt++)
      if (curve->ctlpoint[i_pt][0] < minx)
	  minx = curve->ctlpoint[i_pt][0];
  return minx;
}

////////////////////////////////////////////////////////////////////////////
//returns the minimum x value of the curve
double get_maxx_value(CURVE *curve) 
{
  double maxx = -DBL_MAX;
  int i_pt;
  for (i_pt = 0 ; i_pt < curve->num_ctlpoints; i_pt++)
      if (curve->ctlpoint[i_pt][0] > maxx)
	  maxx = curve->ctlpoint[i_pt][0];
  return maxx;
}

////////////////////////////////////////////////////////////////////////////
//returns the minimum y value of the curve
double get_miny_value(CURVE *curve) 
{
  double miny = DBL_MAX;
  int i_pt;
  for (i_pt = 0 ; i_pt < curve->num_ctlpoints; i_pt++)
      if (curve->ctlpoint[i_pt][1] < miny)
	  miny = curve->ctlpoint[i_pt][0];
  return miny;
}

////////////////////////////////////////////////////////////////////////////
//returns the minimum y value of the curve
double get_maxy_value(CURVE *curve) 
{
  double maxy = -DBL_MAX;
  int i_pt;
  for (i_pt = 0 ; i_pt < curve->num_ctlpoints; i_pt++)
      if (curve->ctlpoint[i_pt][1] > maxy)
	  maxy = curve->ctlpoint[i_pt][0];
  return maxy;
}

