// PartialsSpecgramGraphicsScene class definition
//
//  The QGraphicsScene-derived partials spectrogram scene
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef PartialsSpecgramGraphicsScene_H
#define PartialsSpecgramGraphicsScene_H

#include <QVector>
#include <QItemSelectionModel>
#include <QGraphicsScene>

#include "ColorMap.h"
#include "RulerSpec.h"

class QObject;
class PartialsSpecgramGraphicsFrameItem;
class QModelIndex;
class QItemSelection;
class ATSModelManager;


class PartialsSpecgramGraphicsScene : public QGraphicsScene
{
    Q_OBJECT

 public:

    PartialsSpecgramGraphicsScene(QObject *parent = 0);
    virtual ~PartialsSpecgramGraphicsScene();

    void setModelManager(ATSModelManager* pModelMgr);
    
    // Set amplitude color contrast/brightness in [0, 1]
    void setAmplitudeContrast(double dContrast);
    void setAmplitudeBrightness(double dBrightness);

    void selectPartialFrame(int nPartialIndex, int nFramIndex,
    						QItemSelectionModel::SelectionFlags selFlags);

 public slots:

    void clear();

 signals:
    
    void colorMapChanged(const ColorMap* pColorMap);
    void timeRangeChanged(const RulerSpec* pRulerSpec);
    void frequencyRangeChanged(const RulerSpec* pRulerSpec);

 private:

    void loadFromModel();
    void loadPartials();
    
 private slots:

    void onModelReset();
    void onDataChanged(const QModelIndex& qmiTopLeft, const QModelIndex& qmiBotRight);

    void onSelectionChanged(const QItemSelection& qisSelected, 
			    const QItemSelection& qisDeselected);
    
 private:
    
    ATSModelManager *_pModelMgr;

    // Amplitude color map.
    ColorMap _cmAmplColorMap;

    // Time ruler spec.
    RulerSpec _rsTimeRulerSpec;

    // Frequency ruler spec.
    RulerSpec _rsFreqRulerSpec;

    // Graphics items index (1 vector of frames for each partial).
    QVector<QVector<PartialsSpecgramGraphicsFrameItem*>* >* _pqvgiPartials;

    // Current max. mean amplitude (over time) of the partials.
    double _dMaxPartialMeanAmpl;
};

#endif // PartialsSpecgramGraphicsScene_H
