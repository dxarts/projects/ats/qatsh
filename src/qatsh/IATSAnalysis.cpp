// IATSAnalysis interface base implementation
//
//  The interface of any ATS analysis : generate 1 ATSSound from 1 sampled sound
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <cstdlib>

#include "IATSAnalysis.h"

IATSAnalysis::~IATSAnalysis()
{
}

const QString IATSAnalysis::typeName()
{
    return QString::null;
}

bool IATSAnalysis::isOfType(const QString& qsType) const
{
    return qsType == type();
}
