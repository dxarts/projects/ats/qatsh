// DocumentWindow class definition
//
//  The widget that aggregate all the views of an ATS document :
//  - the partials list view
//  - the partials spectrogram view
//  - the residual list view
//  - the residual spectrogram view
//  and the associated controls (sliders, splitters, buttons, rulers, ...)
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef DocumentWindow_h
#define DocumentWindow_h

#include <vector>

#include <QString>
#include <QWidget>

#include "SampledSound.h"

namespace Ui {
    class DocumentWindow;
}

class QModelIndex;
class ATSModelManager;
class IATSAnalysis;
class ATSSynthesis;
class MainWindow;


class DocumentWindow : public QWidget 
{
    Q_OBJECT
    Q_DISABLE_COPY(DocumentWindow)

 public:

    explicit DocumentWindow(QWidget *parent);
    virtual ~DocumentWindow();

    // .ats files management
    bool loadFile(const QString &qsFileName);
    bool storeFile(const QString &qsFileName);

    // Sampled sound files management
    bool analyseFile(const QString &qsFileName, IATSAnalysis* pAnalysis);
    bool synthesiseFile(const QString &qsFileName, SampledSound::ESampleFormat eFormat, 
			ATSSynthesis* pSynthesis);

    // Show .ats file properties dialog (if loaded).
    void showFileProperties();

    // Show SelectionEdition/Analysis/Synthesis tool widget
    void showSelectionEditionTool();
    void showAnalysisTool();
    void showSynthesisTool();

    // Toggle graphical view
    typedef enum { ePartialsSpectrogram, eResidualsSpectrogram } EGraphicalView;
    void showGraphicalView(EGraphicalView eView);

    // Zoom managment
    void zoomIn();
    void zoomOut();
    void zoomAll();
    void zoomSelection();

public slots:

    void onStartPlayingSynthesisResult();

protected:

    void connectModelAndViews();

    virtual void changeEvent(QEvent *e);

private slots:

    void onModelChanged(const QModelIndex&, const QModelIndex&);
    void onModelReset();

    void onReAnalyse(const QString&, IATSAnalysis*);
    void onReSynthesise();
    void onStopPlayingSynthesisResult();

private:

    Ui::DocumentWindow *_pui;

    MainWindow* _pMainWin;

    // Document model manager.
    ATSModelManager* _pModelMgr;

    // Currently active graphical view.
    EGraphicalView _eCurrGraphView;

    // Last analysed sample file.
    QString _qsSampleFileName;

    // Last synthesised sample file and associated synthesis params.
    QString _qsLastSyntResFileName;
    SampledSound::ESampleFormat _eLastSyntFormat;
    ATSSynthesis* _pLastSyntParams;
    std::vector<int> _vecLastSelParts;
};

#endif // DocumentWindow_h
