Partials modification formula fatures
=====================================

Predefined variables :
- P = number of partials
- T = number of frames
- p = current partial index [0, nPartials - 1] (av order)
- t = current frame index [0, nFrames - 1]
- a, amin, amax, amean = amplitude
- f, fmin, fmax, fmean = frequency
- h, hmin, hmax, hmean = phase
- v, vmin, vmax, vmean = selected magnitude (= a or f or h)

Predefined functions :
- A(p,t) = amplitude
- F(p,t) = frequency
- H(p,t) = phase
- V(p,t) = selected magnitude (= a or f or h)


Examples:
- frequency-flatten spectrogram around F(pr,tr) reference frequency
     f = f*F(pr,tr)/F(pr,t)

  Ex: xj220b.ats : f = f*F(38,42)/F(38,t)
