// SynthesisParamsWidget class definition
//
//  The synthesis parameters widget
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef SynthesisParamsWidget_h
#define SynthesisParamsWidget_h

#include <QWidget>

namespace Ui {
    class SynthesisParamsWidget;
}

class ATSSynthesis;

class SynthesisParamsWidget : public QWidget
{
    Q_OBJECT
    Q_DISABLE_COPY(SynthesisParamsWidget)

 public:

    explicit SynthesisParamsWidget(QWidget *parent = 0);
    virtual ~SynthesisParamsWidget();

    void setupSynthesis();
    ATSSynthesis* synthesis();

    void setMaxDuration(double dMaxDuration);

 protected:

    virtual void changeEvent(QEvent *pqEvent);

 public slots:

    void onRestoreDefaults();

 private:

    Ui::SynthesisParamsWidget *_pui;

    // The synthesis parameters.
    ATSSynthesis* _pSynthesis;

    // Max duration of the synthesis (the current sound's duration)
    double _dMaxDuration;
};

#endif // SynthesisParamsWidget_h
