// ATSModelManager class definition
//
//  The class to centralize management for all the different models used in Qatsh
//  as well as the associated selection models that are shared between views.
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#include <QItemSelectionModel>

#include "ATSModelManager.h"

#include "ATSModel.h"
#include "ATSPropertiesProxyModel.h"
#include "ATSPartialsFrameProxyModel.h"
#include "ATSResidualsFrameProxyModel.h"
#include "ATSPartialsProxyModel.h"
#include "ATSResidualsProxyModel.h"


// Constructor / Destructor ==========================================
ATSModelManager::ATSModelManager()
{
    // Create main model.
    _pMainModel = new ATSModel();

    // Create proxy models and associate them to their source model.
    _pPropsModel = new ATSPropertiesProxyModel();
    _pPropsModel->setSourceModel(_pMainModel);

    _pPartsModel = new ATSPartialsProxyModel();
    _pPartsModel->setSourceModel(_pMainModel);

    _pResidsModel = new ATSResidualsProxyModel();
    _pResidsModel->setSourceModel(_pMainModel);
    
    _pPartsFrameModel = new ATSPartialsFrameProxyModel();
    _pPartsFrameModel->setSourceModel(_pPartsModel);

    _pResidsFrameModel = new ATSResidualsFrameProxyModel();
    _pResidsFrameModel->setSourceModel(_pResidsModel);

    // Create selection models.
    _pPartsSelModel = new QItemSelectionModel(_pPartsModel); 
    _pResidsSelModel = new QItemSelectionModel(_pResidsModel); 
}

ATSModelManager::~ATSModelManager()
{
    if (_pPropsModel)
	delete _pPropsModel;
    if (_pPartsFrameModel)
	delete _pPartsFrameModel;
    if (_pResidsFrameModel)
	delete _pResidsFrameModel;
    if (_pPartsModel)
	delete _pPartsModel;
    if (_pResidsModel)
 	delete _pResidsModel;
    if (_pMainModel)
	delete _pMainModel;
}

// Models getters ======================================================
ATSModel* ATSModelManager::mainModel()
{
    return _pMainModel;
}

ATSPropertiesProxyModel* ATSModelManager::propertiesModel()
{
    return _pPropsModel;
}


ATSPartialsFrameProxyModel* ATSModelManager::partialsFrameModel()
{
    return _pPartsFrameModel;
}

ATSResidualsFrameProxyModel* ATSModelManager::residualsFrameModel()
{
    return _pResidsFrameModel;
}

ATSPartialsProxyModel* ATSModelManager::partialsModel()
{
    return _pPartsModel;
}

ATSResidualsProxyModel* ATSModelManager::residualsModel()
{
    return _pResidsModel;
}

// Selection models accessors ==========================================
QItemSelectionModel* ATSModelManager::partialsSelectionModel()
{
    return _pPartsSelModel;
}

// void ATSModelManager::setPartialsSelectionModel(QItemSelectionModel* pSelModel)
// {
//     _pPartsSelModel = pSelModel;
// }

QItemSelectionModel* ATSModelManager::residualsSelectionModel()
{
    return _pResidsSelModel;
}

// void ATSModelManager::setResidualsSelectionModel(QItemSelectionModel* pSelModel)
// {
//     _pResidsSelModel = pSelModel;
// }

