// SampledSound class definition
//
//  A sound as an array of multi-channel samples
//
// QATSH Copyright 2009 Jean-Philippe MEURET <jpmeuret@free.fr>

#ifndef SAMPLEDSOUND_H
#define SAMPLEDSOUND_H


class SampledSound
{
 public:

    // Constructor (_copy_ samples if present)
    SampledSound(int nChannels = 0, int nFrames = 0, int nSamplingRate = 0, 
		 double** adSamples = 0);

    // Destructor.
    ~SampledSound();

    // Load from file.
    bool load(const char* pszFileName);

    // Store to file with given sample format.
    enum ESampleFormat
    {
	ePCM16 = 0, // Signed 16 bit data
	ePCM24,     // Signed 24 bit data
	ePCM32,     // Signed 32 bit data
	eFloat ,    // 32 bit float data
	eDouble,    // 64 bit float data
	
	eSampFormatNumber
    };
    bool store(const char* pszFileName, ESampleFormat eSampFormat) const;

    // Accessors.
    int samplingRate() const;
    int nbChannels() const;
    int nbFrames() const;

    double** samples();
    double* channel(int nChanInd);

 private:

    // Internal full cleanup.
    void freeSamples();

 private:

    // Sample properties.
    int _nChannels; // Ex: Stereo => 2 channels.
    int _nFrames; // Number of samples for each channel.
    int _nSamplingRate; // Number of samples per second.

    // Sample data : 1 double array of samples per channel.
    double** _adSamples; // Access data through [channel][frame]
    
};

#endif // SAMPLEDSOUND_H
